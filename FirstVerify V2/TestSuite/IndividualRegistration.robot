*** Settings ***
Suite Setup       Config
Suite Teardown
Test Teardown     Close All Browsers
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot

*** Test Cases ***
TC_001 Validate the employee registration screen is displayed when the user clicks the Individual Registration
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${CompanyRegStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegHeading_lbl}
    Run Keyword If    '${CompanyRegStatus}'=='False'    Fail    Individual Registration is not displayed/Incorrect

TC_002 Validate the first verify logo displayed in the Employee registration
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${LogoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${FirstverifyLogo_img}
    Run Keyword If    '${LogoStatus}'=='False'    Fail    FirstVerify Logo is not displayed

TC_003 Validate the Employee registration text is displayed as the header
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${HeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegHeading_lbl}
    Run Keyword If    '${HeadingStatus}'=='False'    Fail    Individual Registration Header is not displayed

TC_004 Validate the fields displayed in the Employee Registration
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${FirstNameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegFirstName_txt}
    Run Keyword If    '${FirstNameStatus}'=='False'    Fail    FirstName Field is not displayed
    ${LastNameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegLastName_txt}
    Run Keyword If    '${LastNameStatus}'=='False'    Fail    LastName Field is not displayed
    ${TitleStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegTitle_txt}
    Run Keyword If    '${TitleStatus}'=='False'    Fail    Title Field is not displayed
    ${DocumentTypeStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegDocumentType_dd}
    Run Keyword If    '${DocumentTypeStatus}'=='False'    Fail    Document Type Field is not displayed
    ${Last4DigitStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegLast4DigitsOfID_txt}
    Run Keyword If    '${Last4DigitStatus}'=='False'    Fail    Last4Digits of ID Field is not displayed
    ${EmailStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegEmailAddress_txt}
    Run Keyword If    '${EmailStatus}'=='False'    Fail    EmailField is not displayed
    ${CreatePwdStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegCreatePassword_txt}
    Run Keyword If    '${CreatePwdStatus}'=='False'    Fail    CreatePassword is not displayed
    ${ConfPwdStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegConfirmPassword_txt}
    Run Keyword If    '${ConfPwdStatus}'=='False'    Fail    Confirm Password is not displayed
    ${VendorCodeStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndRegVendorCode_txt}
    Run Keyword If    '${VendorCodeStatus}'=='False'    Fail    Vendor Code is not displayed
    ${CreateAccountStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${IndRegCreateAccount_btn}
    Run Keyword If    '${CreateAccountStatus}'=='False'    Fail    Create Account Button is not displayed
    ${CancelStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${IndRegCancel_btn}
    Run Keyword If    '${CancelStatus}'=='False'    Fail    Cancel Button is not displayed
    ${BackToLoginStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${IndRegBackToLogin_btn}
    Run Keyword If    '${BackToLoginStatus}'=='False'    Fail    Back To Login Button is not displayed

TC_006 Validate the Placeholder text displayed for the first name field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${IndRegFirstName_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='First Name'    Fail    Placeholder for FirstName is incorrect

TC_007 Validate the error message is displayed when the user enters other than alphabets in first name field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    1234    Kishore    Mr.    Drivers License    1111    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the FistName is incorrect

TC_008 Validate the error message is displayed when the first name is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1111    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the FirstName is left blank

TC_009 Validate the Placeholder text displayed for the Last Name field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${IndRegLastName_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Last \ Name'    Fail    Placeholder for LastName is incorrect

TC_010 Validate the error message is displayed when the user enters other than alphabets in Last Name field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    Deepak    123    Mr.    Drivers License    1111    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Last Name is Incorrect

TC_011 Validate the error message is displayed when the Last Name is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    Deepak    ${EMPTY}    Mr.    Drivers License    1111    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the LastName is left blank

TC_012 Validate the Placeholder text displayed for the Title field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${IndRegTitle_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Title'    Fail    Placeholder for Title is incorrect

TC_013 Validate the error message is displayed when the user enters other than alphabets in Title field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    Deepak    Kishore    123    Drivers License    1111    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Title is Incorrect

TC_013 Validate the error message is displayed when the Title is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    Deepak    Kishore    ${EMPTY}    Drivers License    1111    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Title is left blank

TC_015 Validate the error message is displayed when the user does not select the document type
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    Deepak    Kishore    123    ${EMPTY}    1111    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Document Type is left blank

TC_016 Validate the Placeholder text displayed for the Last 4 Digit of ID field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${IndRegLast4DigitsOfID_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Last 4 Digits of ID'    Fail    Placeholder for Last 4 Digits of ID is incorrect

TC_017 Validate the error message is displayed when the user enters other than Numbers in Last 4 Digits of Id field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    1234    Kishore    Mr.    Drivers License    qwerty    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Last 4 Digits Of ID is alphabet

TC_018 Validate the error message is displayed when the Last 4 Digits of ID is left blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    ${EMPTY}    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Last 4 Digits of ID is left blank

TC_019 Validate the Placeholder text displayed for the Email Address field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${IndRegEmailAddress_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Email Address'    Fail    Placeholder for Email Address is incorrect

TC_020 Validate the error message is displayed when the user enters invalid email id
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    1234    Kishore    Mr.    Drivers License    qwerty    deepakkishore.07@gmail    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Email Address is Incorrect

TC_021 Validate the error message is displayed when the Email is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1234    ${EMPTY}    Qwerty@1    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the EmailAddress is left blank

TC_022 Validate the Placeholder displayed for the Password field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${IndRegCreatePassword_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Create Password'    Fail    Placeholder for Create Password is incorrect

TC_023 Validate the Placeholder displayed for the Confirm Password field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${IndRegConfirmPassword_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Confirm Password'    Fail    Placeholder for Confirm Password is incorrect

TC_024 Validate the error message is displayed when both the password and the confirm password is blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1234    deepakkishore.07@gmail.com    ${EMPTY}    ${EMPTY}    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Password is left blank

TC_025 Validate the error message is displayed when the password \ is blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1234    deepakkishore.07@gmail.com    ${EMPTY}    Qwerty@1    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Password is left blank

TC_026 Validate the error message is displayed when the Confirm password \ is blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1234    deepakkishore.07@gmail.com    Qwerty@1    ${EMPTY}    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Confirm Password is left blank

TC_027 Validate the error message is displayed when the Password and Confirm password are not matching
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1234    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@12    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Password and Confirm Password is not matching

TC_028 Validate the error message is displayed when the Password does not meet the Password criterial
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1234    deepakkishore.07@gmail.com    123    123    1234
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Password and Confirm Password is not matching

TC_029 Validate the Placeholder text displayed for the Vendor Code field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${IndRegVendorCode_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Vendor Code'    Fail    Placeholder for VendorCode is incorrect

TC_030 Validate the error message is displayed when the Vendor Code is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1234    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    ${EMPTY}
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Vendor code is left blank

TC_031 Validate the Individual registration is completed by providing the valid details in all the fields
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${IndividualRegistration_btn}
    IndividualRegistration    ${EMPTY}    Kishore    Mr.    Drivers License    1234    deepakkishore.07@gmail.com    Qwerty@1    Qwerty@1    123
    #Validate the success message is displayed or not
