*** Settings ***
Suite Setup       Config
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot
Resource          ../Resources/ReusableGroup.robot
Resource          ../Resources/Pages/DashboardPage.robot
Library           OperatingSystem
Library           String
Library           DateTime
Resource          ../Resources/Pages/QuestionarePage.robot

*** Test Cases ***
VerifyThe Section Displayed
    Set Selenium Timeout    ${timeout}
    ${PqID}    Set Variable    20487    #    61699    61517    58054    57580
    ${Token}    GetAuthToken
    GetPQLocations    ${PqID}
    ${JsonObj_PqSections_Res}    GetPQSections    ${PqID}
    Set Suite Variable    ${PqID}
    Set Suite Variable    ${Token}
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardDashboardAnalytics_btn}    Dashboard Analytics Button
    Comment    SelectClient    Clayco
    Comment    Input Text    ${DashboardSearch_txt}    Miller Electric
    Comment    Press Keys    ${DashboardSearch_txt}    ENTER
    Comment    ${QuestionareName}    Set Variable    Miller Electric    #    Chaparral Plumbing, Inc.
    Comment    Wait Until Element Is Visible    //a[text()="${QuestionareName}"]    timeout=20s
    Comment    Click Element    //a[text()="${QuestionareName}"]
    Go To    https://fvpq2.firstverify.com/vendordetails?pqid=${PqID}
    Wait Until Element Is Visible    //button[text()="Questionnaire"]    ${timeout}
    Click Element    //button[text()="Questionnaire"]
    Scroll to the element    //button[text()="Questionnaire"]    down
    Comment    Execute Javascript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    Wait Until Element Is Not Visible    //span[@role="progressbar"]    ${timeout}
    ${Sections}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data
    ${TotalSectionCount}    Get Length    ${Sections}
    FOR    ${DataIndex}    IN RANGE    0    ${TotalSectionCount}
        ${SectionName}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${DataIndex}].sectionName
        Verify Questionare SubSection    ${DataIndex}    ${JsonObj_PqSections_Res}
    END

*** Keywords ***
Verify Questionare SubSection
    [Arguments]    ${DataIndex}    ${JsonObj_PqSections_Res}
    Set Test Variable    ${DataIndex}
    ${Sections}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${DataIndex}]
    Log    ${Sections}
    ${SectionID}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${DataIndex}].sectionId
    ${SectionType}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${DataIndex}].sectionType
    ${SectionName}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${DataIndex}].sectionName
    #PqSubSections
    ${JsonObj_PqSubSections_Res}    GetPqSubSections    ${PqID}    ${SectionID}
    Scroll to the element    //div[text()="${SectionName}"]    UP
    Click Element    //div[text()="${SectionName}"]
    Execute Javascript    window.scrollTo(0, document.body.scrollHeight)
    Wait Until Element Is Not Visible    //span[@role="progressbar"]    ${timeout}
    Sleep    2s
    Run Keyword And Return Status    Click Element    //div[text()="${SectionName}"]
    Wait Until Element Is Not Visible    //span[@role="progressbar"]    ${timeout}
    Sleep    2s
    #Verify the Section Content
    Verify the section header content    ${JsonObj_PqSections_Res}    ${SectionName}
    #Verifyies the section name displayed in the Questionare against the API
    Verify the section names, section count and the status displayed in the Questionare    ${JsonObj_PqSections_Res}
    #Verify the Question and the Content displayed in the subsection
    ${SubSections}    GetValueFromResponse    ${JsonObj_PqSubSections_Res}    $.data
    ${TotalSubSectionCount}    Get Length    ${SubSections}
    ${SelectSubsectionIndex}    Set Variable    1
    FOR    ${i}    IN RANGE    0    ${TotalSubSectionCount}
        ${SubSectionId}    GetValueFromResponse    ${JsonObj_PqSubSections_Res}    $.data[${i}].subSectionId
        ${SubSectionName}    GetValueFromResponse    ${JsonObj_PqSubSections_Res}    $.data[${i}].subSectionName
        ${subSectionTypeCondition}    GetValueFromResponse    ${JsonObj_PqSubSections_Res}    $.data[${i}].subSectionTypeCondition
        ${JsonObj_SectionOrSubSectionDetails_Res}    GetSectionOrSubSectionDetails    ${PqID}    ${SectionID}    ${SectionType}    ${subSectionTypeCondition}    ${SubSectionId}
        ${SubSectionName}    GetValueFromResponse    ${JsonObj_PqSubSections_Res}    $.data[${i}].subSectionName
        ${JsonObj_PqLocations_Res}    Run Keyword If    "${SectionType}"=="7"    GetPQLocations    ${PqID}
        Scroll to the element    //div[text()="${SelectSubsectionIndex}"]/parent::button    up
        Click Element    //div[text()="${SelectSubsectionIndex}"]/parent::button
        Run Keyword If    'Supporting Documents'=='${SectionName}'    Verify Supporting Documents    ${JsonObj_SectionOrSubSectionDetails_Res}
        Run Keyword If    'Experience Modification Rating'!='${SubSectionName}'    Verify the questions displayed    ${JsonObj_SectionOrSubSectionDetails_Res}    0
        Run Keyword If    "${SectionType}"=="7"    Verify Location    ${JsonObj_PqLocations_Res}
        ${SelectSubsectionIndex}    Evaluate    ${SelectSubsectionIndex}+1
        Sleep    5s
    END
    ${JsonObj_SectionOrSubSectionDetails_Res}    GetSectionOrSubSectionDetails    ${PqID}    ${SectionID}    ${SectionType}    ${EMPTY}    ${EMPTY}
    ${clientProfiles}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.clientProfiles
    ${clientProfilesCount}    Get Length    ${clientProfiles}
    FOR    ${i}    IN RANGE    0    ${clientProfilesCount}
        ${Title}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.clientProfiles[${i}].positionTitle
        ${index}    Evaluate    ${i}+1
        Run Keyword If    "${Title}"!="${EMPTY}" and "${Title}"!="None"    Click Element    //span[text()="${Title}"]/parent::button
        ...    ELSE    Click Element    //div[text()="${index}"]/parent::button
        Verify the subsection content without question    ${JsonObj_SectionOrSubSectionDetails_Res}    ${i}
    END

Verify Location
    [Arguments]    ${JsonObj_PqLocations_Res}
    ${Locations}    GetValueFromResponse    ${JsonObj_PqLocations_Res}    $.data
    ${Count}    Get Length    ${Locations}
    FOR    ${i}    IN RANGE    0    ${Count}
        ${businessUnitName}    GetValueFromResponse    ${JsonObj_PqLocations_Res}    $.data[${i}].businessUnitName
        ${businesUnitsiteName}    GetValueFromResponse    ${JsonObj_PqLocations_Res}    $.data[${i}].businesUnitsiteName
        ${siteStatus}    GetValueFromResponse    ${JsonObj_PqLocations_Res}    $.data[${i}].siteStatus
        ${ClientName}    GetValueFromResponse    ${JsonObj_PqLocations_Res}    $.data[${i}].clientName
        Run Keyword And Return Status    Click Element    //span[text()="${ClientName}"]/parent::div/parent::div[@aria-expanded="false"]
        VerifyElementIsDisplayed    //th[text()="${businessUnitName}"]    ${businessUnitName}
        Comment    Scroll to the element    //td[text()="${businesUnitsiteName}"]    up
        VerifyElementIsDisplayed    //td[text()="${businesUnitsiteName}"]    ${businesUnitsiteName}
        VerifyElementIsDisplayed    //td[text()="${businesUnitsiteName}"]/..//span[text()="${siteStatus}"]    ${businesUnitsiteName}:- ${siteStatus}
    END
