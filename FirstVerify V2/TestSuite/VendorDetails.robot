*** Settings ***
Suite Setup       Config
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/VendorDetailsPage.robot
Library           String
Resource          ../Resources/Pages/DashboardPage.robot
Library           OperatingSystem
Library           DateTime

*** Test Cases ***
TC_01_Vendor Details_Validate the click functionality of Vendor Details
    LoginToFirstVerify    admin
    ${ClientName}    Set Variable    Green Plains, Inc.
    ${VendorName}    Set Variable    Test by tejoma15
    GoToVendorDetails    ${ClientName}    ${VendorName}
    Wait Until Element Is Visible    ${VendorDetailVendorName_lbl}
    Wait Until Element Is Visible    ${VendorDetailClientName_lbl}
    Wait Until Element Is Visible    ${VendorDetailDate_lbl}
    Wait Until Element Is Visible    ${VendorDetailTab_btn}
    Wait Until Element Is Visible    ${VendorDetailQuestionnare_btn}

TC_02_Locations_Validate the Locations section is getting displayed

TC_03_General information_Validate the General information section is getting displayed

TC_04_Address_Validate the Address section is getting displayed

TC_05_Contact Information_Validate the Contact Information section is getting displayed

TC_06_Vendor Contacts_Validate the Vendor Contacts section is getting displayed

TC_07_Vendor Details_Validate the Completed By section is getting displayed

TC_08_Contractor Performance Evaluation_Validate the Contractor Performance Evaluation section is getting displayed

TC_09_Vendor Details_Validate the footer section is getting displayed

TC_10_Contractor Performance Evaluation_Validate the Completed Evaluations button is getting displayed

TC_11_Vendor Details_Validate the Users icon is getting displayed

TC_12_Vendor Details_Validate the Email icon is getting displayed

TC_13_Vendor Details_Validate the Eye icon is getting displayed

TC_14_Vendor Details_Validate the Edit icon is getting displayed

TC_15_Vendor Details_Validate the Questionnaire Status is getting displayed

TC_16_Vendor Details_Validate the Delete Button is getting displayed

TC_17_Vendor Details_Validate the Vendor Name is getting displayed

TC_18_Vendor Details_Validate the Client Name is getting displayed

TC_19_Vendor Details_Validate the Status Period is getting displayed

TC_20_Vendor Details_Validate the Prequalification history is getting displayed

TC_21_Vendor Details_Validate the invitations is getting displayed

TC_22_Vendor Details_Validate the Edit icon is getting displayed under status

TC_23_Locations_Validate the popup message is getting displayed when the Delete Button is clicked under the location section

TC_24_Locations_​Validate the DISAGREE Button is getting displayed after clicking on Delete Button under the location section

TC_25_Locations_​Validate the AGREE Button is getting displayed after clicking on Delete Button under the location section

TC_26_Locations_​Validate the click functionality of DISAGREE Button after clicking on Delete Button under the location section

TC_27_Locations_​Validate the click functionality of AGREE Button after clicking on Delete Button under the location section

TC_28_Locations_Validate the client name under the location section

TC_29_Locations_Validate the location name under Locations section

TC_30_Locations_Validate the status is getting displayed under the Locations section

TC_31_Locations_Validate the message is getting displayed after deleting all locations under the Locations section

TC_32_Locations_Validate the message is getting displayed when there are no locations under the location section

TC_33_General infomation_Validate the Tax ID is getting displayed under the General infomation section

TC_34_General infomation_Validate the Tax ID is getting displayed under the General information section with the Format

TC_35_General infomation_Validate the Vendor ID is getting displayed under the General information section

TC_36_General infomation_Validate the Vendor ID is getting displayed greater than "0" under the General information section

TC_37_General infomation_Validate the Started date within the format is getting displayed under the General information section

TC_38_General infomation_Validate the Time should not be displayed under the Started date

TC_39_Address_Validate the Address is getting displayed under the Address section

TC_40_Address_Validate the Address 1 is getting displayed under the Address section

TC_41_Address_Validate the Address 2 is getting displayed under the Address section

TC_42_Address_Validate the City is getting displayed under the Address section

TC_43_Address_Validate the State or Province is getting displayed under the Address section

TC_44_Address_Validate the Zip/Postal Code is getting displayed under the Address section

TC_45_Contact Information_Validate the Phone number is getting displayed within the format under the Contact Information section

TC_46_Contact Information_Validate the Fax is getting displayed under the Contact Information section

TC_47_Contact Information_Validate the Website is getting displayed under the Contact Information section

TC_48_Vendor Contacts_Validate the Vendor Contacts information is getting displayed under the Vendor Contacts section

TC_49_Vendor Contacts_Validate the Full Name is getting displayed under the Vendor Contacts section

TC_50_Vendor Contacts_Validate the Title name is getting displayed under the Vendor Contacts section

TC_51_Vendor Contacts_Validate the Email Address is getting displayed under the Vendor Contacts section

TC_52_Vendor Contacts_Validate the Telephone Number is getting displayed under the Vendor Contacts section

TC_53_Vendor Details_Validate the drop down is getting displayed under the Prequalification history

TC_54_Vendor Details_Validate the click functionality of drop down under the Prequalification history

TC_55_Vendor Details_Verifying the options of drop down under the Prequalification history

TC_56_Vendor Details_Validate the drop down is getting displayed under the invitations

TC_57_Vendor Details_Validate the click functionality of drop down under the invitations

TC_58_Vendor Details_Verifying the options of drop down under the invitations

TC_59_Contractor Performance Evaluation_Validate the click functionality of Completed Evaluations Button

TC_60_Completed Evaluations_Validate the headers under Completed Evaluations page

TC_61_Completed Evaluations_Validate the Cancel Button is getting displayed under the Completed Evaluations page

TC_62_Completed Evaluations_Validate the Cancel Button functionality under the Completed Evaluations page

TC_63_Contractor Performance Evaluation_Validate the Average Evaluation sub section under the Contractor Performance Evaluation section

TC_64_Contractor Performance Evaluation_Validate the Average Evaluation score under the Contractor Performance Evaluation section

TC_65_Contractor Performance Evaluation_Validate the Average Project Evaluation sub section under the Contractor Performance Evaluation section

TC_66_Contractor Performance Evaluation_Validate the Average Project Evaluation score under the Contractor Performance Evaluation section

TC_67_Contractor Performance Evaluation_Validate the Like symbol under the Average Evaluation sub section

TC_68_Contractor Performance Evaluation_Validate the Dislike symbol under the Average Project Evaluation sub section

TC_69_Contractor Performance Evaluation_Validate the Like score under the Average Evaluation sub section

TC_70_Contractor Performance Evaluation_Validate the Dislike score under the Average Project Evaluation sub section

TC_71_Contractor Performance Evaluation_Validate the Like symbol color under the Average Evaluation sub section

TC_72_Contractor Performance Evaluation_Validate the Dislike symbol color under the Average Project Evaluation sub section

TC_73_Contractor Performance Evaluation_Validate the Like symbol background color under the Average Evaluation sub sections

TC_74_Contractor Performance Evaluation_Validate the Dislike symbol background color under the Average Project Evaluation sub section

TC_75_Contractor Performance Evaluation_Validate the Completed by client user name is getting displayed under the Average Project Evaluation sub section

TC_76_Vendor Details_Validate the click functionality of Edit icon under the status

TC_77_Vendor Details_​Validate the client logo if the client contains the logo is that getting displayed.

TC_78_Vendor Details_​Validate the client logo if the client does not contains the logo that is getting displayed.

TC_79_Vendor Details_Validate the Questionnaire Name is getting displayed

TC_80_Vendor Details_Validate the Renew button is getting displayed in the Admin Dashboard under the Vendor details page.

TC_81_Vendor Details_Validate the Renew button should not be displayed in the Vendor Dashboard under the Vendor details page.

TC_82_Vendor Details_Verifying the expiration date and the renew button functionality

TC_83_Vendor Details_Verifying the appearance of the tooltip while Hovering on any card in the "No Response" Vendor details page under the location section

TC_84_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Expired" Vendor details page under the location section

TC_85_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Client Review" Vendor details page under the location section

TC_86_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Probation" Vendor details page under the location section

TC_87_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "N/A" Vendor details page under the location section

TC_88_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Processing" Vendor details page under the location section

TC_89_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "No Response to Renewal Request" Vendor details page under the location section

TC_90_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Not Prequalified" Vendor details page under the location section

TC_91_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Pending-Renewal" Vendor details page under the location section

TC_92_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Prequalified" Vendor details page under the location section

TC_93_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Incomplete" Vendor details page under the location section

TC_94_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Pending" Vendor details page under the location section

TC_95_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Exception" Vendor details page under the location section

TC_96_Vendor Details_Verifying the appearance of the tooltip while Hovering on on any card in the "Banned" Vendor details page under the location section

TC_97_Locations_Validate the Questionnaire status is getting displayed under the location under the location section

TC_98_Change Status_Validate the Current Period Start Date is getting displayed correctly

TC_99_Change Status_Validate the Current Status is getting displayed correctly

TC_100_Change Status_Validate the check box for I have reviewed the Supporting Documents section for missing or expired documents before changing status to Prequalified, Exception or Probation.

TC_101_Change Status_Validate the statuses is getting displayed under the New Status drop down

TC_102_Change Status_Validate the New Period Start Date

TC_103_Change Status_Validate the New Status Change Date

TC_104_Change Status_Validate the users are getting displayed under the Vendor Assigned To drop down

TC_105_Change Status_Validate the click functionality of Yes/No options under the Display Exception/Probation section

TC_106_Change Status_Validate the sections under the Select Sections Not Complete

TC_107_Change Status_Validate the * icon color for the Vendor Assigned To required field

TC_108_Change Status_Validate the click functionality of Yes/No options under the Confirm Change

TC_109_Change Status_Validate the Save button is getting displayed

TC_110_Change Status_Validate the Save button functionality

TC_111_Change Status_Validate the Cancel button is getting displayed

TC_112_Change Status_Validate the Cancel Button functionality

TC_113_Completed Evaluations_Validate the Grid background color under the Other Evaluations Tab

TC_114_Other Evaluations_Validate the Review Type Bi- Annual is getting displayed

TC_115_Other Evaluations_Validate the Review Type Annual is getting displayed

TC_116_Other Evaluations_Validate the Review Type Project is getting displayed

TC_117_Other Evaluations_Validate the Rating is getting displayed for all the Reveiw types

TC_118_Other Evaluations_Validate the Evaluated By user name is getting displayed for all the Review types

TC_119_Completed By_Validate the vendor Full Name is getting displayed under the Completed By section

TC_120_Completed By_Validate the Title Name is getting displayed under the Completed By section

TC_121_Completed By_Validate the Signed date and time is getting displayed within the format

TC_122_Completed By_Validate the Started date is getting displayed
