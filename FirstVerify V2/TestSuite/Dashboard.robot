*** Settings ***
Suite Setup       Config
Suite Teardown
Test Teardown     Close All Browsers
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot
Library           String
Resource          ../Resources/Pages/DashboardPage.robot
Library           OperatingSystem
Library           DateTime

*** Test Cases ***
TC_001 Validate the first verify Logo is displayed in the Dashboard screen
    LoginToFirstVerify    admin
    ${Status}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardTitle_lbl}
    Run Keyword If    '${Status}'!='True'    Fail    Dashboard is not displayed
    ${LogoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardLogo_img}
    Run Keyword If    '${LogoStatus}'=='False'    Fail    FirstVerify Logo is not displayed in dashboard screen

TC_002 Validate the Search TextBox is displayed
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardSearch_txt}    Search TextBox

TC_003 Validate the Download button is displayed
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardDownload_btn}    Download Button

TC_004 Validate the List\Grid Veiw button is displayed
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardListGridView_btn}    List/Grid View Button

TC_005 Validate the Dashboard Analytics button is displayed
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardDashboardAnalytics_btn}    Dashboard Analytics Button

TC_006 Validate the Recent Documents button is displayed
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardTitle_lbl}    Page Title Dashboard
    VerifyElementIsDisplayed    ${DashboardRecendDocuments_btn}    Recent Documents Button

TC_007 Validate the click functionality of "download" button
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardDownload_btn}
    Sleep    5s
    ${DownloadFileStatus}    Run Keyword And Return Status    File Should Exist    ${downloadDir}PreQualification.csv
    Run Keyword If    ${DownloadFileStatus}!=True    Fail    File Not Downloaded
    ${DeleteFileStatus}    Run Keyword And Return Status    Remove File    ${downloadDir}PreQualification.csv

TC_008 Validate the click functionality of "download" button under "Greater than 30 days" tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    ClickElement_rgp    ${DashboardDownload_btn}
    Sleep    5s
    ${DownloadFileStatus}    Run Keyword And Return Status    File Should Exist    ${downloadDir}PreQualification.csv
    Run Keyword If    ${DownloadFileStatus}!=True    Fail    File Not Downloaded
    ${DeleteFileStatus}    Run Keyword And Return Status    Remove File    ${downloadDir}PreQualification.csv

TC_009 Validate the click functionality of "download" button under "Within 30 days" tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    ClickElement_rgp    ${DashboardDownload_btn}
    ${DownloadFileStatus}    Run Keyword And Return Status    File Should Exist    ${downloadDir}PreQualification.csv
    ${DeleteFileStatus}    Run Keyword And Return Status    Remove File    ${downloadDir}PreQualification.csv
    Run Keyword If    ${DownloadFileStatus}!=True    Fail    File Not Downloaded

TC_010 Validate the click functionality of "download" button under "ClientReview" tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    ClickElement_rgp    ${DashboardDownload_btn}
    ${DownloadFileStatus}    Run Keyword And Return Status    File Should Exist    ${downloadDir}PreQualification.csv
    ${DeleteFileStatus}    Run Keyword And Return Status    Remove File    ${downloadDir}PreQualification.csv
    Run Keyword If    ${DownloadFileStatus}!=True    Fail    File Not Downloaded

TC_011 Validate the click functionality of "download" button under "Yesterday" tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    ClickElement_rgp    ${DashboardDownload_btn}
    ${DownloadFileStatus}    Run Keyword And Return Status    File Should Exist    ${downloadDir}PreQualification.csv
    ${DeleteFileStatus}    Run Keyword And Return Status    Remove File    ${downloadDir}PreQualification.csv
    Run Keyword If    ${DownloadFileStatus}!=True    Fail    File Not Downloaded

TC_012 Validate the click functionality of "download" button under "Reminder" tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    ClickElement_rgp    ${DashboardDownload_btn}
    ${DownloadFileStatus}    Run Keyword And Return Status    File Should Exist    ${downloadDir}PreQualification.csv
    ${DeleteFileStatus}    Run Keyword And Return Status    Remove File    ${downloadDir}PreQualification.csv
    Run Keyword If    ${DownloadFileStatus}!=True    Fail    File Not Downloaded

TC_013 Validate the click functionality of Grid View/ List View
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardListGridView_btn}

TC_014 Validate the user is able to navigate to Recent Documents
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardRecendDocuments_btn}    Recent Documents Button
    ClickLink_rgp    ${DashboardRecendDocuments_btn}
    VerifyElementIsDisplayed    //h5[text()="Recent Documents"]    Page Title Recent Documents

TC_015 Validate the user is able to navigate to Dashboard Analytics
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardDashboardAnalytics_btn}    Dashboard Analytics Button
    ClickLink_rgp    ${DashboardDashboardAnalytics_btn}
    VerifyElementIsDisplayed    //h2[text()="Dashboard Analytics"]    Page Title Dashboard Analytics

TC_016 Validate the click functionality of the "Greater than 30 Days" button in the dashboard
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_017 Validate the click functionality of the "Within 30 Days" button in the dashboard
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_018 Validate the click functionality of the "Client Review" button in the dashboard
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_019 Validate the click functionality of the "Yesterday" button in the dashboard
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_020 Validate the click functionality of the "Reminder" button in the dashboard
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_021 Validate the tooltip displayed in the Client Review button
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Mouse Over    ${DashboardFirstCardClientReview_btn}
    ${Act_txt}    Get Text    ${DashboardButtonTooltip_lbl}
    ${Exp_txt}    Set Variable    Client is reviewing vendor's information
    Run Keyword If    "${Act_txt}"!="${Exp_txt}"    Fail    Tooltip displayed on the processing button is incorrect

TC_022 Validate the tooltip displayed in the Incomplete button
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Mouse Over    ${DashboardFirstCardIncomplete_btn}
    ${Act_txt}    Get Text    ${DashboardButtonTooltip_lbl}
    ${Exp_txt}    Set Variable    Vendor's information is incomplete or inaccurate
    Run Keyword If    "${Act_txt}"!="${Exp_txt}"    Fail    Tooltip displayed on the processing button is incorrect

TC_023 Validate the tooltip displayed in the processing button
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Mouse Over    ${DashboardFirstCardProcessing_btn}
    ${Act_txt}    Get Text    ${DashboardButtonTooltip_lbl}
    ${Exp_txt}    Set Variable    Vendor has finalized and submitted the questionnaire for processing
    Run Keyword If    "${Act_txt}"!="${Exp_txt}"    Fail    Tooltip displayed on the processing button is incorrect

TC_040 Verifying the click on any page number in the "Greater than 30 days" tab and click on any other tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    Mouse Out    ${DashboardFirstCardProcessing_btn}
    ClickElement_rgp    //nav[@aria-label="pagination navigation"]/ul/li[5]/button    #Clicking the 5th PageNo
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    Execute JavaScript    window.scrollTo(0, 0)
    Sleep    2s
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ${PageSelected}    Run Keyword If    ${NoRecordsFoundStatus}!= True    Get Element Attribute    //nav[@aria-label="pagination navigation"]/ul/li[2]/button    aria-current
    Run Keyword If    "${PageSelected}"!="true" and ${NoRecordsFoundStatus}!=True    Fail    Page 1 is not selected
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_041 Verifying the click on any page number in the "Reminder" tab and click on any other tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    ClickElement_rgp    //nav[@aria-label="pagination navigation"]/ul/li[5]/button    #Clicking the 5th PageNo
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    Execute JavaScript    window.scrollTo(0, 0)
    Sleep    2s
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ${PageSelected}    Run Keyword If    ${NoRecordsFoundStatus}!= True    Get Element Attribute    //nav[@aria-label="pagination navigation"]/ul/li[2]/button    aria-current
    Run Keyword If    "${PageSelected}"!="true" and ${NoRecordsFoundStatus}!=True    Fail    Page 1 is not selected
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_042 Verifying the click on any page number in the "ClientReview" tab and click on any other tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    ClickElement_rgp    //nav[@aria-label="pagination navigation"]/ul/li[5]/button    #Clicking the 5th PageNo
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    Execute JavaScript    window.scrollTo(0, 0)
    Sleep    2s
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ${PageSelected}    Run Keyword If    ${NoRecordsFoundStatus}!= True    Get Element Attribute    //nav[@aria-label="pagination navigation"]/ul/li[2]/button    aria-current
    Run Keyword If    "${PageSelected}"!="true" and ${NoRecordsFoundStatus}!=True    Fail    Page 1 is not selected
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_043 Verifying the click on any page number in the "Within 30 Days" tab and click on any other tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    ClickElement_rgp    //nav[@aria-label="pagination navigation"]/ul/li[5]/button    #Clicking the 5th PageNo
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    Execute JavaScript    window.scrollTo(0, 0)
    Sleep    2s
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ${PageSelected}    Run Keyword If    ${NoRecordsFoundStatus}!= True    Get Element Attribute    //nav[@aria-label="pagination navigation"]/ul/li[2]/button    aria-current
    Run Keyword If    "${PageSelected}"!="true" and ${NoRecordsFoundStatus}!=True    Fail    Page 1 is not selected
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_044 Verifying the click on any page number in the "Yesterday" tab and click on any other tab
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    ClickElement_rgp    //nav[@aria-label="pagination navigation"]/ul/li[5]/button    #Clicking the 5th PageNo
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    Execute JavaScript    window.scrollTo(0, 0)
    Sleep    2s
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ${PageSelected}    Run Keyword If    ${NoRecordsFoundStatus}!= True    Get Element Attribute    //nav[@aria-label="pagination navigation"]/ul/li[2]/button    aria-current
    Run Keyword If    "${PageSelected}"!="true" and ${NoRecordsFoundStatus}!=True    Fail    Page 1 is not selected
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_050 Validate the Click functionality of the vendor card
    LoginToFirstVerify    admin
    ${ExpVendorName}    Get Text    ${DashboardFirstCard}/div/div[1]/div/h6/a
    ClickLink_rgp    ${DashboardFirstCard}/div/div[1]/div/h6/a
    Wait Until Element Is Visible    ${VendorDetailVendorName_lbl}
    ${ActVendorName}    Get Text    ${VendorDetailVendorName_lbl}
    ${Status}    Should Contain    ${ActVendorName}    ${ExpVendorName}    ignore_case=False    strip_spaces=False
    ClickLink_rgp    ${MenuHome_btn}
    VerifyFirstCardIsDisplayed

TC_054 Validate the "Go to page" functionality by providing only number
    LoginToFirstVerify    admin
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    Clear Element Text    ${DashboardGoToPage_txt}
    Sleep    2s
    InputText_rgp    ${DashboardGoToPage_txt}    5
    VerifyFirstCardIsDisplayed
    ${PageSelected}    Get Element Attribute    //button[@aria-label="page 6"]    aria-current
    Run Keyword If    "${PageSelected}"!="true"    Fail    Page 20 is not selected

TC_055 Validate the "Go to page" functionality by providing Zero
    LoginToFirstVerify    admin
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    Clear Element Text    ${DashboardGoToPage_txt}
    Sleep    2s
    InputText_rgp    ${DashboardGoToPage_txt}    0
    VerifyFirstCardIsDisplayed
    ${PageSelected}    Get Element Attribute    //button[@aria-label="page 6"]    aria-current
    Run Keyword If    "${PageSelected}"!="true"    Fail    Page 20 is not selected

TC_056 Validate the "Go to page" functionality by providing number as beyond the page number
    LoginToFirstVerify    admin
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    InputText_rgp    ${DashboardGoToPage_txt}    500
    VerifyFirstCardIsDisplayed
    ${MaxPageNo}    Get Element Attribute    ${DashboardGoToPage_txt}    max
    ${locator}    Set Variable    //button[@aria-label="page ${MaxPageNo}"]
    ${PageSelected}    Get Element Attribute    ${locator}    aria-current
    Run Keyword If    "${PageSelected}"!="true"    Fail    Page ${MaxPageNo} is not selected

TC_058 Validate the Footer section in dashboard
    LoginToFirstVerify    admin
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    VerifyFooterSection

TC_059 Validate the User is able to view Greater than 30 Days widget
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardTitle_lbl}    Page Title Dashboard
    VerifyElementIsDisplayed    ${DashboardGreaterThan30DaysCard_btn}    Widget "Greater than 30 Days"

TC_060 Validate the User is able to view Within 30 Days widget
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardTitle_lbl}    Page Title Dashboard
    VerifyElementIsDisplayed    ${DashboardWithin30DaysCard_btn}    Widget "Within 30 Days"

TC_061 Validate the User is able to view Client Review widget
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardTitle_lbl}    Page Title Dashboard
    VerifyElementIsDisplayed    ${DashboardClientReviewCard_btn}    Widget "Client Review"

TC_062 Validate the User is able to view Yesterday widget
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardTitle_lbl}    Page Title Dashboard
    VerifyElementIsDisplayed    ${DashboardYesterdayCard_btn}    Widget "Yesterday"

TC_063 Validate the User is able to view Reminders widget
    LoginToFirstVerify    admin
    VerifyElementIsDisplayed    ${DashboardTitle_lbl}    Page Title Dashboard
    VerifyElementIsDisplayed    ${DashboardRemindersCard_btn}    Widget "Reminders"

TC_064 Validate the value displayed in the Greater than 30 Days widget is "0" or greater
    LoginToFirstVerify    admin
    ${Act_txt}    Get Text    ${DashboardGreaterThan30DaysValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0

TC_065 Validate the value displayed in the Within 30 Days widget is "0" or greater
    LoginToFirstVerify    admin
    ${Act_txt}    Get Text    ${DashboardWithin30DaysValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0

TC_066 Validate the value displayed in the Client Review widget is "0" or greater
    LoginToFirstVerify    admin
    ${Act_txt}    Get Text    ${DashboardClientReviewValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0

TC_067 Validate the value displayed in the Yesterday widget is "0" or greater
    LoginToFirstVerify    admin
    ${Act_txt}    Get Text    ${DashboardYesterdayValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0

TC_068 Validate the value displayed in the Reminders widget is "0" or greater
    LoginToFirstVerify    admin
    ${Act_txt}    Get Text    ${DashboardRemindersValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0

TC_069 Validate the value displayed in the Greater than 30 Days widget is matching with the cards displayed in the grid
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    ${Act_txt}    Get Text    ${DashboardGreaterThan30DaysValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Sleep    2s
    ${PageCount}    Get Element Count    //nav[@aria-label="pagination navigation"]/ul/li
    ${PageCount}    Run Keyword If    ${PageCount}>0    Evaluate    ${PageCount}-1
    ...    ELSE    Set Variable    0
    ${CardsCount}    Run Keyword If    ${PageCount}>0    GetCardsCount    ${PageCount}
    ...    ELSE    Set Variable    0
    Run Keyword If    ${Act_txt}!=${CardsCount}    Fail    GreaterThan30Days cards count is not matching with the widget count

TC_070 Validate the value displayed in the Within 30 Days widget is matching with the cards displayed in the grid
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    ${Act_txt}    Get Text    ${DashboardWithin30DaysValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Sleep    2s
    ${Count}    Get Element Count    //nav[@aria-label="pagination navigation"]/ul/li
    ${Count}    Run Keyword If    ${Count}>0    Evaluate    ${Count}-1
    ...    ELSE    Set Variable    0
    ${CardsCount}    Run Keyword If    ${Count}>0    GetCardsCount    ${Count}
    ...    ELSE    Set Variable    0
    Run Keyword If    ${Act_txt}!=${CardsCount}    Fail    Within30Days cards count is not matching with the widget count

TC_071 Validate the value displayed in the Client Review widget is matching with the cards displayed in the grid
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    ${Act_txt}    Get Text    ${DashboardClientReviewValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Sleep    2s
    ${Count}    Get Element Count    //nav[@aria-label="pagination navigation"]/ul/li
    ${Count}    Run Keyword If    ${Count}>0    Evaluate    ${Count}-1
    ...    ELSE    Set Variable    0
    ${CardsCount}    Run Keyword If    ${Count}>0    GetCardsCount    ${Count}
    ...    ELSE    Set Variable    0
    Run Keyword If    ${Act_txt}!=${CardsCount}    Fail    ClientReview cards count is not matching with the widget count

TC_072 Validate the value displayed in the Yesterday widget is matching with the cards displayed in the grid
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    ${Act_txt}    Get Text    ${DashboardYesterdayValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Sleep    2s
    ${Count}    Get Element Count    //nav[@aria-label="pagination navigation"]/ul/li
    ${Count}    Run Keyword If    ${Count}>0    Evaluate    ${Count}-1
    ...    ELSE    Set Variable    0
    ${CardsCount}    Run Keyword If    ${Count}>0    GetCardsCount    ${Count}
    ...    ELSE    Set Variable    0
    Run Keyword If    ${Act_txt}!=${CardsCount}    Fail    Yesterday cards count is not matching with the widget count

TC_073 Validate the value displayed in the Reminder widget is matching with the cards displayed in the grid
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    ${Act_txt}    Get Text    ${DashboardRemindersValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Sleep    2s
    ${Count}    Get Element Count    //nav[@aria-label="pagination navigation"]/ul/li
    ${Count}    Run Keyword If    ${Count}>0    Evaluate    ${Count}-1
    ...    ELSE    Set Variable    0
    ${CardsCount}    Run Keyword If    ${Count}>0    GetCardsCount    ${Count}
    ...    ELSE    Set Variable    0
    Run Keyword If    ${Act_txt}!=${CardsCount}    Fail    Reminder cards count is not matching with the widget count

TC_074 Validate the user is able to search the cards in Greater than 30 days
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Input Text    ${DashboardSearch_txt}    Aptus Group USA
    Press Keys    ${DashboardSearch_txt}    ENTER
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //a[text()="Aptus Group USA, LLC"]
    Run Keyword If    ${SearchStatus}!=True    Fail    Searched Vendor is not displayed

TC_075 Validate the user is able to search the cards in Within 30 days
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    Input Text    ${DashboardSearch_txt}    Building Zone Industries, LLC
    Press Keys    ${DashboardSearch_txt}    ENTER
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //a[text()="Building Zone Industries, LLC"]
    Run Keyword If    ${SearchStatus}!=True    Fail    Searched Vendor is not displayed

TC_076 Validate the user is able to search the cards in ClientReview
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Input Text    ${DashboardSearch_txt}    Paul Reilly Company Illinois Inc
    Press Keys    ${DashboardSearch_txt}    ENTER
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //a[text()="Paul Reilly Company Illinois Inc"]
    Run Keyword If    ${SearchStatus}!=True    Fail    Searched Vendor is not displayed

TC_077 Validate the user is able to search the cards in Reminders
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Input Text    ${DashboardSearch_txt}    Landwehr Construction, Inc.
    Press Keys    ${DashboardSearch_txt}    ENTER
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //a[text()="Landwehr Construction, Inc."]
    Run Keyword If    ${SearchStatus}!=True    Fail    Searched Vendor is not displayed

TC_078 Validate the user is able to search the cards in Yesterday
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    Input Text    ${DashboardSearch_txt}    Landwehr Construction, Inc.
    Press Keys    ${DashboardSearch_txt}    ENTER
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //a[text()="Landwehr Construction, Inc."]
    Run Keyword If    ${SearchStatus}!=True    Fail    Searched Vendor is not displayed

TC_079 Validate the error messaage is displayed when the searched card is not available
    LoginToFirstVerify    admin
    Input Text    ${DashboardSearch_txt}    test lishore
    Press Keys    ${DashboardSearch_txt}    ENTER
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in Greater than 30
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in within 30 days
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in Client review
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in Reminder
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in yesterday
    ClickElement_rgp    ${DashboardListGridView_btn}
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    Sleep    3s
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in Greater than 30
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    Sleep    3s
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in within 30 days
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    Sleep    3s
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in Client review
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    Sleep    3s
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in Reminder
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    Sleep    3s
    ${SearchStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Run Keyword If    ${SearchStatus}!=True    Fail    Cards should not be displayed for this search text in yesterday

TC_080 Validate Error message is displayed in each widget when there is no vendor available
    LoginToFirstVerify    admin
    SelectClient    Insurance Program
    ClickElement_rgp    ${DashboardGreaterThan30DaysValue_lbl}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardWithin30DaysCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardClientReviewCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardRemindersCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed
    ClickElement_rgp    ${DashboardYesterdayCard_btn}
    VerifyFirstCardIsDisplayed
    Run Keyword If    ${CardsStatus}!=True and ${NoRecordsFoundStatus}!= True    Fail    Cards or No Records Found error message should be displayed

TC_081 Validate the grid headers displayed in the List view
    LoginToFirstVerify    admin
    ClickElement_rgp    ${DashboardListGridView_btn}
    Wait Until Element Is Visible    ${DashboardVendorName_lbl}
    Wait Until Element Is Visible    ${DashboardClientName_lbl}
    Wait Until Element Is Visible    ${DashboardStatus_lbl}
    Wait Until Element Is Visible    ${DashboardSubmitted_lbl}
    Wait Until Element Is Visible    ${DashboardStatusPeriod_lbl}

TC_083 Validate the Global Client Selection Popup is displayed
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${PopupStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //h2[@id="responsive-dialog-title"]
    Run Keyword If    ${PopupStatus}!=True    Fail    Global Client Search Popup is not displayed

TC_084 Validate the "Save" button is displayed under Global dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${SaveBtnStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardSelectClientSave_btn}
    Run Keyword If    ${SaveBtnStatus}!=True    Fail    Save button is not displayed in Global Client Search Popup

TC_085 Validate the Cancel button is displayed under Global dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${CancelBtnStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardSelectClientCancel_btn}
    Run Keyword If    ${CancelBtnStatus}!=True    Fail    Cancel button is not displayed in Global Client Search Popup

TC_086 Validate add icon Fuctionality in the global client search dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${ClientName}    Set Variable    Calyco
    Sleep    2s
    ${Status}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardGlobalClientAddIcon_btn}
    Run Keyword If    ${Status}!=True    Fail    Add Icon is not displayed in global client search list
    ${AddFirstElementTxt}    Get Text    ${DashboardGlobalClientAddFirstelement_lbl}
    ClickElement_rgp    ${DashboardGlobalClientAddIcon_btn}
    ${Count}    Get Element Count    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div[@role="button"]
    ${DeleteFirstElementStatus}    Set Variable    False
    FOR    ${i}    IN RANGE    1    ${Count}+1
        ${actText}    Get Text    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div[@role="button"][${i}]/span
        ${DeleteFirstElementStatus}    Run Keyword And Return Status    Should Contain    ${AddFirstElementTxt}    ${actText}    ignore_case=True    strip_spaces=False
        Exit For Loop If    '${DeleteFirstElementStatus}'=='True'
    END
    Run Keyword If    ${DeleteFirstElementStatus}!=True    Fail    Client name is not moved to selected list after clicking the add button

TC_087 Validate Delete icon functionality in the global client search dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    Sleep    2s
    ${Status}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardGlobalClientDeleteIcon_btn}
    Run Keyword If    ${Status}!=True    Fail    Delete Icon is not displayed in global client search list
    ${DeleteFirstElementTxt}    Get Text    ${DashboardGlobalClientDeleteFirstelement_lbl}
    ClickElement_rgp    ${DashboardGlobalClientDeleteIcon_btn}
    Input Text    ${DashboardGlobalClientAddSearch_txt}    ${DeleteFirstElementTxt}
    Sleep    2s
    ${Count}    Get Element Count    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[1]/div[2]/ul/li
    ${AddFirstElementStatus}    Set Variable    False
    FOR    ${i}    IN RANGE    1    ${Count}+1
        ${actText}    Get Text    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[1]/div[2]/ul/li[${i}]/div[1]/div[1]/span
        ${AddFirstElementStatus}    Run Keyword And Return Status    Should Contain    ${DeleteFirstElementTxt}    ${actText}    ignore_case=True    strip_spaces=False
        Exit For Loop If    '${AddFirstElementStatus}'=='True'
    END
    Run Keyword If    ${AddFirstElementStatus}!=True    Fail    Client name is not moved to selected list after clicking the add button

TC_088 Validate the search text box is displayed above the selection list
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${ClientName}    Set Variable    Clayco
    Sleep    2s
    ${AddSearchTextStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardGlobalClientAddSearch_txt}
    Run Keyword If    ${AddSearchTextStatus}!=True    Fail    Search text box above the add list is not displayed

TC_089 Validate the search text box is displayed above the UnSelected list
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${ClientName}    Set Variable    Clayco
    Sleep    2s
    ${DeleteSearchTextStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardGlobalClientDeleteSearch_txt}
    Run Keyword If    ${DeleteSearchTextStatus}!=True    Fail    Search text box above the delete list is not displayed

TC_090 Validate user is able to search the selected client in global client search dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${ClientName}    Set Variable    Clayco
    Sleep    2s
    Input Text    ${DashboardGlobalClientAddSearch_txt}    ${ClientName}
    ${AddFirstElementStatus}    Run Keyword And Return Status    Get Text    ${DashboardGlobalClientAddFirstelement_lbl}
    ${AddFirstElementTxt}    Run Keyword If    '${AddFirstElementStatus}'=='True'    Get Text    ${DashboardGlobalClientAddFirstelement_lbl}
    Run Keyword If    '${AddFirstElementStatus}'=='True'    Should Contain    ${AddFirstElementTxt}    ${ClientName}    ignore_case=True    strip_spaces=False

TC_091 Validate user is able to search the UnSelected Client in global client search dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${ClientName}    Set Variable    Clayco
    Sleep    2s
    Input Text    ${DashboardGlobalClientDeleteSearch_txt}    ${ClientName}
    ${DeleteFirstElementStatus}    Run Keyword And Return Status    Get Text    ${DashboardGlobalClientDeleteFirstelement_lbl}
    ${DeleteFirstElementTxt}    Run Keyword If    '${DeleteFirstElementStatus}'=='True'    Get Text    ${DashboardGlobalClientDeleteFirstelement_lbl}
    Run Keyword If    '${DeleteFirstElementStatus}'=='True'    Should Contain    ${DeleteFirstElementTxt}    ${ClientName}    ignore_case=True    strip_spaces=False

TC_092 Validate the user is able to save the selected clients in the global dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${ClientName}    Set Variable    Clayco
    Sleep    2s
    ${Count}    Get Element Count    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div
    FOR    ${i}    IN RANGE    1    ${Count}+1
        Click Element    ${DashboardGlobalClientDeleteIcon_btn}
        Sleep    1s
    END
    Input Text    ${DashboardGlobalClientAddSearch_txt}    ${ClientName}
    Click Element    ${DashboardGlobalClientAddIcon_btn}
    Click Element    ${DashboardSelectClientSave_btn}
    ${ActClientName}    Get Text    ${DashboardFirstCard}/div/div[4]/span
    Should Contain    ${ActClientName}    ${ClientName}    ignore_case=False    strip_spaces=False

TC_093 Validate the user is able to cancel the chages made in global dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    ${ClientName}    Set Variable    Clayco
    Sleep    2s
    ${Count}    Get Element Count    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div
    FOR    ${i}    IN RANGE    1    ${Count}+1
        Click Element    ${DashboardGlobalClientDeleteIcon_btn}
        Sleep    1s
    END
    Click Element    ${DashboardSelectClientCancel_btn}
    ${ActClientName}    Get Text    ${DashboardFirstCard}/div/div[4]/span
    Should Contain    ${ActClientName}    ${ClientName}    ignore_case=False    strip_spaces=False

TC_094 Validate the error message is displayed when the user removes all the selected client and tries to save Global client search dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    Sleep    2s
    ${Count}    Get Element Count    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div
    ${Count}    Evaluate    ${Count}+1
    FOR    ${i}    IN RANGE    1    ${Count}
        ClickElement_rgp    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div[@role="button"][1]/*[2]
    END
    ClickElement_rgp    ${DashboardSelectClientSave_btn}
    ${ActErrorText}    Handle Alert
    Run Keyword If    '${ActErrorText}'!='Please select the client'    Fail    Please select the client error message should be displayed

TC_095 Validate the No. of clients selected is displayed in the global search dropdown
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    Sleep    2s
    ${Count}    Get Element Count    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div
    ${Count}    Evaluate    ${Count}-1
    ClickElement_rgp    //button[text()="Cancel"]
    ${Status}    Run Keyword And Return Status    ${DashboardClientSearch_txt}/parent::div/div/span
    Run Keyword If    '${Status}'!='False' and ${Count}>1    Fail
    ${ExpectedClientText}    Get Text    ${DashboardClientSearch_txt}/parent::div/div/span
    ${ActualClientText}    Set Variable If    ${Count}>1    +${Count} Client(s)    ${EMPTY}
    Run Keyword If    '${ExpectedClientText}'!='${ActualClientText}'    Fail    No. of clients displayed in the global client search dropdown is incorrect

TC_096 Validate the First Client name which is in the selected list is displayed in the global client search text box
    LoginToFirstVerify    admin
    Click Element    ${DashboardClientSearch_txt}
    Sleep    2s
    ${ExpClientName}    Get Text    ${DashboardGlobalClientDeleteFirstelement_lbl}
    Click Element    ${DashboardSelectClientCancel_btn}
    ${ActClientName}    Get Element Attribute    ${DashboardClientSearch_txt}    value
    Should Contain    ${ExpClientName}    ${ActClientName}    ignore_case=False    strip_spaces=False
