*** Settings ***
Suite Setup       Config
Suite Teardown
Test Teardown     Close All Browsers
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot

*** Test Cases ***
TC_001 Validate the First verify Logo is displayed in the forgot password screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ${LogoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${FirstverifyLogo_img}
    Run Keyword If    '${LogoStatus}'!='True'    Fail    FirstVerify Logo is not displayed

TC_002 Validate the Key icon is displayed in the forgot password screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ${KeyIconStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdKeyLogo_img}
    Run Keyword If    '${KeyIconStatus}'!='True'    Fail    Key Icon is not displayed

TC_003 Validate the Header text displayed in the forgot password screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ${HeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdHeading_span}
    Run Keyword If    '${HeadingStatus}'!='True'    Fail    Forget Password? is not displayed

TC_004 Validate the Sub Header text displayed in the forgot password screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ${SubHeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdSubHeading_span}
    Run Keyword If    '${SubHeadingStatus}'!='True'    Fail    No worries, we will send you reset instructions.is not displayed

TC_005 Validate the fields displayed in the forgot password Screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ${EmailStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdEmail_txt}
    Run Keyword If    '${EmailStatus}'!='True'    Fail    Email field is not displayed
    ${ResetPwdBtnStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${ForgetPwdResetPwd_btn}
    Run Keyword If    '${ResetPwdBtnStatus}'!='True'    Fail    Reset password button is not displayed/enabled
    ${BackToLoginBtnStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${ForgetPwdBackToLogin_btn}
    Run Keyword If    '${BackToLoginBtnStatus}'!='True'    Fail    Back to login button is not displayed/enabled

TC_006 Validate the Placeholder displayed in the email text box
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ${EmailStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdEmail_txt}
    Run Keyword If    '${EmailStatus}'!='True'    Fail    Email field Placeholder is incorrect

TC_007 Validate the error message is displayed when the email field is left blank
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    ${EMPTY}
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'!='True'    Fail    Error message should be displayed when the Email is blank

TC_008 Validate the error message is displayed when the email is invalid
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    deepakkishore@gmail
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'!='True'    Fail    Error message should be displayed when the Email is Invalid

TC_009 Validate the error message is displayed when the email format is valid and not registered in first verify
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    kishore.sdk@gmail.com
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'!='True'    Fail    Error message should be displayed when the Email is valid and not registered

TC_010 Validate the Check your mail screen is displayed when the user enter the valid email in forgot password screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    deepakkishore.07@gmail.com
    ${CheckYourEmailStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //span[text()="Check your email"]
    Run Keyword If    '${CheckYourEmailStatus}'!='True'    Fail    Check your email screen is not displayed

TC_011 Validate the first verify Logo is displayed in the Check your mail screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    deepakkishore.07@gmail.com
    ${LogoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${FirstverifyLogo_img}
    Run Keyword If    '${LogoStatus}'=='False'    Fail    FirstVerify Logo is not displayed

TC_012 Validate the mail icon is displayed in the Check your mail screen below the first verify logo
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    deepakkishore.07@gmail.com
    ${MailIconStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdEmailLogo_img}
    Run Keyword If    '${MailIconStatus}'!='True'    Fail    Mail Icon is not displayed

TC_013 Validate the header text displayed in the check you mail screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    deepakkishore.07@gmail.com
    ${HeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdEmailHeading_span}
    Run Keyword If    '${HeadingStatus}'!='True'    Fail    Check your email Heading is not displayed

TC_014 Validate the sub header text displayed in the check you mail screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ${Email}    Set Variable    deepakkishore.07@gmail.com
    ForgotPassword    ${Email}
    ${SubHeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //span[contains(text(),"We sent a password reset link to ${Email}")]
    Run Keyword If    '${SubHeadingStatus}'!='True'    Fail    Check your email SubHeading is not displayed/Incorrect

TC_015 Validate the fields displayed in the check you mail screen
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    deepakkishore.07@gmail.com
    ${OpenEmailAppBtnStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${ForgetPwdCheckYourEmail_btn}
    Run Keyword If    '${OpenEmailAppBtnStatus}'!='True'    Fail    Open Email app button is not displayed/enabled
    ${ResetBtnStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${ForgetPwdReset_lnk}
    Run Keyword If    '${ResetBtnStatus}'!='True'    Fail    Reset button is not displayed/enabled

TC_017 Validate the user is able to resend the email by clicking the Resend button
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    deepakkishore.07@gmail.com
    ClickElement_rgp    ${ForgetPwdReset_lnk}
    ${ResendStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled
    Run Keyword If    '${ResendStatus}'!='True'    Fail

TC_018 Validate the text displayed before the Click to Resend link
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ForgotPassword    deepakkishore.07@gmail.com
    ${ResendPrefixTextStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdReset_span}
    Run Keyword If    '${ResendPrefixTextStatus}'!='True'    Fail    Text displayed before the Resend button is incorret
