*** Settings ***
Suite Setup       Config
Suite Teardown
Test Teardown
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot

*** Test Cases ***
TC_001 Validate the first verify Logo is displayed in the login screen
    LaunchBrowser
    ${LogoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${FirstverifyLogo_img}
    Run Keyword If    '${LogoStatus}'=='False'    Fail    FirstVerify Logo is not displayed

TC_002 Validate the header text displayed in the login screen
    LaunchBrowser
    ${HeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHeading_lbl}
    Run Keyword If    '${HeadingStatus}'=='False'    Fail    Login to your account is not displayed

TC_003 Validate the Sub header text displayed in the login screen
    LaunchBrowser
    ${SubHeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginSubHeading_lbl}
    Run Keyword If    '${SubHeadingStatus}'=='False'    Fail    Welcome Back! Please enter your details is not displayed

TC_004 Validate the fields displayed in the Login Screen
    LaunchBrowser
    ${UserNameLabelStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${UserName_lbl}
    Run Keyword If    '${UserNameLabelStatus}'=='False'    Fail    Username Lable is not displayed
    ${UserNameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${UserName_txt}
    Run Keyword If    '${UserNameStatus}'=='False'    Fail    UserName Field is not displayed
    ${PasswordLabelStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${Password_lbl}
    Run Keyword If    '${PasswordLabelStatus}'=='False'    Fail    Password Label is not displayed
    ${PasswordStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${Password_txt}
    Run Keyword If    '${PasswordStatus}'=='False'    Fail    Password Field is not displayed
    ${LoginStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${Login_btn}
    Run Keyword If    '${LoginStatus}'=='False'    Fail    Login button is not displayed
    ${RememberMeLabelStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${Remember_lbl}
    Run Keyword If    '${RememberMeLabelStatus}'=='False'    Fail    Remember Me Label is not displayed
    ${RememberMeCheckBoxStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${RememberMe_ckb}
    Run Keyword If    '${RememberMeCheckBoxStatus}'=='False'    Fail    RememberMe Checkbox is not displayed
    ${ForgotPasswordStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgotPassword_btn}
    Run Keyword If    '${ForgotPasswordStatus}'=='False'    Fail    ForgotPassword is not displayed
    ${SignUpLabelStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${SignUp_lbl}
    Run Keyword If    '${SignUpLabelStatus}'=='False'    Fail    SignUp Label is not displayed
    ${SignUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${SignUp_btn}
    Run Keyword If    '${SignUpStatus}'=='False'    Fail    SignUp Button is not displayed
    ${NeedHelpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${NeedHelp_btn}
    Run Keyword If    '${NeedHelpStatus}'=='False'    Fail    NeedHelp Link is not displayed

TC_005 Validate the Placeholder displayed for the username field
    LaunchBrowser
    ${UserNameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${UserName_txt}
    Run Keyword If    '${UserNameStatus}'=='False'    Fail    UserName Placeholder is not displayed/Incorrect

TC_006 Validate the Placeholder displayed for the Password field
    LaunchBrowser
    ${PasswordStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${Password_txt}
    Run Keyword If    '${PasswordStatus}'=='False'    Fail    Password Placeholder is not displayed\Incorrect

TC_007 Validate the Remember me check box is not selected by default
    LaunchBrowser
    Sleep    10s
    Wait Until Element Is Visible    ${RememberMe_ckb}    timeout=5s
    Wait Until Element Is Enabled    ${RememberMe_ckb}    timeout=5s
    Select Checkbox    ${RememberMe_ckb}
    ${RememberMeCheckBoxStatus}    Run Keyword And Return Status    Checkbox Should Not Be Selected    ${RememberMe_ckb}
    Run Keyword If    '${RememberMeCheckBoxStatus}'=='False'    Fail    RememberMe Checkbox should not selected

TC_008 Validate the user is able to navigate to forgot password page while clicking the forgot password link
    LaunchBrowser
    ClickButton_rgp    ${ForgotPassword_btn}
    ${ForgotPasswordScreenStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${ForgetPwdHeading_span}
    Run Keyword If    '${ForgotPasswordScreenStatus}'=='False'    Fail    Forgot Password Screen is not displayed

TC_009 Validate the pre text displayed before the sign up link
    LaunchBrowser
    ${SignUpLabelStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${SignUp_lbl}
    Run Keyword If    '${SignUpLabelStatus}'=='False'    Fail    Sign Up Lable is not displayed\Incorrect

TC_010 Validate the user is able to navigate to Registration page while clicking the Sing Up link
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ${SignUpScreenStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${RegistrationHeading_lbl}
    Run Keyword If    '${SignUpScreenStatus}'=='False'    Fail    Registration Page is not displayed

TC_011 Validate the login Help popup is displayed while clicking the Need Login Help link
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    ${LoginHelpScreenStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpHeader_txt}
    Run Keyword If    '${LoginHelpScreenStatus}'=='False'    Fail    Login Help Pop Up is not displayed

TC_012 Validate the username field is mandatory and error message is displayed by submitting with blank username
    LoginToFirstVerify    Test    ${EMPTY}    Testing#123
    ${Status}    Run Keyword And Return Status    Get Text    //span[text()="User name is required !"]
    Run Keyword If    '${Status}'!='True'    Fail    User Name is required error message should be displayed

TC_013 Validate the Password field is mandatory and error message is displayed by submitting with blank Password
    LoginToFirstVerify    Test    FVTestingTeam@ahaapps.com    ${EMPTY}
    ${Status}    Run Keyword And Return Status    Get Text    //span[text()="Password is required !"]
    Run Keyword If    '${Status}'!='True'    Fail    Password is required error message should be displayed

TC_014 Validate the error message is displayed when the User name and password is left blank
    LoginToFirstVerify    Test    ${EMPTY}    ${EMPTY}
    ${Status}    Run Keyword And Return Status    Get Text    //span[text()="User name is required !"]
    Run Keyword If    '${Status}'!='True'    Fail    User Name is required error message should be displayed
    ${Status}    Run Keyword And Return Status    Get Text    //span[text()="Password is required !"]
    Run Keyword If    '${Status}'!='True'    Fail    Password is required error message should be displayed

TC_015 Validate the User is not able to login and the error message is displayed when the username is invalid and the password is valid
    LoginToFirstVerify    Test    FVTesting@ahaapps.com    Testing#123
    Comment    Enter the validation script for error message

TC_016 Validate the User is not able to login and the error message is displayed when the username is valid and the password is Invalid
    LoginToFirstVerify    Test    FVTestingTeam@ahaapps.com    Testing#1234
    Comment    Enter the validation script for error message

TC_017 Validate the User is not able to login and the error message is displayed when the username is invalid and the password is Invalid
    LoginToFirstVerify    Test    FVTestingTeam1@ahaapps.com    Testing#1234
    Comment    Enter the validation script for error message

TC_018 Validate the User is able to navigate to admin Dashboard screen by giving the admin user credentials
    LoginToFirstVerify    admin
    ${Status}    Run Keyword And Return Status    Get Text    //h2[text()="Dashboard"]
    Run Keyword If    '${Status}'!='True'    Fail    Dashboard is not displayed

TC_019 Validate the User is able to navigate to Vendor Dashboard screen by giving the Vendor user credentials
    LoginToFirstVerify    vendor
    Comment    Enter the validation script for error message

TC_020 Validate the User is able to navigate to Client Dashboard screen by giving the Client user credentials
    LoginToFirstVerify    client
    Comment    Enter the validation script for error message

TC_021 Validate the User is able to navigate to Employee Dashboard screen by giving the Employee user credentials
    LoginToFirstVerify    employee
    ${Status}    Run Keyword And Return Status    Get Text    //h2[text()="Dashboard"]
    Run Keyword If    '${Status}'!='True'    Fail    Dashboard is not displayed

TC_022 Validate the Username is prepopulated in second time login when the remember me checkbox is selected during the first login
    LoginToFirstVerify    admin
    Comment    Enter the validation script for error message
