*** Settings ***
Suite Setup       Config
Suite Teardown
Test Teardown     Close All Browsers
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot

*** Test Cases ***
TC_001 Validate the Company registration screen is displayed when the user clicks the Company Registration for Prequalification
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${CompanyRegStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegHeading_lbl}
    Run Keyword If    '${CompanyRegStatus}'=='False'    Fail    Company Registration is not displayed/Incorrect

TC_002 Validate the first verify logo is displayed in the company registration screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${LogoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${FirstverifyLogo_img}
    Run Keyword If    '${LogoStatus}'=='False'    Fail    FirstVerify Logo is not displayed

TC_003 Validate the header text displayed in the company registration screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${HeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegHeading_lbl}
    Run Keyword If    '${HeadingStatus}'=='False'    Fail    Company Registration Header is not displayed

TC_004 Validate the navigation menu displayed in the company registration screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${BusinessInfoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegBusinessInfo_mnu}
    Run Keyword If    '${BusinessInfoStatus}'=='False'    Fail    Business Information Menu is not displayed
    ${AddressInfoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegAddressInfo_mnu}
    Run Keyword If    '${AddressInfoStatus}'=='False'    Fail    Address Information Menu is not displayed
    ${ContactInfoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegContactInfo_mnu}
    Run Keyword If    '${ContactInfoStatus}'=='False'    Fail    Contact Information Menu is not displayed
    ${PrimaryInfoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegPrimaryUserInfo_mnu}
    Run Keyword If    '${PrimaryInfoStatus}'=='False'    Fail    Primary User Information Menu is not displayed

TC_006 Validate the fields displayed in the Business Information Screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${InternationalRbtnStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${CompRegTypeInternational_btn}
    Run Keyword If    '${InternationalRbtnStatus}'=='False'    Fail    Internationa Radio Button is not enabled
    ${USRbtnStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${CompRegTypeUS_btn}
    Run Keyword If    '${USRbtnStatus}'=='False'    Fail    US Radio Button is not enabled
    ${LegalNameofBusinessStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegLegalNameOfBusiness_txt}
    Run Keyword If    '${LegalNameofBusinessStatus}'=='False'    Fail    Legal Name of Business is not displayed
    ${TaxIdStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegTaxID_txt}
    Run Keyword If    '${TaxIdStatus}'=='False'    Fail    Tax ID is not displayed
    ${CompPrincipalOffNameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegComPrincipalOfficerName_txt}
    Run Keyword If    '${CompPrincipalOffNameStatus}'=='False'    Fail    Company Principal Officer Name is not displayed

TC_007 Validate the US Radio group is selected by default
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${UsRbtnIsSelected}    Get Element Attribute    ${CompRegTypeUS_btn}    aria-pressed
    Run Keyword If    '${UsRbtnIsSelected}'=='False'    Fail    US Is not selected By Default

TC_008 Validate the International Radio group is selected when the user clicks
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ClickButton_rgp    ${CompRegTypeInternational_btn}
    ${UsRbtnIsSelected}    Get Element Attribute    ${CompRegTypeInternational_btn}    aria-pressed
    Run Keyword If    '${UsRbtnIsSelected}'=='False'    Fail    International Is not selected

TC_009 Validate the Placeholder displayed for the field Legal Name of Business
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${CompRegLegalNameOfBusiness_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Legal Name of Business'    Fail    Placeholder for Legal Name of Business is incorrect

TC_010 Validate the error message is displayed when the Legal Name of Business is left as empty
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    ${EMPTY}    123    123
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //input[@placeholder="Legal Name of Business" and @aria-invalid="error"]
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Legal Name of Business is left blank

TC_011 Validate the Placeholder displayed for the field TAX ID
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${CompRegTaxID_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='TAX ID'    Fail    Placeholder for TaxID is incorrect

TC_012 Validate the error message is displayed when the TAX ID is left as empty
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    ${EMPTY}    123
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //input[@placeholder="TAX ID" and @aria-invalid="error"]
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Tax ID is left blank

TC_013 Validate the Placeholder displayed for the field Company Principal Officer Name
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ${PlaceHolder}    Get Element Attribute    ${CompRegTaxID_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Company Principal Officer Name'    Fail    Placeholder for Company Principal Officer Name is incorrect

TC_014 Validate the error message is displayed when the Company Principal Officer Name is left as empty
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    ${EMPTY}
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //input[@placeholder="Company Principal Officer Name" and @aria-invalid="error"]
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Company Principal Officer Name is left blank

TC_016 Validate the address Info screen is displayed
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    ${AddressInfoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegAddress1_txt}
    Run Keyword If    '${AddressInfoStatus}'=='False'    Fail    Address Info Screen is not displayed

TC_018 Validate the Placeholder text displayed in the address line 1 field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    ${PlaceHolder}    Get Element Attribute    ${CompRegAddress1_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Address Line 1'    Fail    Placeholder for Address Line 1 is incorrect

TC_019 Validate the error message is displayed when the address field 1 is empty
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    ${EMPTY}    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    United States    ${EMPTY}    English    International
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Address Line 1 is left blank

TC_024 Validate the user is able to select the country from the country dropdown
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International

TC_027 Validate the error message is displayed when the country is not selected
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    ${EMPTY}    ${EMPTY}    English    International
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Address Line 1 is left blank

TC_028 Validate the user is able to select the State and Province from the State and Province dropdown
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    Tamil Nadu    English    International

TC_030 Validate the error message is displayed when the State and Province is not selected
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the State is not selected

TC_031 Validate the Placeholder text displayed in the Zip/Postal Code field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    ${PlaceHolder}    Get Element Attribute    ${CompRegZip/PostalCode_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Zip/Postal Code'    Fail    Placeholder for Zip/PostalCode is incorrect

TC_032 Validate the error message is displayed when the Zip/Postal Code field is empty
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    ${EMPTY}    United States    ${EMPTY}    English    International
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the PostalCode is left blank

TC_033 Validate the error message is displayed when the user enters other than number in Zip/Postal Code field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    abcd    United States    ${EMPTY}    English    International
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the PostalCode accepts other than numbers

TC_034 Validate the Placeholder text displayed in the City field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    ${PlaceHolder}    Get Element Attribute    ${CompRegCity_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Zip/Postal Code'    Fail    Placeholder for Zip/PostalCode is incorrect

TC_035 Validate the error message is displayed when the City field is empty
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    ${EMPTY}    ${EMPTY}    United States    ${EMPTY}    English    International
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the City is left blank

TC_036 Validate the error message is displayed when the user enters other than alphabets in City field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur123$    641603    United States    ${EMPTY}    English    International
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the City accepts other than Alphabets

TC_037 Validate the user is able to select the Select Language from the Select Language dropdown
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    United States    ${EMPTY}    Spanish    International

TC_038 Validate the English is selected as default to the Select Language field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    ${PlaceHolder}    Get Text    ${CompRegLanguage_lst}
    Run Keyword If    '${PlaceHolder}'!='English'    Fail    English is not displayed as default in select language

TC_039 Validate the user is able to select the Vendor Operating Region from the Vendor Operating Region dropdown
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    Tamil Nadu    English    International

TC_041 Validate the no error message is displayed when the Vendor Operating Region is not selected
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    Tamil Nadu    English    ${EMPTY}

TC_042 Validate the user is able to navigate back to the business information screen by clicking Back button
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    ClickButton_rgp    ${CompRegBack_lnk}
    ${LegalNameofBusinessStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegLegalNameOfBusiness_txt}
    Run Keyword If    '${LegalNameofBusinessStatus}'=='False'    Fail    Business Information screen is not displayed

TC_043 Validate all the information are prepopulated in Business information screen while navigating back from the address info screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    ClickButton_rgp    ${CompRegBack_lnk}

TC_044 Validate the Contact Info screen is displayed by clicking the next button from address info screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    ${PhoneNumberStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_PhoneNumber_txt}
    Run Keyword If    '${PhoneNumberStatus}'=='False'    Fail    Contact Information Page is not displayed

TC_047 Validate the fields displayed in the contact info screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    ${PhoneNumberStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_PhoneNumber_txt}
    Run Keyword If    '${PhoneNumberStatus}'=='False'    Fail    Phone Number Field is not displayed
    ${ExtNumberStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_ExtNumber_txt}
    Run Keyword If    '${ExtNumberStatus}'=='False'    Fail    Ext Number field is not displayed
    ${FaxNumberStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_FaxNumber_txt}
    Run Keyword If    '${FaxNumberStatus}'=='False'    Fail    Fax Number field is not displayed
    ${WebsiteUrlStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_WebsiteURL_txt}
    Run Keyword If    '${WebsiteUrlStatus}'=='False'    Fail    Website URL field is not displayed

TC_049 Validate the Phone number format is displayed as the place holder for the phone number field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    ${PlaceHolder}    Get Element Attribute    ${CompanyReg_PhoneNumber_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!=''    Fail    Placeholder for PhoneNumber is incorrect

TC_050 Validate the error message is displayed when the phone number is less then 10 digit
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    123456    044    123456    www.google.com
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Phonenumber length is less than 10 digit

TC_051 Validate the error message is displayed when the phone number is greater then 10 digit
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    12345678901    044    123456    www.google.com
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Phonenumber length is less than 10 digit

TC_053 Validate error message is displayed when the phone number is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    ${EMPTY}    044    123456    www.google.com
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Phone Number is mandatory error message should be displayed

TC_056 Validate the Phone Extension format is displayed as the place holder for the phone Extension field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    ${PlaceHolder}    Get Element Attribute    ${CompanyReg_ExtNumber_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!=''    Fail    Placeholder for Phone Extention is incorrect

TC_061 Validate the Fax Number format is displayed as the place holder for the Fax Number field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    ${PlaceHolder}    Get Element Attribute    ${CompanyReg_FaxNumber_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!=''    Fail    Placeholder for Fax Number is incorrect

TC_063 Validate error message is displayed when the Fax Number is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Fax Number is mandatory error message should be displayed

TC_066 Validate the Website URL format is displayed as the place holder for the Website URL field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    ${PlaceHolder}    Get Element Attribute    ${CompanyReg_WebsiteURL_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!=''    Fail    Placeholder for PhoneNumber is incorrect

TC_067 Validate the user is able to navigate back to the address info screen by clicking back button from contact info screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    ${EMPTY}    044    123456    www.google.com
    ClickButton_rgp    ${CompRegBack_lnk}
    ${AddressInfoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompRegAddress1_txt}
    Run Keyword If    '${AddressInfoStatus}'=='False'    Fail    Address Info Screen is not displayed

TC_068 Validate the Primary user information screen is displayed when the user clicks the next button from the contact info screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${FirstNameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_FirstName_txt}
    Run Keyword If    '${FirstNameStatus}'=='False'    Fail    Primary User Information screen is not displayed

TC_071 Validate the fields displayed in the personal user information screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${FirstNameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_FirstName_txt}
    Run Keyword If    '${FirstNameStatus}'=='False'    Fail    FirstName is not displayed
    ${LastNameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_LastName_txt}
    Run Keyword If    '${LastNameStatus}'=='False'    Fail    LastName is not displayed
    ${TitleStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_ContactTitle_txt}
    Run Keyword If    '${TitleStatus}'=='False'    Fail    Title is not displayed
    ${MobileNumberStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_MobileNumber_txt}
    Run Keyword If    '${MobileNumberStatus}'=='False'    Fail    MobileNumber is not displayed
    ${EmailStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_Email_txt}
    Run Keyword If    '${EmailStatus}'=='False'    Fail    Email is not displayed
    ${PasswordStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_Password_txt}
    Run Keyword If    '${PasswordStatus}'=='False'    Fail    Password is not displayed
    ${ConfirmPasswordStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyReg_ConfirmPassword_txt}
    Run Keyword If    '${ConfirmPasswordStatus}'=='False'    Fail    Confirm Password is not displayed
    ${AgreeBtnStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${CompanyReg_Agree_ckb}
    Run Keyword If    '${AgreeBtnStatus}'=='False'    Fail    I Agree field is not displayed

TC_070 Validate the Placeholder text displayed for the first name field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${PlaceHolder}    Get Element Attribute    ${CompanyReg_FirstName_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='FirstName'    Fail    Placeholder for FirstName is incorrect

TC_071 Validate the error message is displayed when the user enters other than alphabets in first name field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    123    Kishore    MR    1234567890    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the first name contains otherthan alphabets

TC_072 Validate the error message is displayed when the first name is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    ${EMPTY}    Kishore    MR    1234567890    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the first name is empty

TC_073 Validate the Placeholder text displayed for the Last Name field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${PlaceHolder}    Get Element Attribute    ${CompRegLastName_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='LastName'    Fail    Placeholder for LastName is incorrect

TC_074 Validate the error message is displayed when the user enters other than alphabets in Last Name field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    123    MR    1234567890    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Last name contains otherthan alphabets

TC_075 Validate the error message is displayed when the Last Name is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    ${EMPTY}    MR    1234567890    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Last name is empty

TC_076 Validate the Placeholder text displayed for the Title field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${PlaceHolder}    Get Element Attribute    ${CompRegTitle_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='Title'    Fail    Placeholder for Title is incorrect

TC_077 Validate the error message is displayed when the user enters other than alphabets in Title field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    123    1234567890    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Title contains otherthan alphabets

TC_077 Validate the error message is displayed when the title is empty
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    ${EMPTY}    1234567890    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Last name is empty

TC_079 Validate the Phone number format is displayed as the place holder for the phone number field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${PlaceHolder}    Get Element Attribute    ${CompRegPhoneNumber_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='a'    Fail    Placeholder for PhoneNumber is incorrect

TC_080 Validate the error message is displayed when the phone number is less then 10 digit
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    12345678    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the phone Number is less than 10 digit

TC_081 Validate the error message is displayed when the phone number is greater then 10 digit
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    12345678901    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the phone Number is greater than 10 digit

TC_083 Validate error message is displayed when the phone number is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    ${EMPTY}    deepakkishore.07@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the phone Number is empty

TC_085 Validate the Placeholder text displayed for the Email Address field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${PlaceHolder}    Get Element Attribute    ${CompRegEmailAddress_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='a'    Fail    Placeholder for EmailAddress is incorrect

TC_086 Validate the error message is displayed when the user enters invalid email id
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@ok    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Email Address is invalid

TC_087 Validate the error message is displayed when the Email is left as blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    ${EMPTY}    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Email Address is empty

TC_088 Validate the Placeholder displayed for the Password field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${PlaceHolder}    Get Element Attribute    ${CompRegPassword_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='a'    Fail    Placeholder for Password is incorrect

TC_089 Validate the Placeholder displayed for the Confirm Password field
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ${PlaceHolder}    Get Element Attribute    ${CompRegConfirmPassword_txt}    placeholder
    Run Keyword If    '${PlaceHolder}'!='a'    Fail    Placeholder for Confirm Password is incorrect

TC_090 Validate the error message is displayed when both the password and the confirm password is blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    ${EMPTY}    ${EMPTY}    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the confirm password and Password is blank

TC_091 Validate the error message is displayed when the password \ is blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    ${EMPTY}    asd    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Password is blank

TC_092 Validate the error message is displayed when the Confirm Password \ is blank
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    asd    ${EMPTY}    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Confirm Password is blank

TC_093 Validate the error message is displayed when the Password and Confirm password are not matching
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    asdf    asd    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Password and Confirm Password is not matching

TC_094 Validate the error message is displayed when the Password does not meet the Password criterial
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    asdf    asdf    true
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Password and Confirm Password doesnot match the criteria

TC_095 Validate the error message is displayed when the user tries to create an account without selecting the I agree checkbox
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    asdf    asdf    false
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Password and Confirm Password doesnot match the criteria

TC_096 Validate the user is created once the company registration is completed
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    asdf    asdf    True
    #Validate the success message is displayed

TC_097 Validate the existing information are prepopulated when the user clicks the back button
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    asdf    asdf    True
    #Validate the success message is displayed
    ClickButton_rgp    ${CompRegBack_lnk}

TC_099 Validate the user is able to click the Back to Login link at any stage
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    ClickElement_rgp    ${CompRegBackToLogin_btn}
    #Scenario 2
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    ClickElement_rgp    ${CompRegBackToLogin_btn}
    #Scenario 3
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    ClickElement_rgp    ${CompRegBackToLogin_btn}
    #Scenario 4
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    ClickElement_rgp    ${CompRegBackToLogin_btn}
    #Scenario 5
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${CompanyRegistration_btn}
    CompanyRegistrationBusinessInformation    True    123    123    123
    CompanyRegistrationAddressInformation    Test    False    ${EMPTY}    False    ${EMPTY}    Tirupur    641603    India    ${EMPTY}    English    International
    CompanyRegistrationContactInformation    1234567890    044    ${EMPTY}    www.google.com
    CompanyRegistrationPrimaryUserInformation    Deepak    Kishore    Mr    1234567890    deepak@gmail.com    asdf    asdf    True
    ClickElement_rgp    ${CompRegBackToLogin_btn}
