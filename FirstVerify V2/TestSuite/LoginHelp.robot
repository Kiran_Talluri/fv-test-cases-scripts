*** Settings ***
Suite Setup       Config
Suite Teardown
Test Teardown     Close All Browsers
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot

*** Test Cases ***
TC_001 Validate the user is able to navigate to Login Help
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    ${HeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpHeader_txt}
    Run Keyword If    '${HeadingStatus}'!='True'    Fail    Login Help Popup is not displayed

TC_002 Validate the fields displayed in Login Help popup
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    ${NameStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpName_txt}
    Run Keyword If    '${NameStatus}'!='True'    Fail    Name Field is not displayed
    ${CountryDDStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpSelectCountry_dd}
    Run Keyword If    '${CountryDDStatus}'!='True'    Fail    Country Dropdown is not displayed
    ${PhoneNumberStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpPhoneNumber_txt}
    Run Keyword If    '${PhoneNumberStatus}'!='True'    Fail    PhoneNumber is not displayed
    ${EmailStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpEmailAddress_txt}
    Run Keyword If    '${EmailStatus}'!='True'    Fail    Email Field is not displayed
    ${SubjectTypeStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpSubjectType_txt}
    Run Keyword If    '${SubjectTypeStatus}'!='True'    Fail    Subject Type DropDown is not displayed
    ${CommentStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpComments_txt}
    Run Keyword If    '${CommentStatus}'!='True'    Fail    Comment is not displayed
    ${SendRequestStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${LoginHelpSendRequest_btn}
    Run Keyword If    '${SendRequestStatus}'!='True'    Fail    Send Request button is not displayed
    ${CloseStatus}    Run Keyword And Return Status    Wait Until Element Is Enabled    ${LoginHelpClose_btn}
    Run Keyword If    '${CloseStatus}'!='True'    Fail    Close button is not displayed

TC_003 Validate the mandatory fields are displayed with the asterisk(*) symbol
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    ${NameAstrStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //span[text()="Name"]/span
    Run Keyword If    '${NameAstrStatus}'!='True'    Fail    Astrick is not displayed for Name
    ${PhoneAstrStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //span[text()="Phone Number"]/span
    Run Keyword If    '${PhoneAstrStatus}'!='True'    Fail    Astrick is not displayed for Phone Number
    ${EmailAstrStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //span[text()="Email Address"]/span
    Run Keyword If    '${EmailAstrStatus}'!='True'    Fail    Astrick is not displayed for Email
    ${SubjectTypeAstrStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //span[text()="Subject Type"]/span
    Run Keyword If    '${SubjectTypeAstrStatus}'!='True'    Fail    Astrick is not displayed for Subject Type

TC_004 Validate the error message is displayed when the name field is left blank
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    ${EMPTY}    India    1234567890    deepakkishore.07@gmail.com    Option1    Comments
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Name is left blank

TC_005 Validate the error message is displayed when the name field is invalid
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    123    India    1234567890    deepakkishore.07@gmail.com    Option1    Comments
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Name is left blank
    LoginHelp    !23_    India    1234567890    deepakkishore.07@gmail.com    Option1    Comments
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Name is left blank

TC_007 Validate the error message is displayed when the phone field is left blank
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    Deepak    India    ${EMPTY}    deepakkishore.07@gmail.com    Option1    Comments
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the PhoneNumber is left blank

TC_008 Validate the error message is displayed when the phone number is invalid
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    Deepak    India    bac    deepakkishore.07@gmail.com    Option1    Comments
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the PhoneNumber is invalid

TC_009 Validate the error message is displayed when the Email Address field is left blank
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    Deepak    India    1234567890    ${EMPTY}    Option1    Comments
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Email is left blank

TC_010 Validate the error message is displayed when the Email id is invalid
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    Deepak    India    1234567890    deepakkishore.07@gmail    Option1    Comments
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the Email is Invalid

TC_011 Validate the error message is displayed when the Subject Type field is left blank
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    Deepak    India    1234567890    deepakkishore.07@gmail.com    ${EMPTY}    Comments
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='False'    Fail    Error message should be displayed when the SubjectType is left blank

TC_012 Validate the error message is not displayed when the comment field is left blank
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    Deepak    India    1234567890    deepakkishore.07@gmail.com    Option1    ${EMPTY}
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='True'    Fail    Error message should Not be displayed when the comment is left blank

TC_013 Validate the comment field accepts special character, numbers and alphabets
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    Deepak    India    1234567890    deepakkishore.07@gmail.com    Option1    Deepak@kishore@25.1992
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='True'    Fail    Error message should Not be displayed when the comment Contains special char, numbers and alphabets
    ${Message}    Handle Alert
    Run Keyword If    '${Message}'!='request sent successfully'    Fail    Success message is not displayed

TC_014 Validate the request is send to the first verify when the user clicks the send request button
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    LoginHelp    Deepak    India    1234567890    deepakkishore.07@gmail.com    Option1    This is a comment
    ${ErrPopUpStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${MandatoryErrMsg_div}
    Run Keyword If    '${ErrPopUpStatus}'=='True'    Fail    Error message should Not be displayed when the comment Contains special char, numbers and alphabets
    ${Message}    Handle Alert
    Run Keyword If    '${Message}'!='request sent successfully'    Fail    Success message is not displayed

TC_015 Validate the Popup is closed when cancel button is clicked
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    InputText_rgp    ${LoginHelpName_txt}    Deepak
    ClickElement_rgp    \\li[text()='India')]
    InputText_rgp    ${LoginHelpPhoneNumber_txt}    1234567890
    InputText_rgp    ${LoginHelpEmailAddress_txt}    deepakkishore.07@gmail.com
    ClickElement_rgp    \\li[text()='Option1')]
    InputText_rgp    ${LoginHelpComments_txt}    This is a comment
    ClickButton_rgp    ${LoginHelpClose_btn}
    ${NameAstrStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    //span[text()="Name"]/span
    Run Keyword If    '${NameAstrStatus}'!='True'    Fail    Login Help Popup should not be displayed

TC_016 Validate the Header displayed in the Login Help
    LaunchBrowser
    ClickButton_rgp    ${NeedHelp_btn}
    ${HeaderStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHelpHeader_txt}
    Run Keyword If    '${HeaderStatus}'!='True'    Fail    Login Help Header should be displayed
