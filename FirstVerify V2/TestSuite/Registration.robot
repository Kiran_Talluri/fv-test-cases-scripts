*** Settings ***
Suite Setup       Config
Suite Teardown
Test Teardown     Close All Browsers
Resource          ../Resources/Properties.robot
Resource          ../Resources/Pages/LoginAndRegistrationPage.robot
Resource          ../Resources/Pages/LoginAndRegistrationPageObject.robot

*** Test Cases ***
TC_001 Validate the Registration page is displayed when the sign up link is clicked
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ${SignUpScreenStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${RegistrationHeading_lbl}
    Run Keyword If    '${SignUpScreenStatus}'=='False'    Fail    Registration Page is not displayed

TC_002 Validate the first verify Logo is displayed in the registration page
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ${LogoStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${FirstverifyLogo_img}
    Run Keyword If    '${LogoStatus}'=='False'    Fail    FirstVerify Logo is not displayed

TC_003 Validate the Header text displayed in registration page
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ${HeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${RegistrationHeading_lbl}
    Run Keyword If    '${HeadingStatus}'=='False'    Fail    Login to your account is not displayed

TC_004 Validate the Sub Header text displayed in registration page
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ${SubHeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${RegistrationSubHeading_lbl}
    Run Keyword If    '${SubHeadingStatus}'=='False'    Fail    Welcome Back! Please enter your details is not displayed

TC_005 Validate the icon displayed in Company Registration for
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ${CompanyRegStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyRegistration_btn}
    Run Keyword If    '${CompanyRegStatus}'=='False'    Fail    Company Registration is not displayed/Incorrect

TC_006 Validate the icon displayed in Individual Registration for Training
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ${IndividualRegStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndividualRegistration_btn}
    Run Keyword If    '${IndividualRegStatus}'=='False'    Fail    Individual Registration is not displayed/Incorrect

TC_007 Validate the buttons displayed in the Registration screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ${CompanyRegStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CompanyRegistration_btn}
    Run Keyword If    '${CompanyRegStatus}'=='False'    Fail    Company Registration is not displayed/Incorrect
    ${IndividualRegStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${IndividualRegistration_btn}
    Run Keyword If    '${IndividualRegStatus}'=='False'    Fail    Individual Registration is not displayed/Incorrect
    ${BackToLoginStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${BackToLogin_btn}
    Run Keyword If    '${BackToLoginStatus}'=='False'    Fail    Back To Login Button is not displayed

TC_008 Validate the Login screen is displayed when the Back to Login link is clicked in registration screen
    LaunchBrowser
    ClickButton_rgp    ${SignUp_btn}
    ClickElement_rgp    ${BackToLogin_btn}
    ${HeadingStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${LoginHeading_lbl}
    Run Keyword If    '${HeadingStatus}'=='False'    Fail    Login Page is not displayed
