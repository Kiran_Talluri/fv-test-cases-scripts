*** Settings ***
Library           Selenium2Library
Resource          ../ReusableGroup.robot
Resource          DashboardPageObject.robot
Library           OperatingSystem
Library           DateTime

*** Keywords ***
GetPQLocations
    [Arguments]    ${PqID}
    ${Wrapper}    Set Variable    /api/Prequalification/PQLocations?pqId=${PqID}
    ${JsonObj_PqSections_Res}    GetFVRequest    ${Wrapper}
    [Return]    ${JsonObj_PqSections_Res}

GetPQSections
    [Arguments]    ${PqID}
    ${Wrapper}    Set Variable    /api/Prequalification/PqSections?pqId=${PqID}
    ${JsonObj_PqSections_Res}    GetFVRequest    ${Wrapper}
    [Return]    ${JsonObj_PqSections_Res}

GetPqSubSections
    [Arguments]    ${PqID}    ${SectionID}
    #PqSubSections
    ${Wrapper}    Set Variable    /api/Prequalification/PqSubSections?pqId=${PqID}&sectionId=${SectionID}
    ${JsonObj_PqSubSections_Res}    GetFVRequest    ${Wrapper}
    [Return]    ${JsonObj_PqSubSections_Res}

GetSectionOrSubSectionDetails
    [Arguments]    ${PqID}    ${SectionID}    ${SectionType}    ${subSectionTypeCondition}    ${SubSectionId}
    #SectionOrSubSectionDetails
    ${Wrapper}    Set Variable    /api/Prequalification/SectionOrSubSectionDetails?pqId=${PqID}&sectionId=${SectionID}&sectionType=${SectionType}&subSectionTypeCondition=${subSectionTypeCondition}&subSectionId=${SubSectionId}
    ${JsonObj_SectionOrSubSectionDetails_Res}    GetFVRequest    ${Wrapper}
    [Return]    ${JsonObj_SectionOrSubSectionDetails_Res}

Verify the Checkbox questions
    [Arguments]    ${QuestionId}    ${Type}    ${QuestionOptions}    ${QuestionValue}    ${FieldName}
    ${Options}    Split String    ${QuestionOptions}    |
    ${OptionsCount}    Get Length    ${Options}
    Click Element    //label[text()="${FieldName}"]/../..//input/..
    FOR    ${i}    IN RANGE    0    ${OptionsCount}
        ${Option}    Set Variable    ${Options}[${i}]
        Comment    Run Keyword If    "${QuestionValue}"=="${Option}"    Wait Until Element Is Visible    //label[text()="${FieldName}"]/../..//input[@Value="${Option}"]
        VerifyElementIsDisplayed    //li[@data-value="${Option}" and @role="option"]    ${FieldName} :- "${Options}"
        ${isChecked}    Get Element Attribute    //li[@data-value="${Option}"]/span/input[@type="checkbox"]    checked
    END
    Click Element    //li[@data-value="${Option}" and @role="option"]/ancestor::ul/div/button

Verify the SubSubSection Details
    [Arguments]    ${JsonObj_SectionOrSubSectionDetails_Res}    ${SubsectionIndex}    ${SubSectionName}
    ${SubSectionID}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[0].subSectionid
    ${SubSubSection}    Run Keyword If    "${SubSectionID}"!="${EMPTY}" and "${SubSectionID}"!="null"    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails
    ...    ELSE    Set Variable    ${EMPTY}
    Log    ${SubSubSection}
    ${SubSubSectionCount}    Run Keyword If    """${SubSubSection}"""!="${EMPTY}"    Get Length    ${SubSubSection}
    ...    ELSE    Set Variable    0
    FOR    ${subSectionDetailsIndex}    IN RANGE    0    ${SubSubSectionCount}
        ${SectionName}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionName
        ${SectionName}    Set Variable If    ${SubSubSectionCount}>1    ${SubSectionName}    ${SectionName}
        ${SectionID}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionid
        ${subSectionQuestions}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions
        ${TotalsubSectionQuestions}    Get Length    ${subSectionQuestions}
        ${index}    Evaluate    ${SubsectionIndex}+1
        ${SubSectionParentHeaderXpath}    Set Variable If    ${subSectionDetailsIndex}==0 and "${SectionName}"!="None" and "${SectionName}"!="${EMPTY}"    //span[text()="${SectionName}"]/parent::button    //div[text()="${index}"]/parent::button
        Run Keyword If    ${subSectionDetailsIndex}==0    Scroll to the element    ${SubSectionParentHeaderXpath}    up
        Run Keyword And Return Status    Run Keyword If    ${subSectionDetailsIndex}==0    Element Should Be Visible    ${SubSectionParentHeaderXpath}
        Run Keyword If    ${subSectionDetailsIndex}==0    Click Button    ${SubSectionParentHeaderXpath}
        Run Keyword If    ${subSectionDetailsIndex}==0    Sleep    2s
        Run Keyword And Return Status    Run Keyword If    ${subSectionDetailsIndex}>0 and "${SectionName}"!="None" and "${SectionName}"!="${EMPTY}"    Element Should Be Visible    //b[text()="${SectionName}"]
        Run Keyword If    "${TotalsubSectionQuestions}"!="${EMPTY}" and "${TotalsubSectionQuestions}"!="0"    Verify the questions displayed    ${JsonObj_SectionOrSubSectionDetails_Res}    ${subSectionDetailsIndex}
        Run Keyword If    ${subSectionDetailsIndex}==0    Scroll to the element    ${SubSectionParentHeaderXpath}    up
        ...    ELSE    Scroll to the element    //b[text()="${SectionName}"]    up
    END

Verify the dependant questions
    [Arguments]    ${JsonObj_SectionOrSubSectionDetails_Res}    ${index}    ${TotalsubSectionQuestions}    ${subSectionDetailsIndex}
    ${DependsOnQuestionId}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${index}].dependOn[0].questionId
    ${DependsOnQuestionValue}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${index}].dependOn[0].questionValue
    ${QuestionText}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${index}].questionText
    ${QuestionValue}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${index}].answer
    ${DependsOnStatus}    Set Variable    false
    FOR    ${i}    IN RANGE    0    ${TotalsubSectionQuestions}
        ${QuestionID}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${i}].questionId
        ${DependantQuestionText}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${i}].questionText
        ${QuestionType}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${i}].questionColumns[0].type
        ${QuestionOption}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${i}].questionColumns[0].options
        ${DependantQuestionValue}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${i}].questionColumns[0].answer
        ${DependsOnStatus}    Set Variable If    "${DependsOnQuestionId}"=="${QuestionID}"    True
        Run Keyword If    "${DependsOnQuestionId}"=="${QuestionID}" and "${QuestionType}"=="CtrlRadio"    Verify the question is not displayed if the dependent question value is incorrect for radio options    ${QuestionID}    ${QuestionType}    ${QuestionOption}    ${DependantQuestionText}    ${QuestionText}    ${DependsOnQuestionValue}
        Run Keyword If    "${DependsOnQuestionId}"=="${QuestionID}" and "${QuestionType}"=="CtrlRadio"    Click Element    //label[text()="${DependantQuestionText}"]/../..//input[@value="${DependsOnQuestionValue}" and @type="radio"]
        Run Keyword If    "${DependsOnQuestionId}"=="${QuestionID}" and "${QuestionType}"=="CtrlCheckBox" and "${DependsOnQuestionValue}"!="${DependantQuestionValue}"    Verify the question is not displayed if the dependent question value is not selected for checkbox    ${QuestionID}    ${QuestionValue}    ${DependantQuestionText}    ${QuestionText}
        Exit For Loop If    "${DependsOnQuestionId}"=="${QuestionID}"
    END
    Run Keyword If    '${DependsOnStatus}'!='True'    Run Keyword And Continue On Failure    fail    Dependant question not found "${QuestionText}"\nQuestion ID:${DependsOnQuestionId}
    ${questionColumns}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${i}].questionColumns
    ${questionColumnsCount}    Get Length    ${questionColumns}
    Run Keyword If    "${DependsOnQuestionValue}"=="${DependantQuestionValue}"    Verify the question content    ${JsonObj_SectionOrSubSectionDetails_Res}    ${questionColumnsCount}    ${index}    ${subSectionDetailsIndex}

Verify the dropdown questions
    [Arguments]    ${QuestionId}    ${Type}    ${QuestionOptions}    ${QuestionValue}    ${FieldName}
    ${Options}    Split String    ${QuestionOptions}    |
    ${OptionsCount}    Get Length    ${Options}
    FOR    ${i}    IN RANGE    0    ${OptionsCount}
        ${Option}    Set Variable    ${Options}[${i}]
        ${expandstatus}    Get Element Attribute    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text())="${FieldName}" or text()="${FieldName}"]/../../div[2]/div/div    aria-expanded
        ${expandstatus}    Convert To Lower Case    ${expandstatus}
        Run Keyword If    "${QuestionValue}"=="${Option}"    Wait Until Element Is Visible    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text())="${FieldName}" or text()="${FieldName}"]/../..//input[@value="${Option}" and contains(@class,"MuiSelect-")]/..
        Run Keyword If    '${expandstatus}'!='true'    Click Element    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text())="${FieldName}" or text()="${FieldName}"]/../../div[2]/div/div
        Run Keyword If    "${QuestionValue}"=="${Option}"    Wait Until Element Is Visible    //ul[@role="listbox"]/li[@role="option" and @aria-selected="true" and @data-value="${Option}"]
        ...    ELSE    VerifyElementIsDisplayed    //ul[@role="listbox"]/li[@role="option" and @aria-selected="false" and @data-value="${Option}"]    ${FieldName}
    END
    Press Keys    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text())="${FieldName}" or text()="${FieldName}"]/../../div[2]/div/div    ESC

Verify the input text questions
    [Arguments]    ${QuestionID}    ${Type}    ${Value}    ${QuestionText}
    ${Questiondoublequotestatus}    Run Keyword And Return Status    Should Contain    ${QuestionText}    "
    ${Valuedoublequotestatus}    Run Keyword And Return Status    Should Contain    ${Value}    "
    ${QuestionText1}    Split String    ${QuestionText}    ${SPACE}${SPACE}
    ${element}    Set Variable If    '${Questiondoublequotestatus}'=='True' and '${Valuedoublequotestatus}'=='False'    \    '${Questiondoublequotestatus}'=='True' and '${Valuedoublequotestatus}'=='True'    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text()) ='${QuestionText}' or text()='${QuestionText}' or contains(text(),'${QuestionText1}[0]')]/../..//input[@value='${Value}' and @type="text"]    '${Questiondoublequotestatus}'=='False' and '${Valuedoublequotestatus}'=='True'    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text()) ="${QuestionText}" or text()="${QuestionText}" or contains(text(),"${QuestionText1}[0]")]/../..//input[@value='${Value}' and @type="text"]    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text()) ="${QuestionText}" or text()="${QuestionText}" or contains(text(),"${QuestionText1}[0]")]/../..//input[@value="${Value}" and @type="text"]
    VerifyElementIsDisplayed    ${element}    ${QuestionText}

Verify the input textarea questions
    [Arguments]    ${QuestionId}    ${Type}    ${Value}    ${FieldName}
    ${Questiondoublequotestatus}    Run Keyword And Return Status    Should Contain    ${QuestionText}    "
    ${FieldName1}    Split String    ${FieldName}    ${SPACE}${SPACE}
    ${Value1}    Split String    ${Value}    ${SPACE}${SPACE}
    ${Valuedoublequotestatus}    Run Keyword And Return Status    Should Contain    ${Value}    "
    ${element}    Set Variable If    ('${Questiondoublequotestatus}'=='True' and '${Valuedoublequotestatus}'=='True') and "${Value}"!="${EMPTY}"    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text()) ='${FieldName}' or text()='${FieldName}' or contains(text(),'${FieldName1}[0]')]/../..//textarea[normalize-space(text())='${Value}' or text()='${Value}' or contains(text(),'${Value1}[0]')]    ('${Questiondoublequotestatus}'=='True' and '${Valuedoublequotestatus}'=='False') and "${Value}"!="${EMPTY}"    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text()) ='${FieldName}' or text()='${FieldName}' or contains(text(),'${FieldName1}[0]')]/../..//textarea[normalize-space(text())="${Value}" or text()="${Value}" or contains(text(),"${Value1}[0]")]    ('${Questiondoublequotestatus}'=='False' and '${Valuedoublequotestatus}'=='True') and "${Value}"!="${EMPTY}"    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text()) ="${FieldName}" or text()="${FieldName}" or contains(text(),"${FieldName1}[0]")]/../..//textarea[normalize-space(text())='${Value}' or text()='${Value}' or contains(text(),'${Value1}[0]')]    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text()) ="${FieldName}" or text()="${FieldName}" or contains(text(),"${FieldName1}[0]")]/../..//textarea[normalize-space(text())="${Value}" or text()="${Value}" or contains(text(),"${Value1}[0]")]
    ${element1}    Set Variable If    '${Questiondoublequotestatus}'=='True' and "${Value}"=="${EMPTY}"    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text()) ='${FieldName}' or text()='${FieldName}' or contains(text(),'${FieldName1}[0]')]/../..//textarea[1]    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text()) ="${FieldName}" or text()="${FieldName}" or contains(text(),"${FieldName1}[0]")]/../..//textarea[1]
    Run Keyword If    "${Value}"!="${EMPTY}"    VerifyElementIsDisplayed    ${element}    ${FieldName}    #//label[text()="${FieldName}"]/../..//textarea[text()="${Value}"]
    Run Keyword If    "${Value}"=="${EMPTY}"    VerifyElementIsDisplayed    ${element1}    ${FieldName}

Verify the question content
    [Arguments]    ${JsonObj_SectionOrSubSectionDetails_Res}    ${questionColumnsCount}    ${subSectionQuestionsIndex}    ${subSectionDetailsIndex}
    ${QuestionText}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].questionText
    FOR    ${i}    IN RANGE    0    ${questionColumnsCount}
        ${QuestionId}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex}].questionId
        ${QuestionType}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex}].questionColumns[${i}].type
        ${QuestionOptions}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex}].questionColumns[${i}].options
        ${QuestionValue}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex}].questionColumns[${i}].answer
        ${QuestionqTableReference}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex}].questionColumns[${i}].qTableReference
        ${ColumnId}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex}].questionColumns[${i}].columnId
        ${QuestionId}    Run Keyword If    ${questionColumnsCount}>1    Set Variable    ${QuestionId}-${ColumnId}
        ...    ELSE    Set Variable    ${QuestionId}
        ${QuestionValue}    Replace String    ${QuestionValue}    ~!    "
        Run Keyword If    "${QuestionType}"=="CtrlRadio"    Verify the radio button questions    ${QuestionId}    ${QuestionType}    ${QuestionOptions}    ${QuestionValue}    ${QuestionText}
        Run Keyword If    "${QuestionType}"=="CtrlSelect" and "${QuestionqTableReference}"=="${EMPTY}"    Verify the dropdown questions    ${QuestionId}    ${QuestionType}    ${QuestionOptions}    ${QuestionValue}    ${QuestionText}
        Run Keyword If    "${QuestionType}"=="CtrlSelect" and "${QuestionqTableReference}"!="${EMPTY}"    Wait Until Element Is Visible    //div[@questionid="${QuestionId}"]/..//label[normalize-space(text())="${QuestionText}" or text()="${QuestionText}"]/../..//input/..
        Run Keyword If    "${QuestionType}"=="CtrlTextNumber" or "${QuestionType}"=="CtrlText"    Verify the input text questions    ${QuestionId}    ${QuestionType}    ${QuestionValue}    ${QuestionText}
        Run Keyword If    "${QuestionType}"=="CtrlTextArea"    Verify the input textarea questions    ${QuestionId}    ${QuestionType}    ${QuestionValue}    ${QuestionText}
        Run Keyword If    "${QuestionType}"=="CtrlSelectMultiple" and "${QuestionqTableReference}"=="${EMPTY}" and "${QuestionOptions}"!="a|b|c"    Verify the Checkbox questions    ${QuestionId}    ${QuestionType}    ${QuestionOptions}    ${QuestionValue}    ${QuestionText}
    END

Verify the question is not displayed if the dependent question value is incorrect for radio options
    [Arguments]    ${QuestionId}    ${QuestionType}    ${QuestionOptions}    ${QuestionText}    ${DependantQuestionText}    ${DependsOnQuestionValue}
    ${Options}    Split String    ${QuestionOptions}    |
    ${OptionsCount}    Get Length    ${Options}
    FOR    ${i}    IN RANGE    0    ${OptionsCount}
        ${Option}    Set Variable    ${Options}[${i}]
        Run Keyword If    "${DependsOnQuestionValue}"!="${Option}"    Scroll to the element    //div[@questionid="${QuestionId}"]//label[text()="${QuestionText}"]/../..//input[@value="${Option}" and @type="radio"]    down
        Run Keyword If    "${DependsOnQuestionValue}"!="${Option}"    Click Element    //div[@questionid="${QuestionId}"]//label[text()="${QuestionText}"]/../..//input[@value="${Option}" and @type="radio"]
        Run Keyword If    "${DependsOnQuestionValue}"!="${Option}"    VerifyElementIsNotDisplayed    //div[@questionid="${QuestionId}"]//label[text()="${DependantQuestionText}"]    ${DependantQuestionText}
    END

Verify the question is not displayed if the dependent question value is not selected for checkbox
    [Arguments]    ${QuestionId}    ${Value}    ${QuestionText}    ${DependantQuestionText}
    Comment    Scroll to the element    //label[text()="${DependantQuestionText}"]/../..//input    down
    VerifyElementIsNotDisplayed    //div[@questionid="${QuestionId}"]//label[text()="${DependantQuestionText}"]/../..//input[@value="${Value}"]    ${DependantQuestionText}

Verify the questions displayed
    [Arguments]    ${JsonObj_SectionOrSubSectionDetails_Res}    ${subSectionDetailsIndex}
    ${subSectionQuestions}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions
    Log    ${subSectionQuestions}
    ${TotalsubSectionQuestions}    Get Length    ${subSectionQuestions}
    #Verify the section Questions with API
    FOR    ${subSectionQuestionsIndex}    IN RANGE    0    ${TotalsubSectionQuestions}
        ${QuestionID}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].questionId
        ${QuestionText}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].questionText
        ${DependsOn}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].dependOn
        ${DependsOn}    Get Length    ${DependsOn}
        ${questionColumns}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].questionColumns
        ${options}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].questionColumns[0].options
        ${QuestionType}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].questionColumns[0].type
        ${QuestionqTableReference}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].questionColumns[0].qTableReference
        ${questionColumnsCount}    Get Length    ${questionColumns}
        ${ColumnId}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.subSectionDetails[${subSectionDetailsIndex}].subSectionQuestions[${subSectionQuestionsIndex} ].questionColumns[0].columnId
        ${QuestionId}    Run Keyword If    ${questionColumnsCount}>1    Set Variable    ${QuestionID}-${ColumnId}
        ...    ELSE    Set Variable    ${QuestionID}
        ${QuestionTextBeforeReplace}    Set Variable    ${QuestionText}
        ${doublequotestatus}    Run Keyword And Return Status    Should Contain    ${QuestionText}    ~!
        ${QuestionText}    Run Keyword If    '${doublequotestatus}'=='True'    Replace String    ${QuestionText}    ~!    "
        ...    ELSE    Set Variable    ${QuestionText}
        ${QuestionText1}    Split String    ${QuestionText}    ${SPACE}${SPACE}
        ${element}    Set Variable If    '${doublequotestatus}'=='True'    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text()) ='${QuestionText}' or text()='${QuestionText}' or contains(text(),'${QuestionText1}[0]')]    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text()) ="${QuestionText}" or text()="${QuestionText}" or contains(text(),"${QuestionText1}[0]")]
        Run Keyword If    "${DependsOn}"=="0" and "${QuestionTextBeforeReplace}"!="${EMPTY}"    Run Keyword And Continue On Failure    Wait Until Element Is Visible    ${element}
        Run Keyword If    "${questionColumnsCount}"!="${EMPTY}" and "${questionColumnsCount}"!="0" and "${DependsOn}"=="0" and "${QuestionTextBeforeReplace}"!="${EMPTY}"    Verify the question content    ${JsonObj_SectionOrSubSectionDetails_Res}    ${questionColumnsCount}    ${subSectionQuestionsIndex}    ${subSectionDetailsIndex}
        Run Keyword If    "${questionColumnsCount}"!="${EMPTY}" and "${questionColumnsCount}"!="0" and "${DependsOn}"!="0" and "${QuestionTextBeforeReplace}"!="${EMPTY}"    Verify the dependant questions    ${JsonObj_SectionOrSubSectionDetails_Res}    ${subSectionQuestionsIndex}    ${TotalsubSectionQuestions}    ${subSectionDetailsIndex}
        Run Keyword If    "${QuestionType}"=="CtrlButton" and "${questionColumnsCount}"!="${EMPTY}" and "${questionColumnsCount}"!="0" and "${DependsOn}"=="0" and "${QuestionTextBeforeReplace}"=="${EMPTY}"    Wait Until Element Is Visible    //div[@questionid="${QuestionId}"]/..//button[contains(text(),'${options}')]
    END

Verify the radio button questions
    [Arguments]    ${QuestionId}    ${Type}    ${QuestionOptions}    ${QuestionValue}    ${FieldName}
    ${Options}    Split String    ${QuestionOptions}    |
    ${OptionsCount}    Get Length    ${Options}
    FOR    ${i}    IN RANGE    0    ${OptionsCount}
        ${Option}    Set Variable    ${Options}[${i}]
        ${Status}    Run Keyword If    "${QuestionValue}"=="${Option}"    Run Keyword And Return Status    Wait Until Element Is Visible    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text())="${FieldName}" or text()="${FieldName}"]/../..//input[@value="${Option}" and @type="radio"]/../../span[contains(@class,"Mui-checked ")]
        Run Keyword If    '${Status}'=='False'    Run Keyword And Continue On Failure    Fail    ${Option} is not selected for ${FieldName}
        VerifyElementIsDisplayed    //div[@questionid="${QuestionID}"]/..//label[normalize-space(text())="${FieldName}" or text()="${FieldName}"]/../..//input[@value="${Option}" and @type="radio"]/../../span[2]    ${FieldName}
    END

Verify the section header content
    [Arguments]    ${JsonObj_PqSections_Res}    ${SectionName}
    ${SectionHeader}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${DataIndex}].sectionHeader
    ${SectionHeader}    RemoveHTMLTags    ${SectionHeader}
    ${SectionHeader}    Split String    ${SectionHeader}    <div>
    ${Count}    Get Length    ${SectionHeader}
    FOR    ${i}    IN RANGE    0    ${Count}
        ${Content}    Strip String    ${SectionHeader}[${i}]
        Log    ${Content}
        #    //div[text()="${SectionName}"]/../div[2]
        Run Keyword And Continue On Failure    Wait Until Element Is Visible    //div[contains(@class,"questionnaire-template M")]/div[2]    ${timeout}
        Run Keyword And Continue On Failure    Element Should Contain    //div[contains(@class,"questionnaire-template M")]/div[2]    ${Content}    ignore_case=False
    END

Verify the section names, section count and the status displayed in the Questionare
    [Arguments]    ${JsonObj_PqSections_Res}
    #Verifyies the section name displayed in the Questionare against the API
    ${Sections}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data
    ${ExpectedSectionCount}    Get Length    ${Sections}
    ${HeaderCount}    Convert To Integer    1
    FOR    ${i}    IN RANGE    0    ${ExpectedSectionCount}
        ${ExpectedSectionName}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${i}].sectionName
        ${sectionQuestions}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${i}].sectionQuestions
        ${sectionQuestionsCompleted}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${i}].sectionQuestionsCompleted
        ${ActualSectionName}    Get Text    (//span[contains(@class,"MuiStepLabel-root MuiStepLabel-horizontal ")])[${HeaderCount}]/span[2]/span/div[1]    #Gets the Section Name
        ${ActualQuestionCompleted}    Get Text    (//span[contains(@class,"MuiStepLabel-root MuiStepLabel-horizontal ")])[${HeaderCount}]/span[2]/span/div[2]    #Gets the Questions Completed
        CompareResults    ${ActualSectionName}    ${ExpectedSectionName}    Section Name
        ${ExpectedQuestionCompleted}    Set Variable If    "${sectionQuestions}"=="0"    ${EMPTY}    ${sectionQuestionsCompleted}/${sectionQuestions}
        CompareResults    ${ActualQuestionCompleted}    ${ExpectedQuestionCompleted}    Section Name
        ${HeaderCount}    Evaluate    ${HeaderCount}+1
        Log    ${HeaderCount}:-${ActualSectionName}:${ExpectedSectionName}\n
    END
    #Verifyies the section count displayed in API with the Questionare
    ${ActSectionCount}    Get Element Count    //div[contains(@class,"MuiStepper-root MuiStepper-horizontal")]/div
    CompareResults    ${ActSectionCount}    ${ExpectedSectionCount}    Section Count
    #Verify the status of the questionare
    ${HeaderCount}    Convert To Integer    1
    FOR    ${i}    IN RANGE    0    ${ExpectedSectionCount}
        ${ExpectedIsSectionCompleted}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${i}].isSectionCompleted
        ${ExpectedSectionName}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data[${i}].sectionName
        VerifyQuestionareSectionStatus    ${HeaderCount}    ${ExpectedIsSectionCompleted}    ${ExpectedSectionName}
        ${HeaderCount}    Evaluate    ${HeaderCount}+1
    END

Verify the subsection content without question
    [Arguments]    ${JsonObj_PqSections_Res}    ${ClientProfileIndex}
    ${SectionHeader}    GetValueFromResponse    ${JsonObj_PqSections_Res}    $.data.clientProfiles[${ClientProfileIndex}].positionContent
    ${SectionHeader}    RemoveHTMLTags    ${SectionHeader}
    ${SectionHeader}    Split String    ${SectionHeader}    <div>
    ${Count}    Get Length    ${SectionHeader}
    Sleep    3s
    FOR    ${i}    IN RANGE    0    ${Count}
        ${Content}    Strip String    ${SectionHeader}[${i}]
        Log    ${Content}
        Wait Until Element Is Visible    //div[contains(@class,"questionnaire-template M")]/div[2]/div[1]    ${timeout}
        Element Should Contain    //div[contains(@class,"questionnaire-template M")]/div[2]/div[1]    ${Content}    ignore_case=False
    END

VerifyQuestionareSectionStatus
    [Arguments]    ${Count}    ${IsCompleted}    ${SectionName}
    ${Locator}    Set Variable    (//span[contains(@class,"MuiStepLabel-root MuiStepLabel-horizontal ")])[${Count}]/span[1]/div/*
    Comment    ${Width}    Run Keyword And Return If    '${IsCompleted}'=='true'    Get Element Attribute    ${Locator}    width
    Comment    ${Height}    Run Keyword And Return If    '${IsCompleted}'=='true'    Get Element Attribute    ${Locator}    height
    ${ViewBox}=    Execute Javascript    document.evaluate('${Locator}', document, null, XPathResult.ANY_TYPE, null).iterateNext().getAttribute('viewBox')\n
    Comment    ${ViewBox}=    Get Element Attribute    ${Locator}    viewBox
    Run Keyword If    "${IsCompleted}"=="true" and "${ViewBox}"!="0 0 24 24"    Fail    ${SectionName} Should be in Completed State
    Run Keyword If    "${IsCompleted}"=="false" and "${ViewBox}"!="0 0 25 24"    Fail    ${SectionName} Should be in Progress State

GetDoubleQuoteQuestionElementText
    [Arguments]    ${QuestionText}
    ${doubleQuoteStatus}    Run Keyword And Return Status    Should Contain    ${QuestionText}    '
    @{QueDoubleQuoteArr}    Split String    ${QuestionText}    "    max_split=-1
    ${doubleQuoteCount}    Get Length    ${QueDoubleQuoteArr}
    ${QuestionElementTxt}    Set Variable    ${EMPTY}
    FOR    ${i}    IN RANGE    0    ${doubleQuoteCount}
        ${str}    Set Variable If    ${i}==0    "${QueDoubleQuoteArr}[${i}]"    ,'"${QueDoubleQuoteArr}[${i}]'
        ${QuestionElementTxt}    Set Variable    ${QuestionElementTxt}${str}
    END
    Log    ${QuestionElementTxt}
    [Return]    ${QuestionElementTxt}

VerifySupportingDocumentsContent
    [Arguments]    ${JsonObj_SectionOrSubSectionDetails_Res}    ${subSectionDetailsIndex}
    ${documentStatus}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].documentStatus
    ${documentTypeName}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].documentTypeName
    ${documentName}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].documentName
    ${documentId}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].documentId
    ${expires}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].expires
    ${expires}    Run Keyword If    '${expires}'!='${EMPTY}'    Convert Date    ${expires}    result_format=%m/%d/%Y %I:%M %p
    ...    ELSE    Set Variable    ${EMPTY}
    ${uploadedOn}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].uploaded
    ${uploadedOn}    Run Keyword If    '${uploadedOn}'!='${EMPTY}'    Convert Date    ${uploadedOn}    result_format=%m/%d/%Y %I:%M %p
    ...    ELSE    Set Variable    ${EMPTY}
    ${uploaderFullName}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].uploaderFullName
    ${updatedOn}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].updatedOn
    ${updatedOn}    Run Keyword If    '${updatedOn}'!='${EMPTY}'    Convert Date    ${updatedOn}    result_format=%m/%d/%Y %I:%M %p
    ...    ELSE    Set Variable    ${EMPTY}
    ${updatedByUserFullName}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].updatedByUserFullName
    ${statusChangeDate}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].statusChangeDate
    ${statusChangeDate}    Run Keyword If    '${statusChangeDate}'!='${EMPTY}'    Convert Date    ${statusChangeDate}    result_format=%m/%d/%Y %I:%M %p
    ...    ELSE    Set Variable    ${EMPTY}
    ${statusChangedUserFullName}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].statusChangedUserFullName
    ${statusChangedBy}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*[${subSectionDetailsIndex}].statusChangedBy
    ${documentStatus}    Set Variable If    '${documentStatus}'=='Deleted'    Deleted Documents    Supporting Documents
    Run Keyword If    "${documentStatus}"=="Deleted Documents"    Scroll to the element    //span[text()='${documentStatus}']/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//span[text()='${documentName}']    up
    ...    ELSE    Scroll to the element    //span[text()='${documentStatus}']/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']    up
    Sleep    2s
    Run Keyword If    "${documentStatus}"=="Deleted Documents"    Element Should Be Visible    //span[text()='${documentStatus}']/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//span[text()='${documentName}']
    ...    ELSE    Element Should Be Visible    //span[text()='${documentStatus}']/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']
    #Below comment can be removed once we get the data for the history api
    ${JsonObj_DocLog_Res}    GetDocumentLog    ${documentId}
    ${Logs}    GetValueFromResponse    ${JsonObj_DocLog_Res}    $.data
    ${TotalLogCount}    Get Length    ${Logs}
    FOR    ${i}    IN RANGE    0    ${TotalLogCount}
        ${LogDocumentName}    GetValueFromResponse    ${JsonObj_DocLog_Res}    $.data[${i}].documentName
        ${LogDocumentType}    GetValueFromResponse    ${JsonObj_DocLog_Res}    $.data[${i}].documentType
        ${LogExpiration}    GetValueFromResponse    ${JsonObj_DocLog_Res}    $.data[${i}].expirationDate
        ${LogExpiration}    Run Keyword If    '${LogExpiration}'!='${EMPTY}'    Convert Date    ${LogExpiration}    result_format=%m/%d/%Y %I:%M %p
        ...    ELSE    Set Variable    ${EMPTY}
        ${LogStatus}    GetValueFromResponse    ${JsonObj_DocLog_Res}    $.data[${i}].status
        ${LogChangedBy}    GetValueFromResponse    ${JsonObj_DocLog_Res}    $.data[${i}].modifiedBy
        ${LogChanged}    GetValueFromResponse    ${JsonObj_DocLog_Res}    $.data[${i}].modifiedOn
        ${LogChanged}    Run Keyword If    '${LogChanged}'!='${EMPTY}'    Convert Date    ${LogChanged}    result_format=%m/%d/%Y %I:%M %p
        ...    ELSE    Set Variable    ${EMPTY}
        ${LogActivity}    GetValueFromResponse    ${JsonObj_DocLog_Res}    $.data[${i}].modificationType
        ${EMPTY}
        Run Keyword If    "${documentStatus}"=="Deleted Documents"    Click Element    //span[text()='${documentStatus}']/../../..//span[text()='${documentTypeName}']/../..//span[text()='${documentName}']/../../button[@aria-label='edit']
        ...    ELSE    Click Element    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../button[@aria-label='Restore']
        Wait Until Element Is Visible    //th[text()='Name']/ancestor::table//td[text()='${LogDocumentType}']
        Element Should Be Visible    //span[text()='${LogDocumentName}']/parent::h2
        Element Should Be Visible    //th[text()='Name']/ancestor::table//td[text()='${LogDocumentType}']
        Element Should Be Visible    //th[text()='type']/ancestor::table//td[text()='${LogDocumentName}']
        CompareElementText    //th[text()='Expiration']/ancestor::table//td[text()='${LogExpiration}']    ${LogExpiration}    Expiration
        Element Should Be Visible    //th[text()='Status']/ancestor::table//td[text()='${LogStatus}']
        Element Should Be Visible    //th[text()='Changed By']/ancestor::table//td[text()='${LogChangedBy}']
        Element Should Be Visible    //th[text()='Changed']/ancestor::table//td[text()='${LogChanged}']
        Element Should Be Visible    //th[text()='Activity']/ancestor::table//td[text()='${LogActivity}']
        Click Element    //h2[@id="responsive-dialog-title"]/*[2]
    END
    #For Approved Documents
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    Element Should Be Visible    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../button[@aria-label='Replace']
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    Element Should Be Visible    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../button[@aria-label='edit']
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    Element Should Be Visible    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../button[@aria-label='delete']
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    Element Should Be Visible    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../button[@aria-label='Replace']
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    Element Should Be Visible    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../button[@aria-label='Replace']
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    Element Should Be Visible    //div[@id='${documentId}']//a[text()='${documentName}']/../../*[@data-testid="EditIcon"]
    ${Upload}    Catenate    Uploaded on    ${uploadedOn}    by    ${uploaderFullName}
    ${Upload}    Replace String    ${Upload}    ${SPACE}${SPACE}    ${SPACE}    count=-1
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    CompareElementText    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../..//p[1]    ${Upload}    Uploaded On
    ${LastUpdated}    Catenate    Last Updated on    ${updatedOn}    by    ${updatedByUserFullName}
    ${LastUpdated}    Replace String    ${LastUpdated}    ${SPACE}${SPACE}    ${SPACE}    count=-1
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    CompareElementText    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../..//p[2]    ${LastUpdated}    Last Updated on
    ${Status1}    Catenate    Status: Approved on    ${statusChangeDate}    by    ${statusChangedUserFullName}
    ${Status}    Set Variable If    '${statusChangeDate}'=='${EMPTY}'    Status: on by    ${Status1}
    ${Status}    Replace String    ${Status}    ${SPACE}${SPACE}    ${SPACE}    count=-1
    Run Keyword If    "${documentStatus}"!="Deleted Documents"    CompareElementText    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../../..//p[3]    ${Status}    Status
    ${Expired}    Catenate    Expired on :${expires}
    Run Keyword If    "${documentStatus}"!="Deleted Documents" and '${expires}'!='${EMPTY}'    CompareElementText    (//span[text()='${documentStatus}'])[2]/../../..//span[text()='${documentTypeName}']/../../div[@id='${documentId}']//a[text()='${documentName}']/../../..//span[@style="color: red;"]    ${Expired}    Status

Verify Supporting Documents
    [Arguments]    ${JsonObj_SectionOrSubSectionDetails_Res}
    ${count}    GetValueFromResponse    ${JsonObj_SectionOrSubSectionDetails_Res}    $.data.*
    ${count}    Get Length    ${count}
    Scroll to the element    (//span[text()='Supporting Documents'])[2]/ancestor::div[@aria-expanded="false" and @role="button"]    up
    Run Keyword And Return Status    Click Element    (//span[text()='Supporting Documents'])[2]/ancestor::div[@aria-expanded="false" and @role="button"]
    Sleep    3s
    Scroll to the element    (//span[text()='Deleted Documents'])[1]/ancestor::div[@aria-expanded="false" and @role="button"]    up
    Run Keyword And Return Status    Click Element    (//span[text()='Deleted Documents'])[1]/ancestor::div[@aria-expanded="false" and @role="button"]
    FOR    ${i}    IN RANGE    0    ${count}
        VerifySupportingDocumentsContent    ${JsonObj_SectionOrSubSectionDetails_Res}    ${i}
    END

Verify the dependant questions with empty question text
    [Arguments]    ${JsonObj_SectionOrSubSectionDetails_Res}    ${index}    ${TotalsubSectionQuestions}    ${subSectionDetailsIndex}

GetDocumentLog
    [Arguments]    ${DocID}
    ${Wrapper}    Set Variable    /api/Prequalification/GetDocumentLog?documentId=${DocID}
    ${JsonObj_PqSections_Res}    GetFVRequest    ${Wrapper}
    [Return]    ${JsonObj_PqSections_Res}
