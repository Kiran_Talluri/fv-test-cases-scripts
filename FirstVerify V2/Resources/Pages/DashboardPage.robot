*** Settings ***
Library           Selenium2Library
Resource          ../ReusableGroup.robot
Resource          DashboardPageObject.robot

*** Keywords ***
VerifyVendorCountDisplayedInCards
    #Greater Than 30 days
    ${Act_txt}    Get Text    ${DashboardGreaterThan30DaysValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0
    #Within 30 Days
    ${Act_txt}    Get Text    ${DashboardWithin30DaysValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0
    #Client Review
    ${Act_txt}    Get Text    ${DashboardClientReviewValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0
    #Yesterday
    ${Act_txt}    Get Text    ${DashboardYesterdayValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0
    #Reminder
    ${Act_txt}    Get Text    ${DashboardRemindersValue_lbl}
    ${Act_txt}    Convert To Integer    ${Act_txt}
    Run Keyword If    ${Act_txt}<0    Fail    The value should be equal to or greater than 0

SelectClient
    [Arguments]    @{ClientNames}
    Run Keyword And Return Status    Click Element    ${DashboardClientSearch_txt}
    Sleep    2s
    FOR    ${ClientName}    IN    @{ClientNames}
        Run Keyword And Return Status    ClickElement_rgp    //span[text()="${ClientName}"]/ancestor::li/div[2]
    END
    ClickElement_rgp    ${DashboardSelectClientSave_btn}
    VerifyFirstCardIsDisplayed

VerifyFirstCardIsDisplayed
    ${CardsStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardFirstCard}
    ${NoRecordsFoundStatus}    Run Keyword And Return Status    Wait Until Element Is Visible    ${DashboardNoResultsFound_lbl}
    Set Test Variable    ${CardsStatus}
    Set Test Variable    ${NoRecordsFoundStatus}

GetCardsCount
    [Arguments]    ${Count}
    ${Pagecount}    Get Text    //nav[@aria-label="pagination navigation"]/ul/li[${Count}]
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Sleep    2s
    ClickElement_rgp    //nav[@aria-label="pagination navigation"]/ul/li[${Count}]
    Sleep    2s
    ${CardsCount}    Get Element Count    //div[contains(@class,"MuiCard-root grid-card")]
    ${CardsCount}    Evaluate    ((${Pagecount}-1)*12)+${CardsCount}
    [Return]    ${CardsCount}
