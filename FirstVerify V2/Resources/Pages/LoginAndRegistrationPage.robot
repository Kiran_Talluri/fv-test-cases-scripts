*** Settings ***
Library           Selenium2Library
Resource          ../ReusableGroup.robot
Resource          LoginAndRegistrationPageObject.robot
Resource          DashboardPage.robot

*** Keywords ***
CompanyRegistrationAddressInformation
    [Arguments]    ${Address1}    ${AddAddress2}    ${Address2}    ${AddAddress3}    ${Address3}    ${City}    ${PostalCode}    ${Country}    ${State}    ${Language}    ${Region}
    InputText_rgp    ${CompRegAddress1_txt}    ${Address1}
    Run Keyword If    '${AddAddress2}'=='True'    InputText_rgp    ${CompRegAddress2_txt}    ${Address2}
    Run Keyword If    '${AddAddress3}'=='True'    InputText_rgp    ${CompRegAddress3_txt}    ${Address3}
    InputText_rgp    ${CompRegCity_txt}    ${City}
    InputText_rgp    ${CompRegZip/PostalCode_txt}    ${PostalCode}
    Run Keyword If    '${Country}'!='${EMPTY}'    Select From List By Label    ${CompRegCountry_txt}    ${Country}
    Comment    Wait Until Element Is Visible    //option[text()="${State}"]    5s
    Run Keyword If    '${State}'!='${EMPTY}'    Select From List By Label    ${CompRegStateAndProvince_txt}    ${State}
    Wait Until Element Is Visible    //option[text()="${Language}"]    5s
    Run Keyword If    '${Language}'!='${EMPTY}'    ClickElement_rgp    ${CompRegLanguage_lst}
    Run Keyword If    '${Language}'!='${EMPTY}'    ClickElement_rgp    //span[text()="${Language}"]
    Run Keyword If    '${Region}'!='${EMPTY}'    Select From List By Label    ${CompRegVendorRegion_txt}    ${Region}
    ClickButton_rgp    ${CompRegNext_btn}

CompanyRegistrationBusinessInformation
    [Arguments]    ${isUS}    ${Name}    ${TaxID}    ${OfficerName}
    Run Keyword If    '${isUS}'=='True'    ClickElement_rgp    ${CompRegTypeInternational_btn}
    ...    ELSE IF    '${isUS}'=='False'    ClickElement_rgp    ${CompRegTypeUS_btn}
    InputText_rgp    ${CompRegLegalNameOfBusiness_txt}    ${Name}
    InputText_rgp    ${CompRegTaxID_txt}    ${TaxID}
    InputText_rgp    ${CompRegComPrincipalOfficerName_txt}    ${OfficerName}
    Scroll to the element    ${CompRegComPrincipalOfficerName_txt}    down
    ClickButton_rgp    ${CompRegNext_btn}

CompanyRegistrationContactInformation
    [Arguments]    ${PhoneNumber}    ${ExtNumber}    ${FaxNumber}    ${WebsiteURL}
    InputText_rgp    ${CompanyReg_PhoneNumber_txt}    ${PhoneNumber}
    InputText_rgp    ${CompanyReg_ExtNumber_txt}    ${ExtNumber}
    InputText_rgp    ${CompanyReg_FaxNumber_txt}    ${FaxNumber}
    InputText_rgp    ${CompanyReg_WebsiteURL_txt}    ${WebsiteURL}
    ClickButton_rgp    ${CompRegNext_btn}

CompanyRegistrationPrimaryUserInformation
    [Arguments]    ${FirstName}    ${LastName}    ${ContactTitle}    ${MobileNumber}    ${Email}    ${Password}    ${ConfirmPassword}    ${isAgree}
    InputText_rgp    ${CompanyReg_FirstName_txt}    ${FirstName}
    InputText_rgp    ${CompanyReg_LastName_txt}    ${LastName}
    InputText_rgp    ${CompanyReg_ContactTitle_txt}    ${ContactTitle}
    InputText_rgp    ${CompanyReg_MobileNumber_txt}    ${MobileNumber}
    InputText_rgp    ${CompanyReg_Email_txt}    ${Email}
    InputText_rgp    ${CompanyReg_Password_txt}    ${Password}
    InputText_rgp    ${CompanyReg_ConfirmPassword_txt}    ${ConfirmPassword}
    Run Keyword If    ${isAgree}=="True"    Select Checkbox    ${CompanyReg_Agree_ckb}
    Run Keyword If    ${isAgree}!="True"    Unselect Checkbox    ${CompanyReg_Agree_ckb}
    ClickButton_rgp    ${CompanyReg_CreateAccount_btn}

IndividualRegistration
    [Arguments]    ${FistName}    ${LastName}    ${Title}    ${DocumentType}    ${Last4DigitsOfID}    ${Email}    ${CreatePassword}    ${ConfirmPassword}    ${VendorCode}
    InputText_rgp    ${IndRegFirstName_txt}    ${FistName}
    InputText_rgp    ${IndRegLastName_txt}    ${LastName}
    InputText_rgp    ${IndRegTitle_txt}    ${Title}
    Run Keyword If    '${DocumentType}'!='${EMPTY}'    ClickElement_rgp    ${IndRegDocumentType_dd}
    Run Keyword If    '${DocumentType}'!='${EMPTY}'    ClickElement_rgp    \\li[text()='${DocumentType}')]
    InputText_rgp    ${IndRegLast4DigitsOfID_txt}    ${Last4DigitsOfID}
    InputText_rgp    ${IndRegEmailAddress_txt}    ${Email}
    InputText_rgp    ${IndRegCreatePassword_txt}    ${CreatePassword}
    InputText_rgp    ${IndRegConfirmPassword_txt}    ${ConfirmPassword}
    InputText_rgp    ${IndRegVendorCode_txt}    ${VendorCode}
    ClickButton_rgp    ${IndRegCreateAccount_btn}

LaunchBrowser
    ${chromeOptions}    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    ${disabled}    Create List    Chrome PDF Viewer
    ${prefs}    Create Dictionary    download.default_directory=${downloadDir}    plugins.plugins_disabled=${disabled}
    Call Method    ${chrome options}    add_experimental_option    prefs    ${prefs}
    Create Webdriver    Chrome    chrome_options=${chromeOptions}
    ${URL_str}    Set Variable If    "${Environment}"=="Test"    ${Url_Test}
    Go To    ${URL_str}
    Maximize Browser Window

LoginToFirstVerify
    [Arguments]    ${Role}
    ${UserName_str}    Set Variable If    "${Role}"=="admin"    ${UserName_admin}    "${Role}"=="client"    ${UserName_client}    "${Role}"=="employee"    ${UserName_employee}    "${Role}"=="vendor"    ${UserName_vendor}
    ${Password_str}    Set Variable If    "${Role}"=="admin"    ${Password_admin}    "${Role}"=="client"    ${Password_client}    "${Role}"=="employee"    ${Password_employee}    "${Role}"=="vendor"    ${Password_vendor}
    LaunchBrowser
    InputText_rgp    ${UserName_txt}    ${UserName_str}
    InputText_rgp    ${Password_txt}    ${Password_str}
    ClickButton_rgp    ${Login_btn}
    VerifyFirstCardIsDisplayed
    Comment    SelectClient    @{ClientName}

LoginHelp
    [Arguments]    ${Name}    ${Country}    ${PhoneNumber}    ${Email}    ${SubjectType}    ${Comment}
    InputText_rgp    ${LoginHelpName_txt}    ${Name}
    Run Keyword If    '${Country}'!='${EMPTY}'    ClickElement_rgp    ${LoginHelpSelectCountry_dd}
    Run Keyword If    '${Country}'!='${EMPTY}'    ClickElement_rgp    \\li[text()='${Country}')]
    InputText_rgp    ${LoginHelpPhoneNumber_txt}    ${PhoneNumber}
    InputText_rgp    ${LoginHelpEmailAddress_txt}    ${Email}
    Run Keyword If    '${SubjectType}'!='${EMPTY}'    ClickElement_rgp    ${LoginHelpSubjectType_txt}
    Run Keyword If    '${SubjectType}'!='${EMPTY}'    ClickElement_rgp    \\li[text()='${SubjectType}')]
    InputText_rgp    ${LoginHelpComments_txt}    ${Comment}
    ClickButton_rgp    ${LoginHelpSendRequest_btn}

ForgotPassword
    [Arguments]    ${Email}
    InputText_rgp    ${ForgetPwdEmail_txt}    ${Email}
    ClickButton_rgp    ${ForgetPwdResetPwd_btn}

VerifyFooterSection
    ${ActFooterText}    Get Text    //div[contains(@class,"footer MuiBox-root")]/span[1]
    ${ActEmail}    Get Text    //div[contains(@class,"footer MuiBox-root")]/a[1]
    ${ActContact}    Get Text    //div[contains(@class,"footer MuiBox-root")]/a[2]
    ${ActCopyright}    Get Text    //div[contains(@class,"footer MuiBox-root")]/span[2]
    ${ExpFooterText}    Set Variable    If you are having issues with the website, please contact us:
    ${ExpEmail}    Set Variable    support@firstverify.com
    ${ExpContact}    Set Variable    (402) 562-5930
    ${ExpCopyright}    Set Variable    © 2014–2022 FIRST, VERIFY. All Rights Reserved.
    Run Keyword If    '${ActFooterText}'!='${ExpFooterText}'    Fail    Footer Text is not matching
    Run Keyword If    '${ActEmail}'!='${ExpEmail}'    Fail    Footer Email is not matching
    Run Keyword If    '${ActContact}'!='${ExpContact}'    Fail    Footer Contact is not matching
    Run Keyword If    '${ActCopyright}'!='${ExpCopyright}'    Fail    Copyright Text is not matching
