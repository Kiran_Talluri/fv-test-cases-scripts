*** Settings ***
Resource          ../ReusableGroup.robot
Library           Selenium2Library
Resource          VendorDetailsPageObject.robot

*** Keywords ***
VerifyGeneralInformation
    [Arguments]    ${TaxId}    ${VendorId}    ${Started}
    ${ExpTaxId}    Get Text    ${VendorDetailGeneralInfoTaxID_lbl}
    ${ExpVendorId}    Get Text    ${VendorDetailGeneralInfoVendorID_lbl}
    ${ExpStarted}    Get Text    ${VendorDetailGeneralInfoStarted_lbl}
    Run Keyword If    ${ExpTaxId}!=${TaxId}    Fail    Tax ID is not matching
    Run Keyword If    ${ExpVendorId}!=${VendorId}    Fail    Vendor ID is not matching
    Run Keyword If    ${ExpStarted}!=${Started}    Fail    Started ID is not matching

VerifyAddressInformation
    [Arguments]    ${AddressLine1}    ${AddressLine2}    ${AddressLine3}
    ${ExpAddressLine1}    Get Text    ${VendorDetailAddressLine1_lbl}
    ${ExpAddressLine2}    Get Text    ${VendorDetailAddressLine2_lbl}
    ${ExpAddressLine3}    Get Text    ${VendorDetailAddressLine3_lbl}
    Run Keyword If    ${ExpAddressLine1}!=${AddressLine1}    Fail    AddressLine1 is not matching
    Run Keyword If    ${ExpAddressLine2}!=${AddressLine2}    Fail    AddressLine2 is not matching
    Run Keyword If    ${ExpAddressLine3}!=${AddressLine3}    Fail    AddressLine3 is not matching

VerifyContactInformation
    [Arguments]    ${Phone}    ${Fax}    ${Website}
    ${ExpPhone}    Get Text    ${VendorDetailContactInfoPhone_lbl}
    ${ExpFax}    Get Text    ${VendorDetailContactInfoFax_lbl}
    ${ExpWebsite}    Get Text    ${VendorDetailContactInfoWebsite_lbl}
    Run Keyword If    ${ExpPhone}!=${Phone}    Fail    Contact Information Phone is not matching
    Run Keyword If    ${ExpFax}!=${Fax}    Fail    Contact Information Fax is not matching
    Run Keyword If    ${ExpWebsite}!=${Website}    Fail    Contact Information Website is not matching

VerifyVendorContacts
    [Arguments]    ${ContactName}    ${ContactRole}    ${ContactEmail}    ${ContactPhone}    ${Position}
    ${ContactName_ltr}    Catenate    ${VendorDetailVendorContactCount_lbl}    [${Position}]    ${VendorDetailVendorContactName_lbl}
    ${ContactRole_ltr}    Catenate    ${VendorDetailVendorContactCount_lbl}    [${Position}]    ${VendorDetailVendorContactRole_lbl}
    ${ContactEmail_ltr}    Catenate    ${VendorDetailVendorContactCount_lbl}    [${Position}]    ${VendorDetailVendorContactEmail_lbl}
    ${ContactPhone_ltr}    Catenate    ${VendorDetailVendorContactCount_lbl}    [${Position}]    ${VendorDetailVendorContactPhone_lbl}
    ${ExpContactName}    Get Text    ${ContactName_ltr}
    ${ExpContactRole}    Get Text    ${ContactRole_ltr}
    ${ExpContactEmail}    Get Text    ${ContactEmail_ltr}
    ${ExpContactPhone}    Get Text    ${ContactPhone_ltr}
    Run Keyword If    ${ExpContactName}!=${ContactName}    Fail    \nVendor Contact Name is not matching \nExp:- ${ExpContactName}\nAct:-${ContactName}
    Run Keyword If    ${ExpContactRole}!=${ExpContactRole}    Fail    \nVendor Contact Role is not matching \nExp:- ${ExpContactRole}\nAct:-${ContactRole}
    Run Keyword If    ${ExpContactEmail}!=${ExpContactEmail}    Fail    \nVendor Contact Email is not matching \nExp:- ${ExpContactEmail}\nAct:-${ContactEmail}
    Run Keyword If    ${ExpContactPhone}!=${ExpContactPhone}    Fail    \nVendor Contact Phone is not matching \nExp:- ${ExpContactPhone}\nAct:-${ContactPhone}

VerifyVendorCompletedBy
    [Arguments]    ${CompletedByName}    ${CompletedByRole}    ${CompletedByDateTime}    ${CompletedByDate}    ${Position}
    ${CompletedByName_ltr}    Catenate    ${VendorDetailCompletedByCount_lbl}    [${Position}]    ${VendorDetailCompletedByName_lbl}
    ${CompletedByRole_ltr}    Catenate    ${VendorDetailCompletedByCount_lbl}    [${Position}]    ${VendorDetailCompletedByRole_lbl}
    ${CompletedByDateTime_ltr}    Catenate    ${VendorDetailCompletedByCount_lbl}    [${Position}]    ${VendorDetailCompletedByDateTime_lbl}
    ${CompletedByDate_ltr}    Catenate    ${VendorDetailCompletedByCount_lbl}    [${Position}]    ${VendorDetailCompletedByDate_lbl}
    ${ExpCompletedByName}    Get Text    ${CompletedByName_ltr}
    ${ExpCompletedByRole}    Get Text    ${CompletedByRole_ltr}
    ${ExpCompletedByDateTime}    Get Text    ${CompletedByDateTime_ltr}
    ${ExpCompletedByDate}    Get Text    ${CompletedByDate_ltr}
    Run Keyword If    ${ExpCompletedByName}!=${CompletedByName}    Fail    \nVendor CompletedBy Name is not matching \nExp:- ${ExpCompletedByName}\nAct:-${CompletedByName}
    Run Keyword If    ${ExpCompletedByRole}!=${ExpCompletedByRole}    Fail    \nVendor CompletedBy Role is not matching \nExp:- ${ExpCompletedByRole}\nAct:-${CompletedByRole}
    Run Keyword If    ${ExpCompletedByDateTime}!=${ExpCompletedByDateTime}    Fail    \nVendor CompletedBy DateTime is not matching \nExp:- ${ExpCompletedByDateTime}\nAct:-${CompletedByDateTime}
    Run Keyword If    ${ExpCompletedByDate}!=${ExpCompletedByDate}    Fail    \nVendor CompletedBy Date is not matching \nExp:- ${ExpCompletedByDate}\nAct:-${CompletedByDate}

DeleteLocation
    [Arguments]    ${CompletedByName}    ${CompletedByRole}    ${CompletedByDateTime}    ${CompletedByDate}    ${Position}
    ${CompletedByName_ltr}    Catenate    ${VendorDetailCompletedByCount_lbl}    [${Position}]    ${VendorDetailCompletedByName_lbl}
    ${CompletedByRole_ltr}    Catenate    ${VendorDetailCompletedByCount_lbl}    [${Position}]    ${VendorDetailCompletedByRole_lbl}
    ${CompletedByDateTime_ltr}    Catenate    ${VendorDetailCompletedByCount_lbl}    [${Position}]    ${VendorDetailCompletedByDateTime_lbl}
    ${CompletedByDate_ltr}    Catenate    ${VendorDetailCompletedByCount_lbl}    [${Position}]    ${VendorDetailCompletedByDate_lbl}
    ${ExpCompletedByName}    Get Text    ${CompletedByName_ltr}
    ${ExpCompletedByRole}    Get Text    ${CompletedByRole_ltr}
    ${ExpCompletedByDateTime}    Get Text    ${CompletedByDateTime_ltr}
    ${ExpCompletedByDate}    Get Text    ${CompletedByDate_ltr}
    Run Keyword If    ${ExpCompletedByName}!=${CompletedByName}    Fail    \nVendor CompletedBy Name is not matching \nExp:- ${ExpCompletedByName}\nAct:-${CompletedByName}
    Run Keyword If    ${ExpCompletedByRole}!=${ExpCompletedByRole}    Fail    \nVendor CompletedBy Role is not matching \nExp:- ${ExpCompletedByRole}\nAct:-${CompletedByRole}
    Run Keyword If    ${ExpCompletedByDateTime}!=${ExpCompletedByDateTime}    Fail    \nVendor CompletedBy DateTime is not matching \nExp:- ${ExpCompletedByDateTime}\nAct:-${CompletedByDateTime}
    Run Keyword If    ${ExpCompletedByDate}!=${ExpCompletedByDate}    Fail    \nVendor CompletedBy Date is not matching \nExp:- ${ExpCompletedByDate}\nAct:-${CompletedByDate}

SearchVendor
    [Arguments]    ${VendorName}

GoToVendorDetails
    [Arguments]    ${ClientName}    ${VendorSearchText}
    ClickButton_rgp    ${VendorSideMenu_btn}
    ClickLink_rgp    ${VendorSearchSideMenu_lnk}
    Wait Until Element Is Visible    ${VendorClientSelect_dd}    ${timeout}
    ${ele}    Get WebElement    ${VendorClientSelect_dd}
    Execute Javascript    arguments[0].click();    ARGUMENTS    ${ele}
    ClickElement_rgp    ${VendorClientSelect_dd}
    ClickElement_rgp    //ul[@role="listbox"]/li[text()="${ClientName}"]
    InputText_rgp    ${VendorSearch_txt}    ${VendorName}
    ClickElement_rgp    //a[text()="${VendorSearchText}"]
