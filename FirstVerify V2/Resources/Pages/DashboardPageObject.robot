*** Settings ***
Library           Selenium2Library
Resource          ../ReusableGroup.robot

*** Keywords ***
Dashboard_PO
    Set Suite Variable    ${DashboardTitle_lbl}    //h2[text()="Dashboard"]
    Set Suite Variable    ${DashboardGreaterThan30DaysCard_btn}    //h6[text()="Greater than 30 Days"]
    Set Suite Variable    ${DashboardWithin30DaysCard_btn}    //h6[text()="Within 30 Days"]
    Set Suite Variable    ${DashboardClientReviewCard_btn}    //h6[text()="Client Review"]
    Set Suite Variable    ${DashboardYesterdayCard_btn}    //h6[text()="Yesterday"]
    Set Suite Variable    ${DashboardRemindersCard_btn}    //h6[text()="Reminders"]
    Set Suite Variable    ${DashboardGreaterThan30DaysValue_lbl}    //h6[text()="Greater than 30 Days"]/following::div/h6
    Set Suite Variable    ${DashboardWithin30DaysValue_lbl}    //h6[text()="Within 30 Days"]/following::div/h6
    Set Suite Variable    ${DashboardClientReviewValue_lbl}    //h6[text()="Client Review"]/following::div/h6
    Set Suite Variable    ${DashboardYesterdayValue_lbl}    //h6[text()="Yesterday"]/following::div/h6
    Set Suite Variable    ${DashboardRemindersValue_lbl}    //h6[text()="Reminders"]/following::div/h6
    Set Suite Variable    ${DashboardRecendDocuments_btn}    //span[text()="Recent Documents"]/parent::a
    Set Suite Variable    ${DashboardDashboardAnalytics_btn}    //span[text()="Dashboard Analytics"]/parent::a
    Set Suite Variable    ${DashboardSearch_txt}    //input[@placeholder="Search"]
    Set Suite Variable    ${DashboardDownload_btn}    //button[@id="menu_btn"]
    Set Suite Variable    ${DashboardListGridView_btn}    //button[@id="menu_btn"]/following-sibling::button
    Set Suite Variable    ${DashboardClientSearch_txt}    //input[contains(@class,"MuiInputBase-input MuiOutlinedInput-input")]
    Set Suite Variable    ${DashboardNoResultsFound_lbl}    //div[text()="No results found!"]
    Set Suite Variable    ${DashboardResultGrid_grid}    //div[contains(@class,"MuiCard-root grid-card")]
    Set Suite Variable    ${DashboardResultList_List}    //div[contains(@class,"collections-list-view MuiBox-root css-0")]/div
    Set Suite Variable    ${DashboardButtonTooltip_lbl}    //div[@role="tooltip"]
    Set Suite Variable    ${DashboardFirstCardProcessing_btn}    (//span[text()="Processing"])[1]/parent::*/parent::*
    Set Suite Variable    ${DashboardFirstCardClientReview_btn}    (//span[text()="Client Review"])[1]/parent::*/parent::*
    Set Suite Variable    ${DashboardFirstCardIncomplete_btn}    (//span[text()="Incomplete"])[1]/parent::*/parent::*
    Set Suite Variable    ${DashboardSelectClientSave_btn}    //button[text()="Save"]
    Set Suite Variable    ${DashboardSelectClientCancel_btn}    //button[text()="Cancel"]
    Set Suite Variable    ${DashboardVendorName_lbl}    //h6[text()="Vendor Name"]
    Set Suite Variable    ${DashboardClientName_lbl}    //h6[text()="Client"]
    Set Suite Variable    ${DashboardStatus_lbl}    //h6[text()="Status"]
    Set Suite Variable    ${DashboardSubmitted_lbl}    //h6[text()="Submitted"]
    Set Suite Variable    ${DashboardStatusPeriod_lbl}    //h6[text()="Status Period"]
    Set Suite Variable    ${DashboardLogo_img}    //div[@class="MuiBox-root css-i9gxme"]
    Set Suite Variable    ${DashboardGlobalClientAddIcon_btn}    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[1]/div[2]/ul/li[1]/div[2]/button
    Set Suite Variable    ${DashboardGlobalClientDeleteIcon_btn}    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div[1]/*[2]
    Set Suite Variable    ${DashboardGlobalClientDeleteSearch_txt}    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[1]/div/input
    Set Suite Variable    ${DashboardGlobalClientAddSearch_txt}    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[1]/div[1]/div/input
    Set Suite Variable    ${DashboardGlobalClientAddFirstelement_lbl}    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[1]/div[2]/ul/li[1]/div[1]/div/span
    Set Suite Variable    ${DashboardGlobalClientDeleteFirstelement_lbl}    (//div[contains(@class,"MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6 ")])[2]/div[2]/ul/div[1]/span
    Set Suite Variable    ${DashboardFirstCard}    (//div[contains(@class,"MuiCard-root grid-card")])[1]
    Set Suite Variable    ${DashboardGoToPage_txt}    //label[text()="Go to page"]/parent::div/parent::div/div[2]/div/input
    Set Suite Variable    ${MenuHome_btn}    //a[@href="/DashBoard"]
