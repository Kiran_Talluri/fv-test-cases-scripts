*** Keywords ***
CompanyRegistration_PO
    Set Suite Variable    ${CompRegHeading_lbl}    //h1[text()="Company Registration"]
    Set Suite Variable    ${CompRegFirstVerifyLogo_img}    //div[@class="first-verify__logo"]
    Set Suite Variable    ${CompRegBusinessInfo_mnu}    //span[text()="Business Information"]
    Set Suite Variable    ${CompRegAddressInfo_mnu}    //span[text()="Address Information"]
    Set Suite Variable    ${CompRegContactInfo_mnu}    //span[text()="Contact Information"]
    Set Suite Variable    ${CompRegPrimaryUserInfo_mnu}    //span[text()="Primary User Information"]
    Set Suite Variable    ${CompRegTypeInternational_btn}    //button[text()="International"]
    Set Suite Variable    ${CompRegTypeUS_btn}    //button[text()="US"]
    Set Suite Variable    ${CompRegLegalNameOfBusiness_txt}    (//span[text()="Legal Name of Business"]/following::input)[1]
    Set Suite Variable    ${CompRegTaxID_txt}    (//span[text()="TAX ID"]/following::input)[1]
    Set Suite Variable    ${CompRegComPrincipalOfficerName_txt}    (//span[text()="Company Principal Officer Name"]/following::input)[1]
    Set Suite Variable    ${CompRegAddress1_txt}    (//span[text()="Address Line 1"]/following::input)[1]
    Set Suite Variable    ${CompRegAddress2_txt}    (//span[text()="Address Line 2"]/following::input)[1]
    Set Suite Variable    ${CompRegAddress3_txt}    (//span[text()="Address Line 3"]/following::input)[1]
    Set Suite Variable    ${CompRegCountry_txt}    (//span[text()="Country"]/following::input)[1]
    Set Suite Variable    ${CompRegStateAndProvince_txt}    (//span[text()="State or Province"]/following::input)[1]
    Set Suite Variable    ${CompRegZip/PostalCode_txt}    (//span[text()="Zip/PostalCode"]/following::input)[1]
    Set Suite Variable    ${CompRegCity_txt}    (//span[text()="City"]/following::input)[1]
    Set Suite Variable    ${CompRegLanguage_lst}    (//span[text()="Select Language"]/following::div)[1]
    Set Suite Variable    ${CompRegVendorRegion_txt}    (//span[text()="Vendor Operating Region"]/following::div)[1]
    Set Suite Variable    ${CompRegFirstName_txt}    (//label[text()="First Name"]/following::input)[1]
    Set Suite Variable    ${CompRegLastName_txt}    (//label[text()="Last Name"]/following::input)[1]
    Set Suite Variable    ${CompRegTitle_txt}    (//label[text()="Title"]/following::input)[1]
    Set Suite Variable    ${CompRegPhoneNumber_txt}    (//label[text()="Phone Number"]/following::input)[1]
    Set Suite Variable    ${CompRegEmailAddress_txt}    (//label[text()="Email Address"]/following::input)[1]
    Set Suite Variable    ${CompRegPassword_txt}    (//label[text()="Password"]/following::input)[1]
    Set Suite Variable    ${CompRegConfirmPassword_txt}    (//label[text()="Confirm Password"]/following::input)[1]
    Set Suite Variable    ${CompRegIAgree_ckb}    (//span[text()="I agree to the FIRST, VERIFY Terms & Conditions."]/preceding::input[@type="checkbox"])
    Set Suite Variable    ${CompRegIAgree_lbl}    //span[text()="I agree to the FIRST, VERIFY Terms & Conditions."]
    Set Suite Variable    ${CompRegCreateAccount_btn}    //button[text()="Create Account"]
    Set Suite Variable    ${CompRegCancel_lnk}    //button[text()="Back"]
    Set Suite Variable    ${CompRegNext_btn}    //button[text()="Next"]
    Set Suite Variable    ${CompRegBackToLogin_btn}    //button[text()="Back to Login"]

ForgotPassword_PO
    Set Suite Variable    ${ForgetPwdKeyLogo_img}    //div[@class="first-verify__logo"]
    Set Suite Variable    ${ForgetPwdHeading_span}    //span[text()="Forget Password?"]
    Set Suite Variable    ${ForgetPwdSubHeading_span}    //span[text()="No worries, we will send you reset instructions."]
    Set Suite Variable    ${ForgetPwdEmail_txt}    //input[@placeholder="Enter your Email"]
    Set Suite Variable    ${ForgetPwdResetPwd_btn}    (//button[text()="Reset Password"])[2]
    Set Suite Variable    ${ForgetPwdBackToLogin_btn}    (//button[text()="Back to Login"])[2]
    Set Suite Variable    ${ForgetPwdEmailLogo_img}    //div[@class="first-verify__emaillogoContainer"]
    Set Suite Variable    ${ForgetPwdEmailHeading_span}    //span[text()="Check your email"]
    Set Suite Variable    ${ForgetPwdCheckYourEmail_btn}    //button[text()="check your email"]
    Set Suite Variable    ${ForgetPwdReset_span}    //span[text()="Didn't receive the email?"]
    Set Suite Variable    ${ForgetPwdReset_lnk}    //span[text()="Click to Reset"]

IndividualRegistration_PO
    Set Suite Variable    ${IndRegHeading_lbl}    //h1[text()="Employee Registration"]
    Set Suite Variable    ${IndRegFirstName_txt}    //input[@placeholder="First Name"]
    Set Suite Variable    ${IndRegLastName_txt}    //input[@placeholder="Last Name"]
    Set Suite Variable    ${IndRegTitle_txt}    //input[@placeholder="Title"]
    Set Suite Variable    ${IndRegDocumentType_dd}    (//span[text()="Document Type*"]/following::div)[2]
    Set Suite Variable    ${IndRegLast4DigitsOfID_txt}    //input[@placeholder="Last 4 Digits of ID"]
    Set Suite Variable    ${IndRegEmailAddress_txt}    //input[@placeholder="Email Address"]
    Set Suite Variable    ${IndRegCreatePassword_txt}    //input[@placeholder="Create Password"]
    Set Suite Variable    ${IndRegConfirmPassword_txt}    //input[@placeholder="Confirm Password"]
    Set Suite Variable    ${IndRegVendorCode_txt}    //input[@placeholder="Vendor Code"]
    Set Suite Variable    ${IndRegCreateAccount_btn}    //button[text()="Create Account"]
    Set Suite Variable    ${IndRegCancel_btn}    //button[text()="Cancel"]
    Set Suite Variable    ${IndRegBackToLogin_btn}    //button[text()="Back to Login"]

InitializeLoginPageObjects
    Login_PO
    Registration_PO
    CompanyRegistration_PO
    IndividualRegistration_PO
    ForgotPassword_PO
    LoginHelp_PO

LoginHelp_PO
    Set Suite Variable    ${LoginHelpName_txt}    (//label[text()="Name"]/following::input)[1]
    Set Suite Variable    ${LoginHelpSelectCountry_dd}    //button[@aria-label="Select country"]
    Set Suite Variable    ${LoginHelpPhoneNumber_txt}    (//span[text()="Phone Number*"]/following::input)[1]
    Set Suite Variable    ${LoginHelpEmailAddress_txt}    (//label[text()="Email Address"]/following::input)[1]
    Set Suite Variable    ${LoginHelpSubjectType_txt}    (//span[text()="Subject Type*"]/following::div)[1]
    Set Suite Variable    ${LoginHelpComments_txt}    (//label[text()="Type comments here"]/following::input)[1]
    Set Suite Variable    ${LoginHelpSendRequest_btn}    //button[text()="Send Request"]
    Set Suite Variable    ${LoginHelpHeader_txt}    //h2[text()="Login Help"]
    Set Suite Variable    ${LoginHelpClose_btn}    //button[@aria-label="close"]

Login_PO
    Set Suite Variable    ${FirstverifyLogo_img}    //div[@class="first-verify__logo"]
    Set Suite Variable    ${LoginHeading_lbl}    //span[text()="Login to your account"]
    Set Suite Variable    ${LoginSubHeading_lbl}    //span[text()="Welcome Back! Please enter your details"]
    Set Suite Variable    ${UserName_lbl}    //span[text()="User ID (email/username/phone no.)"]
    Set Suite Variable    ${UserName_txt}    //*[@placeholder="User ID"]
    Set Suite Variable    ${Password_lbl}    //span[text()="Password"]
    Set Suite Variable    ${Password_txt}    //*[@placeholder="Password"]
    Set Suite Variable    ${Login_btn}    //button[text()="Log In"]
    Set Suite Variable    ${Remember_lbl}    //span[text()="Remember Me"]
    Set Suite Variable    ${RememberMe_ckb}    (//span[text()="Remember Me"]/preceding::input)[3]
    Set Suite Variable    ${ForgotPassword_btn}    //button[text()="Forgot Password"]
    Set Suite Variable    ${SignUp_lbl}    //div[text()="Don't have an account?"]
    Set Suite Variable    ${SignUp_btn}    //button[text()="Sign Up"]
    Set Suite Variable    ${NeedHelp_btn}    //button[text()="Need Login Help?"]

Registration_PO
    Set Suite Variable    ${RegistrationHeading_lbl}    //span[text()="Registration"]
    Set Suite Variable    ${RegistrationSubHeading_lbl}    //span[text()="Note: If you have registered, please DO NOT register again."]
    Set Suite Variable    ${CompanyRegistration_btn}    //span[text()="Company Registration for Prequalification"]/parent::div/..
    Set Suite Variable    ${IndividualRegistration_btn}    //span[text()="Individual Registration for Training"]/parent::div
    Set Suite Variable    ${BackToLogin_btn}    //button[text()="Back to Login"]
    Set Suite Variable    ${MandatoryErrMsg_div}    //div[text()="Please complete required fields!"]
