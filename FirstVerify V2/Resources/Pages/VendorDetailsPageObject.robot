*** Keywords ***
VendorDetails_PO
    Set Suite Variable    ${VendorSideMenu_btn}    //div[contains(@class,"side-menu css-")]/*/ul/li[2]/button
    Set Suite Variable    ${VendorSearchSideMenu_lnk}    //a[text()="Vendor Search"]
    Set Suite Variable    ${VendorSearch_txt}    //input[@placeholder="Search vendor"]
    Set Suite Variable    ${VendorClientSelect_dd}    (//div[@aria-haspopup="listbox" and @role="button"])[1]
    Set Suite Variable    ${VendorDetailVendorName_lbl}    //div[contains(@class,"outlet-container MuiBox-root")]/div[1]/div/div/div[3]/span[1]
    Set Suite Variable    ${VendorDetailClientName_lbl}    //div[contains(@class,"outlet-container MuiBox-root")]/div[1]/div/div/div[3]/span[2]
    Set Suite Variable    ${VendorDetailDate_lbl}    //div[contains(@class,"outlet-container MuiBox-root")]/div[1]/div/div/div[3]/span[3]
    Set Suite Variable    ${VendorDetailTab_btn}    //button[text()="Vendor Details"]
    Set Suite Variable    ${VendorDetailQuestionnare_btn}    //button[text()="Questionnaire"]
    Set Suite Variable    ${VendorDetailLocations_txt}    //h6[text()="Locations"]
    Set Suite Variable    ${VendorDetailLocationsCount_lbl}    ${VendorDetailLocations_txt}/parent::div/div
    Set Suite Variable    ${VendorDetailLocationName_lbl}    ${VendorDetailLocations_txt}/div[2]/div/div/div/h5
    Set Suite Variable    ${VendorDetailLocationDelete_btn}    ${VendorDetailLocations_txt}/div[2]/div/div/div/*[2]
    Set Suite Variable    ${VendorDetailLocationStatus_lbl}    ${VendorDetailLocations_txt}/div[2]/div/div/div[2]/div/span
    Set Suite Variable    ${VendorDetailGeneralInfo_lbl}    //h5[text()="General infomation"]
    Set Suite Variable    ${VendorDetailGeneralInfoTaxID_lbl}    //span[text()="Tax ID: "]/following-sibling::span
    Set Suite Variable    ${VendorDetailGeneralInfoVendorID_lbl}    //span[text()="Vendor ID:"]/following-sibling::span
    Set Suite Variable    ${VendorDetailGeneralInfoStarted_lbl}    //span[text()="Started:"]/following-sibling::span
    Set Suite Variable    ${VendorDetailAddress_lbl}    //h5[text()="Address"]
    Set Suite Variable    ${VendorDetailAddressLine1_lbl}    ${VendorDetailAddress_lbl}/following-sibling::h5[1]/span
    Set Suite Variable    ${VendorDetailAddressLine2_lbl}    ${VendorDetailAddress_lbl}/following-sibling::h5[2]/span
    Set Suite Variable    ${VendorDetailAddressLine3_lbl}    ${VendorDetailAddress_lbl}/following-sibling::h5[3]/span
    Set Suite Variable    ${VendorDetailContactInfo_lbl}    //h5[text()="Contact Information"]
    Set Suite Variable    ${VendorDetailContactInfoPhone_lbl}    //span[text()="Phone:"]/following-sibling::span
    Set Suite Variable    ${VendorDetailContactInfoFax_lbl}    //span[text()="Fax:"]/following-sibling::span
    Set Suite Variable    ${VendorDetailContactInfoWebsite_lbl}    //span[text()="Website :"]/following-sibling::span
    Set Suite Variable    ${VendorDetailVendorContact_lbl}    //h6[text()="Vendor Contacts"]
    Set Suite Variable    ${VendorDetailVendorContactCount_lbl}    //h6[text()="Vendor Contacts"]/parent::div/div/div/div
    Set Suite Variable    ${VendorDetailVendorContactName_lbl}    ${VendorDetailVendorContact_lbl}/div/h6[1]
    Set Suite Variable    ${VendorDetailVendorContactRole_lbl}    ${VendorDetailVendorContact_lbl}/div/h6[2]
    Set Suite Variable    ${VendorDetailVendorContactEmail_lbl}    ${VendorDetailVendorContact_lbl}/div/h6[3]
    Set Suite Variable    ${VendorDetailVendorContactPhone_lbl}    ${VendorDetailVendorContact_lbl}/div/h6[4]
    Set Suite Variable    ${VendorDetailCompletedBy_lbl}    //h6[text()="Completed By"]
    Set Suite Variable    ${VendorDetailCompletedByCount_lbl}    //h6[text()="Completed By"]/parent::div/div/div/div
    Set Suite Variable    ${VendorDetailCompletedByName_lbl}    ${VendorDetailCompletedByCount_lbl}/div/h6[1]
    Set Suite Variable    ${VendorDetailCompletedByRole_lbl}    ${VendorDetailCompletedByCount_lbl}/div/h6[2]
    Set Suite Variable    ${VendorDetailCompletedByDateTime_lbl}    ${VendorDetailCompletedByCount_lbl}/div/h6[3]
    Set Suite Variable    ${VendorDetailCompletedByDate_lbl}    ${VendorDetailCompletedByCount_lbl}/div/h6[4]
    Set Suite Variable    ${VendorDetailContractPerfEval_lbl}    //h6[text()="Contractor Performance Evaluation"]
    Set Suite Variable    ${VendorDetailContractPerfEvalCount_lbl}    ${VendorDetailContractPerfEval_lbl}/parent::div/parent::div/div[2]/div/div
    Set Suite Variable    ${VendorDetailCompletedEvaluation_btn}    //button[text()="Completed Evaluations"]
    Set Suite Variable    ${VendorDetailAvgEvaluation_lbl}    ${VendorDetailContractPerfEvalCount_lbl}[1]/div/span[1]/span
    Set Suite Variable    ${VendorDetailAvgEvaluationCompletedBy_lbl}    ${VendorDetailContractPerfEvalCount_lbl}[1]/div/span[2]/strong

Questionnaire_PO
    Set Suite Variable    ${QuestionnaireSectionCount_lbl}    //div[contains(@class,"MuiStepper-root MuiStepper-horizontal")]/div
    Set Suite Variable    ${QuestionnaireSectionName_lbl}    /span/descendant::div[@class="MuiBox-root css-0"]
    Set Suite Variable    ${QuestionnaireSubSectionCount_lbl}    //ul[contains(@class,"MuiList-root MuiList-padding outlined-tabs")]/li
    Set Suite Variable    ${QuestionnaireSubSectionName_lbl}    /button/span
    Set Suite Variable    ${QuestionnaireSubSectionValue_lbl}    /button/div
    Set Suite Variable    ${QuestionnairePrevious_btn}    //span[text()="Previous"]/parent::button
    Set Suite Variable    ${QuestionnaireNext_btn}    //span[text()="Next"]/parent::button
    Set Suite Variable    ${QuestionnaireSave_btn}    //span[text()="Save"]/parent::button
    Set Suite Variable    ${QuestionnaireCancel_btn}    //span[text()="Cancel"]/parent::button
    Set Suite Variable    ${QuestionnaireSaveAndContinue_btn}    //span[text()="Save and Continue "]/parent::button
