*** Settings ***
Library           Selenium2Library
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           String

*** Keywords ***
ClickButton_rgp
    [Arguments]    ${Xpath}
    Wait Until Element Is Visible    ${Xpath}    ${timeout}
    Wait Until Element Is Enabled    ${Xpath}    ${timeout}
    Click Button    ${Xpath}

ClickElement_rgp
    [Arguments]    ${Xpath}
    Wait Until Element Is Visible    ${Xpath}    ${timeout}
    Wait Until Element Is Enabled    ${Xpath}    ${timeout}
    Click Element    ${Xpath}

ClickLink_rgp
    [Arguments]    ${Xpath}
    Wait Until Element Is Visible    ${Xpath}    ${timeout}
    Wait Until Element Is Enabled    ${Xpath}    ${timeout}
    Click Link    ${Xpath}

InputText_rgp
    [Arguments]    ${Xpath}    ${Value}
    Wait Until Element Is Visible    ${Xpath}    ${timeout}
    Log    ${Xpath}
    Clear Element Text    ${Xpath}
    Input Text    ${Xpath}    ${Value}

VerifyTextIsDisplayed
    [Arguments]    ${Xpath}    ${Exp_txt}    ${FieldName}
    ${Act_txt}    Get Text    ${Xpath}
    Run Keyword If    '${Act_txt}'!='${Exp_txt}'    Fail    Actual and Expected is not matching\n Actual:- ${Act_txt}\n Expected:- ${Exp_txt}\n FieldName:- ${FieldName}

VerifyElementIsDisplayed
    [Arguments]    ${Xpath}    ${FieldName}
    Run Keyword And Return Status    Wait Until Element Is Visible    ${Xpath}    ${timeout}
    Log    ${Xpath}
    ${Status}    Run Keyword And Return Status    Get Text    ${Xpath}
    Run Keyword If    '${Status}'!='True'    Run Keyword And Continue On Failure    Fail    ${FieldName} is not displayed or the Value is incorrect

VerifyElementIsNotDisplayed
    [Arguments]    ${Xpath}    ${FieldName}
    Run Keyword And Return Status    Wait Until Element Is Not Visible    ${Xpath}    ${timeout}
    ${Status}    Run Keyword And Return Status    Get Text    ${Xpath}
    Run Keyword If    '${Status}'!='False'    Capture Page Screenshot    ${FieldName}.png
    Run Keyword If    '${Status}'!='False'    Fail    ${FieldName} is displayed

GetAuthToken
    ${Request}    Set Variable    { \ \ "userName": "FVTestingTeam@ahaapps.com", \ \ "password": "Testing#123" }
    ${Wrapper}    Set Variable    /api/Login/Login
    &{headers}    Create Dictionary    Content-Type=application/json
    create session    mysession    https://fvapi.firstverify.com    headers=${headers}
    ${Response}    POST On Session    mysession    ${Wrapper}    data=${Request}
    ${Response}    Set Variable    ${Response.text}
    ${JsonObj_Res}=    Evaluate    json.loads("""${Response}""")    json
    ${Token}    Get Value From Json    ${JsonObj_Res}    $..token
    ${Token}    Set Variable    Bearer ${Token}[0]
    Log    ${Token}
    Set Suite Variable    ${Token}
    [Return]    ${Token}

PostFVRequest
    [Arguments]    ${Wrapper}    ${Request}
    &{headers}    Create Dictionary    Content-Type=application/json    Authorization=${Token}
    create session    mysession    https://fvapi.firstverify.com    headers=${headers}
    ${Response}    POST On Session    mysession    ${Wrapper}    data=${Request}
    ${Response}    Set Variable    ${Response.text}
    [Return]    ${Response}

GetFVRequest
    [Arguments]    ${Wrapper}
    &{headers}    Create Dictionary    Content-Type=application/json    Authorization=${Token}
    create session    mysession    https://fvapi.firstverify.com    headers=${headers}
    ${Response}    GET On Session    mysession    ${Wrapper}
    ${Response}    Set Variable    ${Response.text}
    Log    ${Response}
    ${Response}    Replace String    ${Response}    \\u003E    >    count=-1
    ${Response}    Replace String    ${Response}    \\u003C    <    count=-1
    ${Response}    Replace String    ${Response}    \\u0027    '    count=-1
    ${Response}    Replace String    ${Response}    \\u0026    &    count=-1
    ${Response}    Replace String    ${Response}    \\u0022\\u0022    ''    count=-1
    ${Response}    Replace String    ${Response}    =\\u0022    ='    count=-1
    ${Response}    Replace String    ${Response}    ;\\u0022    ;'    count=-1
    ${Response}    Replace String    ${Response}    \\u0022    ~!    count=-1
    ${Response}    Replace String    ${Response}    \\n    ${SPACE}    count=-1
    ${Response}    Replace String    ${Response}    &#39;    '    count=-1
    ${Response}    Replace String    ${Response}    \\r    ${EMPTY}    count=-1
    ${Response}    Replace String    ${Response}    \\t    ${EMPTY}    count=-1
    ${Response}    Replace String    ${Response}    . \ Electrical    . Electrical    count=-1
    ${Response}    Replace String    ${Response}    ${SPACE}${SPACE}    ${SPACE}    count=-1
    Comment    ${Response}    Replace String    ${Response}    ${SPACE}${SPACE}${SPACE}    ${SPACE}    count=-1
    ${Response}    Replace String    ${Response}    "58":    "subSectionDetails":    count=-1
    Log    ${Response}
    ${Response}    Evaluate    json.loads("""${Response}""")    json
    Log    ${Response}
    [Return]    ${Response}

CompareResults
    [Arguments]    ${Actual}    ${Expected}    ${FieldName}
    ${Expected}    Convert To String    ${Expected}
    ${Actual}    Convert To String    ${Actual}
    ${Expected}    Strip String    ${Expected}
    ${Actual}    Strip String    ${Actual}
    Run Keyword If    '${Expected}'!='${Actual}'    Fail    ${FieldName} is not matching\nActual:${Actual}\nExpected:${Expected}

RemoveHTMLTags
    [Arguments]    ${InputString}
    Log    ${InputString}
    ${InputString}    Replace String    ${InputString}    </div>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    </i>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <i>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    </b>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <b>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    <br>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    <br />    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    &gt;    >    count=-1
    ${InputString}    Replace String    ${InputString}    &lt;    <    count=-1
    ${InputString}    Replace String    ${InputString}    &nbsp;    ${SPACE}    count=-1
    ${InputString}    Replace String    ${InputString}    &amp;    &    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}style='text-align: justify;'    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}style='text-align: center;'    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}style='font-style: italic;'    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}style='font-style: italic; text-decoration-line: underline;'    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}style='border: none;'    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}style=''    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}class='contentSub'    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <span style='color: rgb(51, 51, 51);'>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    &ndash;    –    count=-1
    ${InputString}    Replace String    ${InputString}    padding: 0px;    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    border: none;    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    margin: 0 0 0 40px;    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}style=' \ '    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}style=' '    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    </span>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <p>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    </p>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <blockquote>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    </blockquote>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <b>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    <i>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <em>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    <u>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    &nbsp;    ${SPACE}    count=-1
    ${InputString}    Replace String    ${InputString}    </b>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    </em>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    </u>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <ul>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    </ul>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <li>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    </li>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    </strong>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    <strong>    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    ~!    "    count=-1
    ${InputString}    Replace String    ${InputString}    &rsquo;    <div>    count=-1
    ${InputString}    Replace String    ${InputString}    &quot;    "    count=-1
    ${InputString}    Replace String    ${InputString}    <span>    ${EMPTY}    count=-1
    ${InputString}    Replace String    ${InputString}    ${SPACE}${SPACE}    <div>    count=-1
    Log    ${InputString}
    [Return]    ${InputString}

GetValueFromResponse
    [Arguments]    ${ResponseObj}    ${Xpath}
    ${Output}    Get Value From Json    ${ResponseObj}    ${Xpath}
    Log    ${Xpath}
    ${Value}    Run Keyword And Return Status    Set Variable    ${Output}[0]
    ${Value}    Set Variable If    '${Value}'=='True'    ${Output}[0]    ${EMPTY}
    ${Value}    Set Variable If    """${Value}"""=="None" or """${Value}"""=="${EMPTY}"    ${EMPTY}    ${Value}
    Log    ${Value}
    [Return]    ${Value}

CompareElementText
    [Arguments]    ${Xpath}    ${Expected}    ${FieldName}
    ${Status}    Run Keyword And Return Status    Get Text    ${Xpath}
    ${Actual}    Run Keyword If    "${Status}"=="True"    Get Text    ${Xpath}
    ...    ELSE    Set Variable    ${EMPTY}
    ${Expected}    CompareResults    ${Actual}    ${Expected}    ${FieldName}

Scroll to the element
    [Arguments]    ${xpath}    ${direction}
    ${xpath_Updated}    Replace String    ${xpath}    "    '    count=-1
    Run Keyword And Return Status    Execute Javascript    document.evaluate("${xpath_Updated}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView();
    Sleep    1s
    ${direction}    Convert To Lower Case    ${direction}
    Run Keyword If    "${direction}"=="down"    Press Keys    ${xpath}    ARROW_DOWN
    Run Keyword If    "${direction}"=="up"    Press Keys    ${xpath}    ARROW_UP
