*** Settings ***
Resource          Pages/LoginAndRegistrationPageObject.robot
Resource          Pages/DashboardPageObject.robot
Resource          Pages/VendorDetailsPageObject.robot

*** Keywords ***
Config
    InitializeAllWebElements
    Set Suite Variable    ${Environment}    Test
    Set Suite Variable    ${Url_Test}    http://fvpq2.firstverify.com/Login
    Set Suite Variable    ${UserName_admin}    FVTestingTeam@ahaapps.com
    Set Suite Variable    ${Password_admin}    Testing#123
    Set Suite Variable    ${UserName_client}    FVTestingTeam@ahaapps.com
    Set Suite Variable    ${Password_client}    Password#123
    Set Suite Variable    ${UserName_employee}    SamBrown1001
    Set Suite Variable    ${Password_employee}    Testing#123
    Set Suite Variable    ${UserName_vendor}    FVTestingTeam@ahaapps.com
    Set Suite Variable    ${Password_vendor}    Testing#123
    Set Suite Variable    ${downloadDir}    ${CURDIR}\\..\\Download\\
    Set Suite Variable    @{ClientName}    Clayco    Green Plains, Inc.
    Set Suite Variable    ${timeout}    30s

InitializeAllWebElements
    InitializeLoginPageObjects
    Dashboard_PO
    VendorDetails_PO
