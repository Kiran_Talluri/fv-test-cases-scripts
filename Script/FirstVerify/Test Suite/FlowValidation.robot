*** Settings ***
Resource          ../Resource/Properties.robot
Resource          ../Resource/Pages/LoginAndRegistrationPage.robot
Resource          ../Resource/Pages/LoginAndRegistrationPage.robot
Resource          ../Resource/Pages/MyAccountPage.robot
Resource          ../Resource/Pages/ClientPage.robot
Resource          ../Resource/Pages/VendorPage.robot

*** Test Cases ***
Invite the client from admin user
    [Setup]    Config
    Comment    ${isUS}    Set Variable    true
    Comment    Run Keyword If    '${isUS}'=='True'    Click Element    ${CompanyReg_TypeInternational_rbtn}
    ...    ELSE IF    '${isUS}'=='False'    Click Element    ${CompanyReg_TypeUS_rbtn}
    Comment    LoginToFirstVerify    Test    Admin
    Comment    MyAcc_SearchUser_Advanced    ${EMPTY}    Any    Absolute Energy, L.L.C.    ${EMPTY}    Any    Chatfield, Bridget
    Comment    Client_InviteVendor    Test Automation    Deepak    Kishore S    dk1@gmail.com    Address1    Address2    Address3    city    pincode    United States    Alabama    English    Safety    ${EMPTY}    Test Comment
    ...    ${EMPTY}    ${EMPTY}
    Comment    LaunchBrowser    Test
    Comment    CompanyRegistration    True    Deepak Kishore    Q123456789    Test    123    Test    Test    city    pincode    United States    Alabama    English    United States    1234567890    123
    ...    123456789    test    Deepak    Kishore    Automation    1234567890    deepakkishore.07@gmail.com    Qwertyuiop@1    Qwertyuiop@1
    LoginToFirstVerify    Test    ${EMPTY}    deepakkishore.07@gmail.com    Qwertyuiop@1
    PreQualify
