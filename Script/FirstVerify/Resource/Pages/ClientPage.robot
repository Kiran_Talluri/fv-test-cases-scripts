*** Settings ***
Library           Selenium2Library
Resource          ../ReusableGroup.robot

*** Keywords ***
Client_InviteVendor_PO
    ${Client_InviteVendor_NewVendor_mnu}    Set Variable    //*[@id="tab1"]
    ${Client_InviteVendor_ExpiredVendor_mnu}    Set Variable    //*[@id="tab2"]
    ${Client_InviteVendor_OutsideNetwork_mnu}    Set Variable    //*[@id="tab3"]
    ${Client_InviteVendor_NewVendor_Location_lst}    Set Variable    //*[@id="Location"]
    ${Client_InviteVendor_NewVendor_VendorCompanyName_txt}    Set Variable    //*[@id="VendorCompanyName"]
    ${Client_InviteVendor_NewVendor_FirstName_txt}    Set Variable    //*[@id="FullName"]
    ${Client_InviteVendor_NewVendor_LastName_txt}    Set Variable    //*[@id="LastName"]
    ${Client_InviteVendor_NewVendor_EmailAddress_txt}    Set Variable    //*[@id="EmailAddress"]
    ${Client_InviteVendor_NewVendor_Address1_txt}    Set Variable    //*[@id="Address1"]
    ${Client_InviteVendor_NewVendor_Address2_txt}    Set Variable    //*[@id="Address2"]
    ${Client_InviteVendor_NewVendor_Address3_txt}    Set Variable    //*[@id="Address3"]
    ${Client_InviteVendor_NewVendor_City_txt}    Set Variable    //*[@id="City"]
    ${Client_InviteVendor_NewVendor_PostalCode_txt}    Set Variable    //*[@id="Zip"]
    ${Client_InviteVendor_NewVendor_Country_lst}    Set Variable    //*[@id="Country"]
    ${Client_InviteVendor_NewVendor_State_lst}    Set Variable    //*[@id="State"]
    ${Client_InviteVendor_NewVendor_Language_lst}    Set Variable    //*[@id="Language"]
    ${Client_InviteVendor_NewVendor_RiskLevel_lst}    Set Variable    //*[@id="RiskLevel"]
    ${Client_InviteVendor_NewVendor_RiskLevel_Low_Agree_ckb}    Set Variable    //*[@id="lockedTearms"]
    ${Client_InviteVendor_NewVendor_RiskLevel_Low_Done_btn}    Set Variable    //button[text()="Done"]
    ${Client_InviteVendor_NewVendor_Comments_txta}    Set Variable    //*[@id="Comments"]
    ${Client_InviteVendor_NewVendor_SendMeCC_ckb}    Set Variable    //*[@id="isSendMeCC"]
    ${Client_InviteVendor_NewVendor_SendOtherCC_lst}    Set Variable    //*[@id="usersSendCC"]
    ${Client_InviteVendor_NewVendor_SendBCC_ckb}    Set Variable    //*[@id="isSendBCC"]
    ${Client_InviteVendor_NewVendor_SendOtherBCC_ckb}    Set Variable    //*[@id="usersSendBCC"]
    ${Client_InviteVendor_NewVendor_SendInvitation_btn}    Set Variable    //button[text()="Send Invitation "]
    ${Client_InviteVendor_NewVendor_CancelInvitation_btn}    Set Variable    //a[text()="Cancel"]
    Set Test Variable    ${Client_InviteVendor_NewVendor_mnu}
    Set Test Variable    ${Client_InviteVendor_ExpiredVendor_mnu}
    Set Test Variable    ${Client_InviteVendor_OutsideNetwork_mnu}
    Set Test Variable    ${Client_InviteVendor_NewVendor_Location_lst}
    Set Test Variable    ${Client_InviteVendor_NewVendor_VendorCompanyName_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_FirstName_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_LastName_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_EmailAddress_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_Address1_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_Address2_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_Address3_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_City_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_PostalCode_txt}
    Set Test Variable    ${Client_InviteVendor_NewVendor_Country_lst}
    Set Test Variable    ${Client_InviteVendor_NewVendor_State_lst}
    Set Test Variable    ${Client_InviteVendor_NewVendor_Language_lst}
    Set Test Variable    ${Client_InviteVendor_NewVendor_RiskLevel_lst}
    Set Test Variable    ${Client_InviteVendor_NewVendor_RiskLevel_Low_Agree_ckb}
    Set Test Variable    ${Client_InviteVendor_NewVendor_RiskLevel_Low_Done_btn}
    Set Test Variable    ${Client_InviteVendor_NewVendor_Comments_txta}
    Set Test Variable    ${Client_InviteVendor_NewVendor_SendMeCC_ckb}
    Set Test Variable    ${Client_InviteVendor_NewVendor_SendOtherCC_lst}
    Set Test Variable    ${Client_InviteVendor_NewVendor_SendBCC_ckb}
    Set Test Variable    ${Client_InviteVendor_NewVendor_SendOtherBCC_ckb}
    Set Test Variable    ${Client_InviteVendor_NewVendor_SendInvitation_btn}
    Set Test Variable    ${Client_InviteVendor_NewVendor_CancelInvitation_btn}

Client_InviteVendor
    [Arguments]    ${VendorCompanyName}    ${FirstName}    ${LastName}    ${EmailAddress}    ${Address1}    ${Address2}    ${Address3}    ${City}    ${PostalCode}    ${Country}    ${State}    ${Language}    ${RiskLevel}    ${isRiskLevel_Low_Agree}    ${Comments}    ${IsSendMeCC}
    ...    ${isSendBCC}
    Click Link    ${Clt_Dash_InviteaVendor_lnk}
    Comment    Select From List By Label    ${Client_InviteVendor_NewVendor_Location_lst}
    Input Text    ${Client_InviteVendor_NewVendor_VendorCompanyName_txt}    ${VendorCompanyName}
    Input Text    ${Client_InviteVendor_NewVendor_FirstName_txt}    ${FirstName}
    Input Text    ${Client_InviteVendor_NewVendor_LastName_txt}    ${LastName}
    Input Text    ${Client_InviteVendor_NewVendor_EmailAddress_txt}    ${EmailAddress}
    Input Text    ${Client_InviteVendor_NewVendor_Address1_txt}    ${Address1}
    Input Text    ${Client_InviteVendor_NewVendor_Address2_txt}    ${Address2}
    Input Text    ${Client_InviteVendor_NewVendor_Address3_txt}    ${Address3}
    Input Text    ${Client_InviteVendor_NewVendor_City_txt}    ${City}
    Input Text    ${Client_InviteVendor_NewVendor_PostalCode_txt}    ${PostalCode}
    Select From List By Label    ${Client_InviteVendor_NewVendor_Country_lst}    ${Country}
    Wait Until Element Is Visible    //option[text()="${State}"]    5s
    Select From List By Label    ${Client_InviteVendor_NewVendor_State_lst}    ${State}
    Wait Until Element Is Visible    //option[text()="${Language}"]    5s
    Select From List By Label    ${Client_InviteVendor_NewVendor_Language_lst}    ${Language}
    Select From List By Label    ${Client_InviteVendor_NewVendor_RiskLevel_lst}    ${RiskLevel}
    Run Keyword If    '${isRiskLevel_Low_Agree}'=='True' and '${RiskLevel}'=='Low Risk'    Select Checkbox    ${Client_InviteVendor_NewVendor_RiskLevel_Low_Agree_ckb}
    Run Keyword If    '${isRiskLevel_Low_Agree}'=='True' and '${RiskLevel}'=='Low Risk'    Click Button    ${Client_InviteVendor_NewVendor_RiskLevel_Low_Done_btn}
    Input Text    ${Client_InviteVendor_NewVendor_Comments_txta}    ${Comments}
    Run Keyword If    '${IsSendMeCC}'=='True'    Select Checkbox    ${Client_InviteVendor_NewVendor_SendMeCC_ckb}
    Run Keyword If    '${isSendBCC}'=='True'    Select Checkbox    ${Client_InviteVendor_NewVendor_SendBCC_ckb}
    Click Button    ${Client_InviteVendor_NewVendor_SendInvitation_btn}

Client_InitializeWebElement
    Client_Dashboard_PO
    Client_InviteVendor_PO

Client_Dashboard_PO
    ${Clt_Dash_Reports_lnk}    Set Variable    //a[text()="Reports "]
    ${Clt_Dash_Notifications_lnk}    Set Variable    //a[text()="Notifications "]
    ${Clt_Dash_Processing_lnk}    Set Variable    //a[text()="Processing"]
    ${Clt_Dash_StatusDescriptions_lnk}    Set Variable    //a[text()="Status Descriptions"]
    ${Clt_Dash_InviteaVendor_lnk}    Set Variable    //a[text()="Invite a Vendor"]
    ${Clt_Dash_VendorsInvited_lnk}    Set Variable    //a[text()="Vendors Invited"]
    ${Clt_Dash_ClientReview_lnk}    Set Variable    //a[text()="Client Review"]
    Set Test Variable    ${Clt_Dash_Reports_lnk}
    Set Test Variable    ${Clt_Dash_Notifications_lnk}
    Set Test Variable    ${Clt_Dash_Processing_lnk}
    Set Test Variable    ${Clt_Dash_StatusDescriptions_lnk}
    Set Test Variable    ${Clt_Dash_InviteaVendor_lnk}
    Set Test Variable    ${Clt_Dash_VendorsInvited_lnk}
    Set Test Variable    ${Clt_Dash_ClientReview_lnk}
