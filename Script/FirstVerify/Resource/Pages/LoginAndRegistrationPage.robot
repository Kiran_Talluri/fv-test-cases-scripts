*** Settings ***
Resource          ../../Resource/Properties.robot
Library           Selenium2Library
Resource          ../ReusableGroup.robot

*** Keywords ***
LoginToFirstVerify
    [Arguments]    ${Environment}    ${Role}    ${Username}=${NONE}    ${Password}=${NONE}
    ${UserName_str}    Set Variable If    "${Role}"=="Admin"    ${UserName_admin}    ${Username}
    ${Password_str}    Set Variable If    "${Role}"=="Admin"    ${Password_admin}    ${Password}
    LaunchBrowser    ${Environment}
    InputText_rgp    ${UserName_txt}    ${UserName_str}
    InputText_rgp    ${Password_txt}    ${Password_str}
    ClickButton_rgp    ${Login_bth}

Login_PO
    ${UserName_txt}    Set Variable    //*[@id="UserName"]
    ${Password_txt}    Set Variable    //*[@id="Password"]
    ${Login_bth}    Set Variable    //input[@type="submit"]
    ${RememberMe_ckb}    Set Variable    //*[@id="RememberMe"]
    ${ForgotPassword_btn}    Set Variable    //button[text()="Forgot Password?"]
    ${NeedHelp_btn}    Set Variable    //button[text()="Need Login Help?"]
    Set Test Variable    ${UserName_txt}
    Set Test Variable    ${Password_txt}
    Set Test Variable    ${Login_bth}
    Set Test Variable    ${RememberMe_ckb}
    Set Test Variable    ${ForgotPassword_btn}
    Set Test Variable    ${NeedHelp_btn}

Login_InitializePageElements
    Login_PO
    Registration_PO

Registration_PO
    ${Registration_mnu}    Set Variable    //a[text()=" Registration"]
    ${CompanyRegistration_lnk}    Set Variable    //h4[text()="Company Registration for Prequalification"]/parent::a
    ${IndividualRegistration_lnk}    Set Variable    //h4[text()="Individual Registration for "]/parent::a
    ${CompanyReg_TypeInternational_rbtn}    Set Variable    //*[@id="NonUSA"]
    ${CompanyReg_TypeUS_rbtn}    Set Variable    //*[@id="USA"]
    ${CompanyReg_Name_txt}    Set Variable    //*[@id="Name"]
    ${CompanyReg_TaxID_txt}    Set Variable    //*[@id="TaxID"]
    ${CompanyReg_OfficerName_txt}    Set Variable    //*[@id="PrincipalCompanyOfficerName"]
    ${CompanyReg_Address1_txt}    Set Variable    //*[@id="Address1"]
    ${CompanyReg_Address2_txt}    Set Variable    //*[@id="Address2"]
    ${CompanyReg_Address3_txt}    Set Variable    //*[@id="Address3"]
    ${CompanyReg_City_txt}    Set Variable    //*[@id="City"]
    ${CompanyReg_PostalCode_txt}    Set Variable    //*[@id="Zip"]
    ${CompanyReg_Country_lst}    Set Variable    //*[@id="Country"]
    ${CompanyReg_State_lst}    Set Variable    //*[@id="State"]
    ${CompanyReg_Language_lst}    Set Variable    //*[@id="Language"]
    ${CompanyReg_Region_lst}    Set Variable    //*[@id="GeographicArea"]
    ${CompanyReg_PhoneNumber_txt}    Set Variable    //*[@id="PhoneNumber"]
    ${CompanyReg_ExtNumber_txt}    Set Variable    //*[@id="ExtNumber"]
    ${CompanyReg_FaxNumber_txt}    Set Variable    //*[@id="FaxNumber"]
    ${CompanyReg_WebsiteURL_txt}    Set Variable    //*[@id="WebsiteURL"]
    ${CompanyReg_FirstName_txt}    Set Variable    //*[@id="FirstName"]
    ${CompanyReg_LastName_txt}    Set Variable    //*[@id="LastName"]
    ${CompanyReg_ContactTitle_txt}    Set Variable    //*[@id="ContactTitle"]
    ${CompanyReg_MobileNumber_txt}    Set Variable    //*[@id="MobileNumber"]
    ${CompanyReg_Email_txt}    Set Variable    //*[@id="Email"]
    ${CompanyReg_Password_txt}    Set Variable    //*[@id="Password"]
    ${CompanyReg_ConfirmPassword_txt}    Set Variable    //*[@id="ConfirmPassword"]
    ${CompanyReg_Agree_ckb}    Set Variable    //*[@id="VndReg"]
    ${CompanyReg_CreateAccount_btn}    Set Variable    //button[text()="Create Account"]
    ${CompanyReg_Cancel_lnk}    Set Variable    //a[text()="Cancel"]
    Set Test Variable    ${Registration_mnu}
    Set Test Variable    ${CompanyRegistration_lnk}
    Set Test Variable    ${IndividualRegistration_lnk}
    Set Test Variable    ${CompanyReg_TypeInternational_rbtn}
    Set Test Variable    ${CompanyReg_TypeUS_rbtn}
    Set Test Variable    ${CompanyReg_Name_txt}
    Set Test Variable    ${CompanyReg_TaxID_txt}
    Set Test Variable    ${CompanyReg_OfficerName_txt}
    Set Test Variable    ${CompanyReg_Address1_txt}
    Set Test Variable    ${CompanyReg_Address2_txt}
    Set Test Variable    ${CompanyReg_Address3_txt}
    Set Test Variable    ${CompanyReg_City_txt}
    Set Test Variable    ${CompanyReg_PostalCode_txt}
    Set Test Variable    ${CompanyReg_Country_lst}
    Set Test Variable    ${CompanyReg_State_lst}
    Set Test Variable    ${CompanyReg_Language_lst}
    Set Test Variable    ${CompanyReg_Region_lst}
    Set Test Variable    ${CompanyReg_PhoneNumber_txt}
    Set Test Variable    ${CompanyReg_ExtNumber_txt}
    Set Test Variable    ${CompanyReg_FaxNumber_txt}
    Set Test Variable    ${CompanyReg_WebsiteURL_txt}
    Set Test Variable    ${CompanyReg_FirstName_txt}
    Set Test Variable    ${CompanyReg_LastName_txt}
    Set Test Variable    ${CompanyReg_ContactTitle_txt}
    Set Test Variable    ${CompanyReg_MobileNumber_txt}
    Set Test Variable    ${CompanyReg_Email_txt}
    Set Test Variable    ${CompanyReg_Password_txt}
    Set Test Variable    ${CompanyReg_ConfirmPassword_txt}
    Set Test Variable    ${CompanyReg_Agree_ckb}
    Set Test Variable    ${CompanyReg_CreateAccount_btn}
    Set Test Variable    ${CompanyReg_Cancel_lnk}

CompanyRegistration
    [Arguments]    ${isUS}    ${Name}    ${TaxID}    ${OfficerName}    ${Address1}    ${Address2}    ${Address3}    ${City}    ${PostalCode}    ${Country}    ${State}    ${Language}    ${Region}    ${PhoneNumber}    ${ExtNumber}    ${FaxNumber}
    ...    ${WebsiteURL}    ${FirstName}    ${LastName}    ${ContactTitle}    ${MobileNumber}    ${Email}    ${Password}    ${ConfirmPassword}
    ClickElement_rgp    ${Registration_mnu}
    Click Link    ${CompanyRegistration_lnk}
    Run Keyword If    '${isUS}'=='True'    ClickElement_rgp    ${CompanyReg_TypeInternational_rbtn}
    ...    ELSE IF    '${isUS}'=='False'    ClickElement_rgp    ${CompanyReg_TypeUS_rbtn}
    InputText_rgp    ${CompanyReg_Name_txt}    ${Name}
    InputText_rgp    ${CompanyReg_TaxID_txt}    ${TaxID}
    InputText_rgp    ${CompanyReg_OfficerName_txt}    ${OfficerName}
    InputText_rgp    ${CompanyReg_Address1_txt}    ${Address1}
    InputText_rgp    ${CompanyReg_Address2_txt}    ${Address2}
    InputText_rgp    ${CompanyReg_Address3_txt}    ${Address3}
    InputText_rgp    ${CompanyReg_City_txt}    ${City}
    InputText_rgp    ${CompanyReg_PostalCode_txt}    ${PostalCode}
    Select From List By Label    ${CompanyReg_Country_lst}    ${Country}
    Wait Until Element Is Visible    //option[text()="${State}"]    5s
    Select From List By Label    ${CompanyReg_State_lst}    ${State}
    Wait Until Element Is Visible    //option[text()="${Language}"]    5s
    Select From List By Label    ${CompanyReg_Language_lst}    ${Language}
    Select From List By Label    ${CompanyReg_Region_lst}    ${Region}
    InputText_rgp    ${CompanyReg_PhoneNumber_txt}    ${PhoneNumber}
    InputText_rgp    ${CompanyReg_ExtNumber_txt}    ${ExtNumber}
    InputText_rgp    ${CompanyReg_FaxNumber_txt}    ${FaxNumber}
    InputText_rgp    ${CompanyReg_WebsiteURL_txt}    ${WebsiteURL}
    InputText_rgp    ${CompanyReg_FirstName_txt}    ${FirstName}
    InputText_rgp    ${CompanyReg_LastName_txt}    ${LastName}
    InputText_rgp    ${CompanyReg_ContactTitle_txt}    ${ContactTitle}
    InputText_rgp    ${CompanyReg_MobileNumber_txt}    ${MobileNumber}
    InputText_rgp    ${CompanyReg_Email_txt}    ${Email}
    InputText_rgp    ${CompanyReg_Password_txt}    ${Password}
    InputText_rgp    ${CompanyReg_ConfirmPassword_txt}    ${ConfirmPassword}
    Select Checkbox    ${CompanyReg_Agree_ckb}
    ClickButton_rgp    ${CompanyReg_CreateAccount_btn}

LaunchBrowser
    [Arguments]    ${Environment}
    Run Keyword And Return Status    Close All Browsers
    ${URL_str}    Set Variable If    "${Environment}"=="Test"    ${Url_Test}
    Open Browser    url=${URL_str}    browser=chrome
    Maximize Browser Window
