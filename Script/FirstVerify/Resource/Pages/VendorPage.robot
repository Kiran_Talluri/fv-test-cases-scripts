*** Settings ***
Library           Selenium2Library
Library           String
Resource          ../ReusableGroup.robot

*** Keywords ***
Vendor_Dashboard_PO
    ${Vendor_StartNewRenew_btn}    Set Variable    //button[text()="Start New/Renew"]
    ${Vendor_Continue_btn}    Set Variable    //button[text()="Continue"]
    ${Vendor_Cancel_btn}    Set Variable    //button[text()="Cancel"]
    ${Vendor_StartPrequalification_btn}    Set Variable    //button[text()="Start Prequalification"]
    ${Vendor_GeneralInfo_FirstName_txt}    Set Variable    //b[contains(text(),'Full Name:')]/following::input[1]
    ${Vendor_GeneralInfo_Title_txt}    Set Variable    //b[contains(text(),'Title')]/following::input[1]
    ${Vendor_GeneralInfo_Email_txt}    Set Variable    //b[contains(text(),'Email Address')]/following::input[1]
    ${Vendor_GeneralInfo_TelephoneNo_txt}    Set Variable    //b[contains(text(),'Telephone Number')]/following::input[1]
    ${Vendor_Save_btn}    Set Variable    //button[text()="Save"]
    ${Vendor_SaveAndContinue_btn}    Set Variable    //button[contains(text(),"Save & Continue")]
    ${Vendor_Agree_ckb}    Set Variable    //*[@id="finalizeChk"]
    ${Vendor_ConfirmPerson_txt}    Set Variable    //*[@id="confirmPerson"]
    ${Vendor_Expand_img}    Set Variable    (//img[@src="../images/expandlist.png"])[1]
    ${Vendor_Finalize AndSubmit_btn}    Set Variable    //button[text()="Finalize & Submit"]
    Set Test Variable    ${Vendor_StartNewRenew_btn}
    Set Test Variable    ${Vendor_Continue_btn}
    Set Test Variable    ${Vendor_Cancel_btn}
    Set Test Variable    ${Vendor_StartPrequalification_btn}
    Set Test Variable    ${Vendor_GeneralInfo_FirstName_txt}
    Set Test Variable    ${Vendor_GeneralInfo_Title_txt}
    Set Test Variable    ${Vendor_GeneralInfo_Email_txt}
    Set Test Variable    ${Vendor_GeneralInfo_TelephoneNo_txt}
    Set Test Variable    ${Vendor_Save_btn}
    Set Test Variable    ${Vendor_SaveAndContinue_btn}
    Set Test Variable    ${Vendor_Agree_ckb}
    Set Test Variable    ${Vendor_ConfirmPerson_txt}
    Set Test Variable    ${Vendor_Expand_img}
    Set Test Variable    ${Vendor_Finalize AndSubmit_btn}

PreQualify
    Comment    ${Client}    Set Variable    Absolute Energy, L.L.C.
    Comment    ${Risk}    Set Variable    Absolute Energy - Safety
    Comment    ClickElement_rgp    ${Vendor_StartNewRenew_btn}
    Comment    ClickElement_rgp    //input[@value="${Client}"]
    Comment    ClickElement_rgp    ${Vendor_Continue_btn}
    Comment    ClickElement_rgp    //h3[text()="${Risk}"]//ancestor::tr/td[1]/input
    Comment    ClickElement_rgp    ${Vendor_StartPrequalification_btn}
    Wait Until Element Is Visible    //b[contains(text(),"Complete Missing Items")]/parent::a    timeout=5s
    ClickElement_rgp    //b[contains(text(),"Complete Missing Items")]/parent::a
    ClickElement_rgp    (//a[text()=" Introduction "])[2]
    Wait Until Element Is Enabled    ${Vendor_Continue_btn}    timeout=5s
    ClickElement_rgp    ${Vendor_Continue_btn}    #Continue Introduction
    Sleep    5s
    ClickElement_rgp    ${Vendor_Continue_btn}    #PreQualification Req
    Vendor_GeneralInfo
    Wait Until Element Is Enabled    ${Vendor_Continue_btn}    timeout=5s
    ClickElement_rgp    ${Vendor_Continue_btn}    #Continue Payment
    Comment    Vendor_Payment
    Vendor_Business
    Vendor_Insurance
    Vendor_SafetyStatistics
    Vendor_SafetyProgramsAndProcedures
    Vendor_SupportingDocuments
    Select Checkbox    //*[@id="finalizeChk"]
    InputText_rgp    //*[@id="confirmPerson"]    Kishore, Deepak
    ClickButton_rgp    //button[text()="Finalize & Submit"]

Vendor_SafetyStatistics_PO
    #Experience Modification Rating
    ${SafetyStatistics_InterStateRating_txt}    Set Variable    //b[contains(text(),"If you do not have an interstate rating, identify type:")]/following::input[1]
    ${SafetyStatistics_StateOfOrgin_lst}    Set Variable    //b[contains(text(),"State of origin:")]/following::select[1]
    ${SafetyStatistics_EMRDate_txt}    Set Variable    //b[contains(text(),"EMR effective date:")]/following::input[1]
    ${SafetyStatistics_RecentPolicyYear_ckb}    Set Variable    //b[contains(text(),"Most Recent Policy Year:")]/following::input[1]
    ${SafetyStatistics_RecentPolicyYear_Year_txt}    Set Variable    //b[contains(text(),"Most Recent Policy Year:")]/following::input[2]
    ${SafetyStatistics_RecentPolicyYear_EMR_txt}    Set Variable    //b[contains(text(),"Most Recent Policy Year:")]/following::input[3]
    ${SafetyStatistics_OneYearPrevious_ckb}    Set Variable    //b[contains(text(),"One Year Previous:")]/following::input[1]
    ${SafetyStatistics_OneYearPrevious_Year_txt}    Set Variable    //b[contains(text(),"One Year Previous:")]/following::input[2]
    ${SafetyStatistics_OneYearPrevious_EMR_txt}    Set Variable    //b[contains(text(),"One Year Previous:")]/following::input[3]
    ${SafetyStatistics_TwoYearPrevious_ckb}    Set Variable    //b[contains(text(),"Two Years Previous:")]/following::input[1]
    ${SafetyStatistics_TwoYearPrevious_Year_txt}    Set Variable    //b[contains(text(),"Two Years Previous:")]/following::input[2]
    ${SafetyStatistics_TwoYearPrevious_EMR_txt}    Set Variable    //b[contains(text(),"Two Years Previous:")]/following::input[3]
    #Workers Compensation
    ${SafetyStatistics_WorkersComp_rbtn}    Set Variable    //b[contains(text(),"Is your firm self-insured for Workers Compensation claims?")]/following::input
    #OSHA Recordkeeping
    ${SafetyStatistics_OSHARecordKeeping_lst}    Set Variable    //b[contains(text(),"Is your company exempt from OSHA recordkeeping?")]/following::select[1]
    #Safety Performance History
    ${SafetyStatistics_Year_txt}    Set Variable    //b[contains(text(),"Note: Data provided MUST be entered directly from your NCCI Worksheet and signed OSHA 300A Forms.")]/following::input
    ${SafetyStatistics_ColumnGOSHA_txt}    Set Variable    //b[contains(text(),"Number of fatalities: (total from Column G on your OSHA Form)")]/following::input
    ${SafetyStatistics_ColumnH_txt}    Set Variable    //b[contains(text(),"Number of lost work day cases: (total from Column H on your OSHA Form)")]/following::input
    ${SafetyStatistics_ColumnI_txt}    Set Variable    //b[contains(text(),"Number of job transfer or restricted work day cases: (total from Column I on your OSHA Form)")]/following::input
    ${SafetyStatistics_ColumnJ_txt}    Set Variable    //b[contains(text(),"Number of other recordable cases: (total from Column J on your OSHA Form)")]/following::input
    ${SafetyStatistics_ColumnK_txt}    Set Variable    //b[contains(text(),"Number of days away from work: (total from Column K on your OSHA Form)")]/following::input
    ${SafetyStatistics_TotalHrs_txt}    Set Variable    //b[contains(text(),"Total hours worked by all employees last year: (from your OSHA Form)")]/following::input
    ${SafetyStatistics_ColumnG_txt}    Set Variable    //b[contains(text(),"Recordable Incident Frequency Rate: # recordable cases (total from columns G, H, I, J) x 200,000 / Total employee hours worked last year")]/following::input
    ${SafetyStatistics_3YearAvg_txt}    Set Variable    //b[contains(text(),"Recordable Incident Frequency Rate (3 year average):")]/following::input[1]
    ${SafetyStatistics_LostWorkDay_txt}    Set Variable    //b[contains(text(),"Lost Work Day Case Rate: # lost work day cases (total from column H) x 200,000 / Total employee hours worked last year")]/following::input
    ${SafetyStatistics_DaysAway_txt}    Set Variable    //b[contains(text(),"Days Away, Restrictions or Transfers Rate (DART): # of DART incidents (total from columns H and I) x 200,000 / Total employee hours worked last year")]/following::input[1]
    #Regulatory
    ${SafetyStatistics_Last3Yrs_rbtn}    Set Variable    //b[contains(text(),"Have you been inspected by OSHA in the last three years?")]/following::input
    ${SafetyStatistics_Past3Yrs_rbtn}    Set Variable    //b[contains(text(),"Has your company been cited by OSHA or the EPA in the past three years?")]/following::input
    ${SafetyStatistics_AgreeDoc_ckb}    Set Variable    //b[contains(text(),"I understand that uploading this document is required.")]/following::input[1]
    Set Test Variable    ${SafetyStatistics_InterStateRating_txt}
    Set Test Variable    ${SafetyStatistics_StateOfOrgin_lst}
    Set Test Variable    ${SafetyStatistics_EMRDate_txt}
    Set Test Variable    ${SafetyStatistics_RecentPolicyYear_ckb}
    Set Test Variable    ${SafetyStatistics_RecentPolicyYear_Year_txt}
    Set Test Variable    ${SafetyStatistics_RecentPolicyYear_EMR_txt}
    Set Test Variable    ${SafetyStatistics_OneYearPrevious_ckb}
    Set Test Variable    ${SafetyStatistics_OneYearPrevious_Year_txt}
    Set Test Variable    ${SafetyStatistics_OneYearPrevious_EMR_txt}
    Set Test Variable    ${SafetyStatistics_TwoYearPrevious_ckb}
    Set Test Variable    ${SafetyStatistics_TwoYearPrevious_Year_txt}
    Set Test Variable    ${SafetyStatistics_TwoYearPrevious_EMR_txt}
    Set Test Variable    ${SafetyStatistics_WorkersComp_rbtn}
    Set Test Variable    ${SafetyStatistics_OSHARecordKeeping_lst}
    Set Test Variable    ${SafetyStatistics_Year_txt}
    Set Test Variable    ${SafetyStatistics_ColumnGOSHA_txt}
    Set Test Variable    ${SafetyStatistics_ColumnH_txt}
    Set Test Variable    ${SafetyStatistics_ColumnI_txt}
    Set Test Variable    ${SafetyStatistics_ColumnJ_txt}
    Set Test Variable    ${SafetyStatistics_ColumnK_txt}
    Set Test Variable    ${SafetyStatistics_TotalHrs_txt}
    Set Test Variable    ${SafetyStatistics_ColumnG_txt}
    Set Test Variable    ${SafetyStatistics_3YearAvg_txt}
    Set Test Variable    ${SafetyStatistics_LostWorkDay_txt}
    Set Test Variable    ${SafetyStatistics_DaysAway_txt}
    Set Test Variable    ${SafetyStatistics_Last3Yrs_rbtn}
    Set Test Variable    ${SafetyStatistics_Past3Yrs_rbtn}
    Set Test Variable    ${SafetyStatistics_AgreeDoc_ckb}

Vendor_SafetyProgramsAndProcedures_PO
    ${SafetyPgm_SafetyAndHealth_rbtn}    Set Variable    //b[contains(text(),"Do you have a written safety and health program/work rules available for review that addresses government requirements relative to your scope of work?")]/following::input
    ${SafetyPgm_AdministrativeProcedures_rbtn}    Set Variable    //b[contains(text(),"Does your written safety program address administrative procedures?")]/following::input
    ${SafetyPgm_SpecificSafetyPlan_rbtn}    Set Variable    //b[contains(text(),"Do you have a written site-specific safety plan?")]/following::input
    #Policy
    ${SafetyPgm_PolicyOfficerCompany_rbtn}    Set Variable    //b[contains(text(),"Do you have a safety policy statement from an officer of the company?")]/following::input
    ${SafetyPgm_DisciplinaryEnforcementPgm_rbtn}    Set Variable    //b[contains(text(),"Do you have a disciplinary process for enforcement of your safety program?")]/following::input
    ${SafetyPgm_CopSafetyGoals_rbtn}    Set Variable    //b[contains(text(),"Does management set corporate safety goals?")]/following::input
    ${SafetyPgm_MgtReviewAccRpt_rbtn}    Set Variable    //b[contains(text(),"Does executive management review accident reports?")]/following::input
    ${SafetyPgm_MgtReviewInspectionRpt_rbtn}    Set Variable    //b[contains(text(),"Does executive management review inspection reports?")]/following::input
    ${SafetyPgm_MgtReviewSafetyStat_rbtn}    Set Variable    //b[contains(text(),"Does executive management review safety statistics?")]/following::input
    ${SafetyPgm_LightDutyWork_rbtn}    Set Variable    //b[contains(text(),"Do you have a light-duty, return-to-work policy?")]/following::input
    ${SafetyPgm_SupPerfEvaluation_rbtn}    Set Variable    //b[contains(text(),"Is safety part of your supervisors' performance evaluations?")]/following::input
    ${SafetyPgm_MgtSafetyDutiesResponse_rbtn}    Set Variable    //b[contains(text(),"Does each level of management have assigned safety duties and responsibilities?")]/following::input
    #Substance Abuse Policy
    ${SafetyPgm_SubstanceAbusePolicy_rbtn}    Set Variable    //b[contains(text(),"Do you have a written substance abuse policy? ")]/following::input
    #Respiratory Protection
    ${SafetyPgm_Trained_rbtn}    Set Variable    //b[contains(text(),"Trained")]/following::input
    ${SafetyPgm_FitTested_rbtn}    Set Variable    //b[contains(text(),"Fit Tested")]/following::input
    ${SafetyPgm_MedicalApproved_rbtn}    Set Variable    //b[contains(text(),"Medically approved")]/following::input
    ${SafetyPgm_SafetyDirector_rbtn}    Set Variable    //b[contains(text(),"Does your company have a full-time safety director?")]/following::input
    #Medical
    ${SafetyPgm_PreEmp_rbtn}    Set Variable    ((//b[contains(text(),"Pre-employment")])[3]/following::input)[1]
    ${SafetyPgm_PrePlacement_rbtn}    Set Variable    //b[contains(text(),"Pre-Placement Job Capability")]/following::input
    ${SafetyPgm_Pulomonary_rbtn}    Set Variable    //b[contains(text(),"Pulmonary")]/following::input
    ${SafetyPgm_Respiratory_rbtn}    Set Variable    (//b[contains(text(),"Respiratory")])[2]/following::input[1]
    ${SafetyPgm_FirstAidMedicalService_txt}    Set Variable    //b[contains(text(),"Describe how you will provide first aid and other medical services for your employees while on site. Specify who will provide this service.")]/following::textarea
    ${SafetyPgm_TrainedFirstAid_rbtn}    Set Variable    //b[contains(text(),"Do you have personnel trained to perform first aid and CPR?")]/following::input
    ${SafetyPgm_AccidentInvestigation_rbtn}    Set Variable    //b[contains(text(),"Do you have an Accident Investigation Procedure?")]/following::input
    ${SafetyPgm_SafetyInspection_rbtn}    Set Variable    //b[contains(text(),"Do you conduct workplace safety inspections?")]/following::input
    #PPE, Equipment Inspections, Audits
    ${SafetyPgm_PPEEmployee_rbtn}    Set Variable    //b[contains(text(),"Is applicable Personal Protective Equipment (PPE) provided for employees?")]/following::input
    ${SafetyPgm_PPEPolicy_rbtn}    Set Variable    //b[contains(text(),"Do you have a PPE policy?")]/following::input
    ${SafetyPgm_PPEMaintain_rbtn}    Set Variable    //b[contains(text(),"Do you have a program to insure that PPE is inspected and maintained?")]/following::input
    ${SafetyPgm_SafetyAndHealthDef_rbtn}    Set Variable    //b[contains(text(),"Do you have a corrective action process for addressing individual safety & health deficiencies?")]/following::input
    ${SafetyPgm_AcquisitionMaterialEquip_rbtn}    Set Variable    //b[contains(text(),"Do you have a system for establishing applicable health, safety, and environmental specifications for acquisition of materials and equipment?")]/following::input
    ${SafetyPgm_RegulatoryReq_rbtn}    Set Variable    //b[contains(text(),"Do you conduct inspections on operating equipment (cranes, forklifts, etc.) in compliance with regulatory requirements?")]/following::input
    ${SafetyPgm_ComplianceRegulatory_rbtn}    Set Variable    //b[contains(text(),"Do you maintain operating equipment in compliance with regulatory requirements?")]/following::input
    ${SafetyPgm_CertRecordOperatingEquip_rbtn}    Set Variable    //b[contains(text(),"Do you maintain the applicable inspection and maintenance certification records for operating equipment?")]/following::input
    ${SafetyPgm_HealthPgmAudit_rbtn}    Set Variable    //b[contains(text(),"Do you conduct safety and health program audits?")]/following::input[2]
    ${SafetyPgm_AuditDeficiencies_rbtn}    Set Variable    //b[contains(text(),"Are corrections of inspection and audit deficiencies documented?")]/following::input
    #Meeting
    ${SafetyPgm_TalkMeeting_rbtn}    Set Variable    //b[contains(text(),"Do you hold site safety and health/Toolbox Talk meetings?")]/following::input
    ${SafetyPgm_Subcontractor_rbtn}    Set Variable    //b[contains(text(),"Do you use subcontractors?")]/following::input
    ${SafetyPgm_OrientationPgm_rbtn}    Set Variable    //b[contains(text(),"Do you have a safety training/orientation program?")]/following::input
    #Training Records
    ${SafetyPgm_EmpName_rbtn}    Set Variable    //b[contains(text(),"Employee name (Identification)")]/following::input
    ${SafetyPgm_DateOfTraining_rbtn}    Set Variable    //b[contains(text(),"Date of training")]/following::input
    ${SafetyPgm_TrainerName_rbtn}    Set Variable    //b[contains(text(),"Name of the trainer")]/following::input
    ${SafetyPgm_VerifyUncerstanding_rbtn}    Set Variable    //b[contains(text(),"Method used to verify understanding")]/following::input
    ${SafetyPgm_WrittenTest_rbtn}    Set Variable    //b[contains(text(),"Written Test")]/following::input
    ${SafetyPgm_OralTest_rbtn}    Set Variable    //b[contains(text(),"Oral Test")]/following::input
    ${SafetyPgm_PerfTest_rbtn}    Set Variable    //b[contains(text(),"Performance Test")]/following::input
    ${SafetyPgm_JobMonitoring_rbtn}    Set Variable    //b[contains(text(),"Job Monitoring")]/following::input
    ${SafetyPgm_TrainingOther_txt}    Set Variable    //b[contains(text(),"Other")]/following::textarea
    #Safety Orientation Program for Newly Hired or Promoted Foremen/Supervisors
    ${SafetyPgm_NewlyHiredSupervisor_rbtn}    Set Variable    //b[contains(text(),"Do you have a safety program for newly hired or promoted foremen/supervisors?")]/following::input
    ${SafetyPgm_ConstructionSafetyCourse_rbtn}    Set Variable    //b[contains(text(),"Do you use the OSHA 10 hour/30 hour Construction Safety courses?")]/following::input
    ${SafetyPgm_JobSkills_rbtn}    Set Variable    //b[contains(text(),"Have employees been trained in appropriate job skills?")]/following::input
    ${SafetyPgm_SkillsCertified_rbtn}    Set Variable    //b[contains(text(),"Are employees' job skills certified where required by regulatory or industry consensus standards?")]/following::input
    ${SafetyPgm_UnderstandEnglish_rbtn}    Set Variable    //b[contains(text(),"Do your employees read, write and understand English such that they can perform their job tasks safely without an interpreter?")]/following::input
    Set Test Variable    ${SafetyPgm_SafetyAndHealth_rbtn}
    Set Test Variable    ${SafetyPgm_AdministrativeProcedures_rbtn}
    Set Test Variable    ${SafetyPgm_SpecificSafetyPlan_rbtn}
    Set Test Variable    ${SafetyPgm_PolicyOfficerCompany_rbtn}
    Set Test Variable    ${SafetyPgm_DisciplinaryEnforcementPgm_rbtn}
    Set Test Variable    ${SafetyPgm_CopSafetyGoals_rbtn}
    Set Test Variable    ${SafetyPgm_MgtReviewAccRpt_rbtn}
    Set Test Variable    ${SafetyPgm_MgtReviewInspectionRpt_rbtn}
    Set Test Variable    ${SafetyPgm_MgtReviewSafetyStat_rbtn}
    Set Test Variable    ${SafetyPgm_LightDutyWork_rbtn}
    Set Test Variable    ${SafetyPgm_SupPerfEvaluation_rbtn}
    Set Test Variable    ${SafetyPgm_MgtSafetyDutiesResponse_rbtn}
    Set Test Variable    ${SafetyPgm_SubstanceAbusePolicy_rbtn}
    Set Test Variable    ${SafetyPgm_Trained_rbtn}
    Set Test Variable    ${SafetyPgm_FitTested_rbtn}
    Set Test Variable    ${SafetyPgm_MedicalApproved_rbtn}
    Set Test Variable    ${SafetyPgm_SafetyDirector_rbtn}
    Set Test Variable    ${SafetyPgm_PreEmp_rbtn}
    Set Test Variable    ${SafetyPgm_PrePlacement_rbtn}
    Set Test Variable    ${SafetyPgm_Pulomonary_rbtn}
    Set Test Variable    ${SafetyPgm_Respiratory_rbtn}
    Set Test Variable    ${SafetyPgm_FirstAidMedicalService_txt}
    Set Test Variable    ${SafetyPgm_TrainedFirstAid_rbtn}
    Set Test Variable    ${SafetyPgm_AccidentInvestigation_rbtn}
    Set Test Variable    ${SafetyPgm_SafetyInspection_rbtn}
    Set Test Variable    ${SafetyPgm_PPEEmployee_rbtn}
    Set Test Variable    ${SafetyPgm_PPEPolicy_rbtn}
    Set Test Variable    ${SafetyPgm_PPEMaintain_rbtn}
    Set Test Variable    ${SafetyPgm_SafetyAndHealthDef_rbtn}
    Set Test Variable    ${SafetyPgm_AcquisitionMaterialEquip_rbtn}
    Set Test Variable    ${SafetyPgm_RegulatoryReq_rbtn}
    Set Test Variable    ${SafetyPgm_ComplianceRegulatory_rbtn}
    Set Test Variable    ${SafetyPgm_CertRecordOperatingEquip_rbtn}
    Set Test Variable    ${SafetyPgm_HealthPgmAudit_rbtn}
    Set Test Variable    ${SafetyPgm_AuditDeficiencies_rbtn}
    Set Test Variable    ${SafetyPgm_TalkMeeting_rbtn}
    Set Test Variable    ${SafetyPgm_Subcontractor_rbtn}
    Set Test Variable    ${SafetyPgm_OrientationPgm_rbtn}
    Set Test Variable    ${SafetyPgm_EmpName_rbtn}
    Set Test Variable    ${SafetyPgm_DateOfTraining_rbtn}
    Set Test Variable    ${SafetyPgm_TrainerName_rbtn}
    Set Test Variable    ${SafetyPgm_VerifyUncerstanding_rbtn}
    Set Test Variable    ${SafetyPgm_WrittenTest_rbtn}
    Set Test Variable    ${SafetyPgm_OralTest_rbtn}
    Set Test Variable    ${SafetyPgm_PerfTest_rbtn}
    Set Test Variable    ${SafetyPgm_JobMonitoring_rbtn}
    Set Test Variable    ${SafetyPgm_TrainingOther_txt}
    Set Test Variable    ${SafetyPgm_NewlyHiredSupervisor_rbtn}
    Set Test Variable    ${SafetyPgm_ConstructionSafetyCourse_rbtn}
    Set Test Variable    ${SafetyPgm_JobSkills_rbtn}
    Set Test Variable    ${SafetyPgm_SkillsCertified_rbtn}
    Set Test Variable    ${SafetyPgm_UnderstandEnglish_rbtn}

Vendor_Insurance_PO
    ${Vendor_Insurance_AgencyName_txt}    Set Variable    //b[contains(text(),'Agency Name:')]/following::input[1]
    ${Vendor_Insurance_AgentsName_txt}    Set Variable    //b[contains(text(),"Agent's Name:")]/following::input[1]
    ${Vendor_Insurance_Title_txt}    Set Variable    //b[contains(text(),'Title:')]/following::input[1]
    ${Vendor_Insurance_Email_txt}    Set Variable    //b[contains(text(),'Email Address')]/following::input[1]
    ${Vendor_Insurance_TelephoneNo_txt}    Set Variable    //b[contains(text(),'Telephone Number')]/following::input[1]
    ${Vendor_Insurance_FaxNo_txt}    Set Variable    //b[contains(text(),'Fax Number')]/following::input[1]
    ${Vendor_Insurance_InsCarrier_txt}    Set Variable    //b[contains(text(),"Insurance Carrier(s):")]/following::textarea
    ${Vendor_Insurance_OccurrenceLimitAmount_txt}    Set Variable    //b[contains(text(),"Each Occurrence Limit Amount:")]/following::input[1]
    ${Vendor_Insurance_AggLimit_txt}    Set Variable    //b[contains(text(),"General Aggregate Limit Amount:")]/following::input[1]
    ${Vendor_Insurance_AccidentLimit_txt}    Set Variable    //b[contains(text(),"Each Accident Limit Amount:")]/following::input[1]
    ${Vendor_Insurance_ExcessOccurrenceLimit_txt}    Set Variable    //b[contains(text(),"Each Occurrence Limit:")]/following::input[1]
    ${Vendor_Insurance_WorkersAccidentLimit_txt}    Set Variable    //b[contains(text(),"Each Accident Limit:")]/following::input[1]
    ${Vendor_Insurance_DiseaseEachEmp_txt}    Set Variable    //b[contains(text(),"Disease Each Employee:")]/following::input[1]
    ${Vendor_Insurance_DiseasePolicyLimit_txt}    Set Variable    //b[contains(text(),"Disease Policy Limit:")]/following::input[1]
    Set Test Variable    ${Vendor_Insurance_AgencyName_txt}
    Set Test Variable    ${Vendor_Insurance_AgentsName_txt}
    Set Test Variable    ${Vendor_Insurance_Title_txt}
    Set Test Variable    ${Vendor_Insurance_Email_txt}
    Set Test Variable    ${Vendor_Insurance_TelephoneNo_txt}
    Set Test Variable    ${Vendor_Insurance_FaxNo_txt}
    Set Test Variable    ${Vendor_Insurance_InsCarrier_txt}
    Set Test Variable    ${Vendor_Insurance_OccurrenceLimitAmount_txt}
    Set Test Variable    ${Vendor_Insurance_AggLimit_txt}
    Set Test Variable    ${Vendor_Insurance_AccidentLimit_txt}
    Set Test Variable    ${Vendor_Insurance_ExcessOccurrenceLimit_txt}
    Set Test Variable    ${Vendor_Insurance_WorkersAccidentLimit_txt}
    Set Test Variable    ${Vendor_Insurance_DiseaseEachEmp_txt}
    Set Test Variable    ${Vendor_Insurance_DiseasePolicyLimit_txt}

Vendor_Payment_PO
    ${Vendor_Payment_PayNow_btn}    Set Variable    //button[text()="Pay Now"]
    ${Vendor_Payment_CardNo_txt}    Set Variable    //*[@id="cc_number"]
    ${Vendor_Payment_ExpMonth_txt}    Set Variable    //*[@id="expdate_month"]
    ${Vendor_Payment_ExpYear_txt}    Set Variable    //*[@id="expdate_year"]
    ${Vendor_Payment_CardPayNow_btn}    Set Variable    //*[@id="btn_pay_cc"]
    Set Test Variable    ${Vendor_Payment_PayNow_btn}
    Set Test Variable    ${Vendor_Payment_CardNo_txt}
    Set Test Variable    ${Vendor_Payment_ExpMonth_txt}
    Set Test Variable    ${Vendor_Payment_ExpYear_txt}
    Set Test Variable    ${Vendor_Payment_CardPayNow_btn}

Vendor_DocUpload_PO
    ${Vendor_DocUpload_AddNew_btn}    Set Variable    //button[contains(text(),'Add New')]
    ${Vendor_DocUpload_DocCategory_lst}    Set Variable    //*[@id="DocumentTypeId"]
    ${Vendor_DocUpload_Expires_txt}    Set Variable    //*[@id="Expires"]
    ${Vendor_DocUpload_Upload_file}    Set Variable    //*[@id="file"]
    Set Test Variable    ${Vendor_DocUpload_AddNew_btn}
    Set Test Variable    ${Vendor_DocUpload_DocCategory_lst}
    Set Test Variable    ${Vendor_DocUpload_Expires_txt}
    Set Test Variable    ${Vendor_DocUpload_Upload_file}

Vendor_InitializeWebElements
    Vendor_Dashboard_PO
    Vendor_Insurance_PO
    Vendor_Payment_PO
    Vendor_SafetyStatistics_PO
    Vendor_SafetyProgramsAndProcedures_PO
    Vendor_DocUpload_PO
    Vendor_Business_PO

Vendor_Business_PO
    ${Vendor_Business_DAndB_txt}    Set Variable    //b[contains(text(),"D-U-N-S (D & B) Number:")]/following::input[1]
    ${Vendor_Business_DateFounded_txt}    Set Variable    //b[contains(text(),"Date Founded:")]/following::input[1]
    ${Vendor_Business_StateOfFormation_lst}    Set Variable    //b[contains(text(),"State of Formation:")]/following::select[1]
    ${Vendor_Business_PresentName_txt}    Set Variable    //b[contains(text(),"Under Present Name Since:")]/following::input[1]
    ${Vendor_Business_PresentMgmt_txt}    Set Variable    //b[contains(text(),"Under Present Mgmt. Since:")]/following::input[1]
    ${Vendor_Business_NameOfPredecessor_txt}    Set Variable    //b[contains(text(),"Name of Predecessor:")]/following::input[1]
    ${Vendor_Business_ReasonForDept_txt}    Set Variable    //b[contains(text(),"Reason for departure/change:")]/following::input[1]
    ${Vendor_Business_NoOfEmp_txt}    Set Variable    //b[contains(text(),"Number of company employees:")]/following::input[1]
    ${Vendor_Business_Administration_txt}    Set Variable    //b[contains(text(),"Administration")]/following::input[1]
    ${Vendor_Business_FieldSupervision_txt}    Set Variable    //b[contains(text(),"Field Supervision:")]/following::input[1]
    ${Vendor_Business_FieldEmp_txt}    Set Variable    //b[contains(text(),"Normal Number of Field Employees:")]/following::input[1]
    ${Vendor_Business_EmpRatio_txt}    Set Variable    //b[contains(text(),"Field Supervision to Employee Ratio:")]/following::input[1]
    Set Test Variable    ${Vendor_Business_DAndB_txt}
    Set Test Variable    ${Vendor_Business_DateFounded_txt}
    Set Test Variable    ${Vendor_Business_StateOfFormation_lst}
    Set Test Variable    ${Vendor_Business_PresentName_txt}
    Set Test Variable    ${Vendor_Business_PresentMgmt_txt}
    Set Test Variable    ${Vendor_Business_NameOfPredecessor_txt}
    Set Test Variable    ${Vendor_Business_ReasonForDept_txt}
    Set Test Variable    ${Vendor_Business_NoOfEmp_txt}
    Set Test Variable    ${Vendor_Business_Administration_txt}
    Set Test Variable    ${Vendor_Business_FieldSupervision_txt}
    Set Test Variable    ${Vendor_Business_FieldEmp_txt}
    Set Test Variable    ${Vendor_Business_EmpRatio_txt}

Vendor_SafetyProgramsAndProcedures
    #Written Safety and Health Program
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_SafetyAndHealth_rbtn})[2]
    #Written Safety Program Administrative Procedures
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_AdministrativeProcedures_rbtn})[2]
    #Site-Specific Safety Plan
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_SpecificSafetyPlan_rbtn})[2]
    #Policy
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_PolicyOfficerCompany_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_DisciplinaryEnforcementPgm_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_CopSafetyGoals_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_MgtReviewAccRpt_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_MgtReviewInspectionRpt_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_MgtReviewSafetyStat_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_LightDutyWork_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_SupPerfEvaluation_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_MgtSafetyDutiesResponse_rbtn})[1]
    #Substance Abuse Policy
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_SubstanceAbusePolicy_rbtn})[2]
    #Respiratory Protection
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_Trained_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_FitTested_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_MedicalApproved_rbtn})[1]
    #Management
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_SafetyDirector_rbtn})[2]
    #Medical
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_PreEmp_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_PrePlacement_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_Pulomonary_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_Respiratory_rbtn})[1]
    InputText_rgp    (${SafetyPgm_FirstAidMedicalService_txt})[1]    test
    ClickElement_rgp    (${SafetyPgm_TrainedFirstAid_rbtn})[1]
    #Accident Investigation Procedure
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_AccidentInvestigation_rbtn})[2]
    #Safety Inspections
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_SafetyInspection_rbtn})[2]
    #PPE, Equipment Inspections, Audits
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_PPEEmployee_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_PPEPolicy_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_PPEMaintain_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_SafetyAndHealthDef_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_AcquisitionMaterialEquip_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_RegulatoryReq_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_ComplianceRegulatory_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_CertRecordOperatingEquip_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_HealthPgmAudit_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_AuditDeficiencies_rbtn})[1]
    #Meeting
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_TalkMeeting_rbtn})[2]
    #Subcontractors
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_Subcontractor_rbtn})[2]
    #Safety Training/Orientation
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_OrientationPgm_rbtn})[2]
    #Training Records
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_EmpName_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_DateOfTraining_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_TrainerName_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_VerifyUncerstanding_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_WrittenTest_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_OralTest_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_PerfTest_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_JobMonitoring_rbtn})[1]
    InputText_rgp    (${SafetyPgm_TrainingOther_txt})[1]    test
    #Safety Orientation Program for Newly Hired or Promoted Foremen/Supervisors
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_NewlyHiredSupervisor_rbtn})[2]
    #OSHA Construction Safety Courses
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_ConstructionSafetyCourse_rbtn})[2]
    #Craft Training
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_JobSkills_rbtn})[1]
    ClickElement_rgp    (${SafetyPgm_SkillsCertified_rbtn})[2]
    #Comprehension
    ExpandSection
    ClickElement_rgp    (${SafetyPgm_UnderstandEnglish_rbtn})[1]
    ClickButton_rgp    ${Vendor_SaveAndContinue_btn}

Vendor_SafetyStatistics
    #Safety Statistics
    ExpandSection
    InputText_rgp    ${SafetyStatistics_InterStateRating_txt}    Chennai
    Select From List By Label    ${SafetyStatistics_StateOfOrgin_lst}    Alabama
    sleep    2s
    ${id}    Get Element Attribute    ${SafetyStatistics_EMRDate_txt}    id
    Execute Javascript    document.getElementById("${id}").removeAttribute("disabled");
    InputText_rgp    ${SafetyStatistics_EMRDate_txt}    11/11/2020
    Select Checkbox    ${SafetyStatistics_RecentPolicyYear_ckb}
    InputText_rgp    ${SafetyStatistics_RecentPolicyYear_Year_txt}    2020
    InputText_rgp    ${SafetyStatistics_RecentPolicyYear_EMR_txt}    123
    Select Checkbox    ${SafetyStatistics_OneYearPrevious_ckb}
    InputText_rgp    ${SafetyStatistics_OneYearPrevious_Year_txt}    2020
    InputText_rgp    ${SafetyStatistics_OneYearPrevious_EMR_txt}    123
    Select Checkbox    ${SafetyStatistics_TwoYearPrevious_ckb}
    InputText_rgp    ${SafetyStatistics_TwoYearPrevious_Year_txt}    2020
    InputText_rgp    ${SafetyStatistics_TwoYearPrevious_EMR_txt}    123
    #Workers Compensation
    ExpandSection
    ClickElement_rgp    ${SafetyStatistics_WorkersComp_rbtn}
    #OSHA Recordkeeping
    ExpandSection
    Select From List By Label    ${SafetyStatistics_OSHARecordKeeping_lst}    No
    #Safety Performance History
    ExpandSection
    InputText_rgp    (${SafetyStatistics_Year_txt})[1]    2020
    InputText_rgp    (${SafetyStatistics_Year_txt})[2]    2021
    InputText_rgp    (${SafetyStatistics_Year_txt})[3]    2022
    InputText_rgp    (${SafetyStatistics_ColumnGOSHA_txt})[1]    2020
    InputText_rgp    (${SafetyStatistics_ColumnGOSHA_txt})[2]    2020
    InputText_rgp    (${SafetyStatistics_ColumnGOSHA_txt})[3]    2020
    InputText_rgp    (${SafetyStatistics_ColumnH_txt})[1]    2021
    InputText_rgp    (${SafetyStatistics_ColumnH_txt})[2]    2021
    InputText_rgp    (${SafetyStatistics_ColumnH_txt})[3]    2021
    InputText_rgp    (${SafetyStatistics_ColumnI_txt})[1]    2022
    InputText_rgp    (${SafetyStatistics_ColumnI_txt})[2]    2022
    InputText_rgp    (${SafetyStatistics_ColumnI_txt})[3]    2022
    InputText_rgp    (${SafetyStatistics_ColumnJ_txt})[1]    2020
    InputText_rgp    (${SafetyStatistics_ColumnJ_txt})[2]    2020
    InputText_rgp    (${SafetyStatistics_ColumnJ_txt})[3]    2020
    InputText_rgp    (${SafetyStatistics_ColumnK_txt})[1]    2021
    InputText_rgp    (${SafetyStatistics_ColumnK_txt})[2]    2021
    InputText_rgp    (${SafetyStatistics_ColumnK_txt})[3]    2021
    InputText_rgp    (${SafetyStatistics_TotalHrs_txt})[1]    2022
    InputText_rgp    (${SafetyStatistics_TotalHrs_txt})[2]    2022
    InputText_rgp    (${SafetyStatistics_TotalHrs_txt})[3]    2022
    Comment    InputText_rgp    (${SafetyStatistics_ColumnG_txt})[1]    2020
    Comment    InputText_rgp    (${SafetyStatistics_3YearAvg_txt})[1]    2020
    Comment    InputText_rgp    (${SafetyStatistics_LostWorkDay_txt})[1]    2021
    Comment    InputText_rgp    (${SafetyStatistics_DaysAway_txt})[1]    2022
    #Regulatory
    ExpandSection
    ClickElement_rgp    ${SafetyStatistics_Last3Yrs_rbtn}
    ClickElement_rgp    ${SafetyStatistics_Past3Yrs_rbtn}
    Select Checkbox    ${SafetyStatistics_AgreeDoc_ckb}
    ClickButton_rgp    ${Vendor_SaveAndContinue_btn}

Vendor_Business
    #Business Menu
    ExpandSection
    ${FormOfBusiness}    Set Variable    LLC
    ${Vendor_Business_FormOfBusiness_txt}    Set Variable    //input[@value="${FormOfBusiness}"]
    ClickElement_rgp    ${Vendor_Business_FormOfBusiness_txt}
    InputText_rgp    ${Vendor_Business_DAndB_txt}    123
    InputText_rgp    ${Vendor_Business_DateFounded_txt}    11/11/2020
    Select From List By Label    ${Vendor_Business_StateOfFormation_lst}    Alabama
    InputText_rgp    ${Vendor_Business_PresentName_txt}    Deepak
    InputText_rgp    ${Vendor_Business_PresentMgmt_txt}    Kishore
    InputText_rgp    ${Vendor_Business_NameOfPredecessor_txt}    SDK
    InputText_rgp    ${Vendor_Business_ReasonForDept_txt}    NA
    #Employees
    ExpandSection
    ExpandSection
    InputText_rgp    ${Vendor_Business_NoOfEmp_txt}    500
    InputText_rgp    ${Vendor_Business_Administration_txt}    Test
    InputText_rgp    ${Vendor_Business_FieldSupervision_txt}    Test
    InputText_rgp    ${Vendor_Business_FieldEmp_txt}    Test
    InputText_rgp    ${Vendor_Business_EmpRatio_txt}    10
    ClickButton_rgp    ${Vendor_SaveAndContinue_btn}

Vendor_Insurance
    #Insurance
    ExpandSection
    InputText_rgp    ${Vendor_Insurance_AgencyName_txt}    Kishore
    InputText_rgp    ${Vendor_Insurance_AgentsName_txt}    SDK
    InputText_rgp    ${Vendor_Insurance_Title_txt}    Testing
    InputText_rgp    ${Vendor_Insurance_Email_txt}    DK@DK.com
    InputText_rgp    ${Vendor_Insurance_TelephoneNo_txt}    9874563210
    InputText_rgp    ${Vendor_Insurance_FaxNo_txt}    123456
    #Insurance Info
    ExpandSection
    InputText_rgp    ${Vendor_Insurance_InsCarrier_txt}    Test
    InputText_rgp    ${Vendor_Insurance_OccurrenceLimitAmount_txt}    100
    InputText_rgp    ${Vendor_Insurance_AggLimit_txt}    100
    InputText_rgp    ${Vendor_Insurance_AccidentLimit_txt}    100
    InputText_rgp    ${Vendor_Insurance_ExcessOccurrenceLimit_txt}    100
    InputText_rgp    ${Vendor_Insurance_WorkersAccidentLimit_txt}    100
    InputText_rgp    ${Vendor_Insurance_DiseaseEachEmp_txt}    100
    InputText_rgp    ${Vendor_Insurance_DiseasePolicyLimit_txt}    100
    ClickButton_rgp    ${Vendor_SaveAndContinue_btn}

Vendor_GeneralInfo
    #Primary Business Contact
    ExpandSection
    InputText_rgp    (${Vendor_GeneralInfo_FirstName_txt})[1]    Deepak Kishore
    InputText_rgp    (${Vendor_GeneralInfo_Title_txt})[1]    AutomationEngineer
    InputText_rgp    (${Vendor_GeneralInfo_Email_txt})[1]    deepakkishore.07@gmail.com
    InputText_rgp    (${Vendor_GeneralInfo_TelephoneNo_txt})[1]    1234567890
    #Person Completing This Questionnaire
    ExpandSection
    InputText_rgp    (${Vendor_GeneralInfo_FirstName_txt})[2]    Deepak Kishore1
    InputText_rgp    (${Vendor_GeneralInfo_Title_txt})[2]    AutomationEngineer1
    InputText_rgp    (${Vendor_GeneralInfo_Email_txt})[2]    deepakkishore.07@gmail.com
    InputText_rgp    (${Vendor_GeneralInfo_TelephoneNo_txt})[2]    1234567891
    ClickButton_rgp    ${Vendor_SaveAndContinue_btn}

Vendor_Payment
    #Payment Menu
    ClickElement_rgp    ${Vendor_Payment_PayNow_btn}
    InputText_rgp    ${Vendor_Payment_CardNo_txt}    4012888888881881
    InputText_rgp    ${Vendor_Payment_ExpMonth_txt}    11
    InputText_rgp    ${Vendor_Payment_ExpYear_txt}    23
    ClickElement_rgp    ${Vendor_Payment_CardPayNow_btn}
    ClickElement_rgp    ${Vendor_Continue_btn}

Vendor_SupportingDocuments
    ClickElement_rgp    ${Vendor_DocUpload_AddNew_btn}
    Switch Window    NEW
    Wait Until Element Is Visible    //option[text()="Certificate of Insurance"]    5s
    Select From List By Label    ${Vendor_DocUpload_DocCategory_lst}    Certificate of Insurance
    InputText_rgp    ${Vendor_DocUpload_Expires_txt}    2/23/2022
    Choose File    ${Vendor_DocUpload_Upload_file}    C://Users//91861//Downloads//sample.pdf
    Sleep    5s
    ClickElement_rgp    ${Vendor_Save_btn}
    Switch Window    MAIN
    ClickElement_rgp    ${Vendor_DocUpload_AddNew_btn}
    Switch Window    NEW
    Sleep    5s
    Select From List By Label    ${Vendor_DocUpload_DocCategory_lst}    Citation Details
    Choose File    ${Vendor_DocUpload_Upload_file}    C://Users//91861//Downloads//sample.pdf
    Sleep    5s
    ClickElement_rgp    ${Vendor_Save_btn}
    Switch Window    MAIN
    ClickElement_rgp    ${Vendor_DocUpload_AddNew_btn}
    Switch Window    NEW
    Sleep    5s
    Select From List By Label    ${Vendor_DocUpload_DocCategory_lst}    NCCI Worksheets / EMR Letter
    InputText_rgp    ${Vendor_DocUpload_Expires_txt}    2/23/2022
    Choose File    ${Vendor_DocUpload_Upload_file}    C://Users//91861//Downloads//sample.pdf
    Sleep    5s
    ClickElement_rgp    ${Vendor_Save_btn}
    Switch Window    MAIN
    ClickElement_rgp    ${Vendor_DocUpload_AddNew_btn}
    Switch Window    NEW
    Sleep    5s
    Select From List By Label    ${Vendor_DocUpload_DocCategory_lst}    OSHA 300A
    Choose File    ${Vendor_DocUpload_Upload_file}    C://Users//91861//Downloads//sample.pdf
    Sleep    5s
    ClickElement_rgp    ${Vendor_Save_btn}
    Switch Window    MAIN
    ClickElement_rgp    ${Vendor_DocUpload_AddNew_btn}
    Switch Window    NEW
    Sleep    5s
    Select From List By Label    ${Vendor_DocUpload_DocCategory_lst}    Other
    Choose File    ${Vendor_DocUpload_Upload_file}    C://Users//91861//Downloads//sample.pdf
    Sleep    5s
    ClickElement_rgp    ${Vendor_Save_btn}
    Switch Window    MAIN
    ClickElement_rgp    ${Vendor_SaveAndContinue_btn}

ExpandSection
    ${xpath}    Set Variable    ${Vendor_Expand_img}
    Wait Until Element Is Visible    ${xpath}    timeout=15s
    Wait Until Element Is Enabled    ${xpath}    timeout=2s
    ClickElement_rgp    ${xpath}
