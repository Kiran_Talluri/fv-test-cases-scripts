*** Settings ***
Library           Selenium2Library

*** Keywords ***
MyAcc_Menu_PO
    ${MyAcc_MyAccount_Lnk}    Set Variable    (//a[text()="My Account"])[2]
    ${MyAcc_ChangePassword_lnk}    Set Variable    //a[text()=" Change Password "]
    ${MyAcc_CompanyDetails_lnk}    Set Variable    //a[text()=" Company Details "]
    ${MyAcc_Users_lnk}    Set Variable    //a[text()=" Users "]
    ${MyAcc_Companies_lnk}    Set Variable    //a[text()=" Companies "]
    ${MyAcc_SiteTypes_lnk}    Set Variable    //a[text()=" Site Types "]
    ${MyAcc_EmailTemplates_lnk}    Set Variable    //a[text()=" Email Templates "]
    ${MyAcc_EmailVendor_lnk}    Set Variable    //a[text()=" Email Vendor "]
    ${MyAcc_RolesAndPermissions_lnk}    Set Variable    //a[text()=" Roles And Permissions "]
    ${MyAcc_AssignRoles_lnk}    Set Variable    //a[text()=" Assign Roles "]
    ${MyAcc_BiddingInterests_lnk}    Set Variable    //a[text()=" Bidding Interests "]
    ${MyAcc_Proficiency&Capabilities_lnk}    Set Variable    //a[text()=" Proficiency & Capabilities "]
    ${MyAcc_DocumentTypes_lnk}    Set Variable    //a[text()=" Document Types "]
    ${MyAcc_InstructionBanner_lnk}    Set Variable    //a[text()=" Instruction Banner "]
    ${MyAcc_SetupNotifications_lnk}    Set Variable    //a[text()=" Setup Notifications "]
    ${MyAcc_SetupInvitationReminders_lnk}    Set Variable    //a[text()=" Setup Invitation Reminders "]
    ${MyAcc_ContractorEvaluations_lnk}    Set Variable    //a[text()=" Contractor Evaluations "]
    ${MyAcc_InvitationConfiguration_lnk}    Set Variable    //a[text()=" Invitation Configuration "]
    ${MyAcc_InvitationCCSetup_lnk}    Set Variable    //a[text()=" Invitation CC Setup "]
    ${MyAcc_NotificationsCCSetup_lnk}    Set Variable    //a[text()=" Notifications CC Setup "]
    ${MyAcc_PowerBIPermissions_lnk}    Set Variable    //a[text()=" Power BI Permissions "]
    Set Test Variable    ${MyAcc_MyAccount_Lnk}
    Set Test Variable    ${MyAcc_ChangePassword_lnk}
    Set Test Variable    ${MyAcc_CompanyDetails_lnk}
    Set Test Variable    ${MyAcc_Users_lnk}
    Set Test Variable    ${MyAcc_Companies_lnk}
    Set Test Variable    ${MyAcc_SiteTypes_lnk}
    Set Test Variable    ${MyAcc_EmailTemplates_lnk}
    Set Test Variable    ${MyAcc_EmailVendor_lnk}
    Set Test Variable    ${MyAcc_RolesAndPermissions_lnk}
    Set Test Variable    ${MyAcc_AssignRoles_lnk}
    Set Test Variable    ${MyAcc_BiddingInterests_lnk}
    Set Test Variable    ${MyAcc_Proficiency&Capabilities_lnk}
    Set Test Variable    ${MyAcc_DocumentTypes_lnk}
    Set Test Variable    ${MyAcc_InstructionBanner_lnk}
    Set Test Variable    ${MyAcc_SetupNotifications_lnk}
    Set Test Variable    ${MyAcc_SetupInvitationReminders_lnk}
    Set Test Variable    ${MyAcc_ContractorEvaluations_lnk}
    Set Test Variable    ${MyAcc_InvitationConfiguration_lnk}
    Set Test Variable    ${MyAcc_InvitationCCSetup_lnk}
    Set Test Variable    ${MyAcc_NotificationsCCSetup_lnk}
    Set Test Variable    ${MyAcc_PowerBIPermissions_lnk}

MyAcc_User_PO
    ${MyAcc_User_SearchType_Name_lnk}    Set Variable    //*[@id="buttonTabOne"]
    ${MyAcc_User_SearchType_Advanced_lnk}    Set Variable    //*[@id="buttonTabTwo"]
    ${MyAcc_User_SearchType_Adv_Status_Active_ckb}    Set Variable    //*[@id="UserStatus_Active"]
    ${MyAcc_User_SearchType_Adv_Status_InActive_ckb}    Set Variable    //*[@id="UserStatus_InActive"]
    ${MyAcc_User_SearchType_Adv_Admin_lst}    Set Variable    //*[@id="AdminId"]
    ${MyAcc_User_SearchType_Adv_Client_lst}    Set Variable    //*[@id="ClientId"]
    ${MyAcc_User_SearchType_Adv_Vendor_txt}    Set Variable    //*[@id="VendorId_drop"]
    ${MyAcc_User_SearchType_Adv_SuperClient_lst}    Set Variable    //*[@id="SuperClientId"]
    ${MyAcc_User_SearchType_Adv_Search_btn}    Set Variable    //*[@id="spinner"]
    ${MyAcc_User_UserView_lnk}    Set Variable    //a[contains(text(),"User View - ")]
    Set Test Variable    ${MyAcc_User_SearchType_Name_lnk}
    Set Test Variable    ${MyAcc_User_SearchType_Advanced_lnk}
    Set Test Variable    ${MyAcc_User_SearchType_Adv_Status_Active_ckb}
    Set Test Variable    ${MyAcc_User_SearchType_Adv_Status_InActive_ckb}
    Set Test Variable    ${MyAcc_User_SearchType_Adv_Admin_lst}
    Set Test Variable    ${MyAcc_User_SearchType_Adv_Client_lst}
    Set Test Variable    ${MyAcc_User_SearchType_Adv_Vendor_txt}
    Set Test Variable    ${MyAcc_User_SearchType_Adv_SuperClient_lst}
    Set Test Variable    ${MyAcc_User_SearchType_Adv_Search_btn}
    Set Test Variable    ${MyAcc_User_UserView_lnk}

MyAcc_SearchUser_Advanced
    [Arguments]    ${isActive}    ${Admin}    ${Client}    ${VendorName}    ${SuperClient}    ${UserName}
    Comment    sleep    10s
    ClickLink_rgp    ${MyAcc_MyAccount_Lnk}
    ClickElement_rgp    ${MyAcc_Users_lnk}
    ClickElement_rgp    ${MyAcc_User_SearchType_Advanced_lnk}
    Run Keyword If    '${isActive}'=='True'    ClickElement_rgp    ${MyAcc_User_SearchType_Adv_Status_Active_ckb}    '${isActive}'=='False'    ClickElement_rgp    ${MyAcc_User_SearchType_Adv_Status_InActive_ckb}
    Select From List By Label    ${MyAcc_User_SearchType_Adv_Admin_lst}    ${Admin}
    Select From List By Label    ${MyAcc_User_SearchType_Adv_Client_lst}    ${Client}
    InputText_rgp    ${MyAcc_User_SearchType_Adv_Vendor_txt}    ${VendorName}
    ${Vendor_xpath}    Set Variable    //li[text()="+${VendorName}+"
    Run Keyword If    '${VendorName}'!='${EMPTY}'    Wait Until Element Is Visible    ${Vendor_xpath}    timeout=10s
    Run Keyword If    '${VendorName}'!='${EMPTY}'    ClickElement_rgp    ${Vendor_xpath}
    Select From List By Label    ${MyAcc_User_SearchType_Adv_SuperClient_lst}    ${SuperClient}
    ClickButton_rgp    ${MyAcc_User_SearchType_Adv_Search_btn}
    Wait Until Element Is Visible    //a[text()="${UserName}"]    timeout=5s
    ClickLink_rgp    //a[text()="${UserName}"]
    ClickLink_rgp    ${MyAcc_User_UserView_lnk}

MyAcc_InitializeWebElement
    MyAcc_Menu_PO
    MyAcc_User_PO
