*** Settings ***
Library           Selenium2Library

*** Keywords ***
ClickElement_rgp
    [Arguments]    ${Xpath}
    Wait Until Element Is Visible    ${Xpath}    timeout=5s
    Wait Until Element Is Enabled    ${Xpath}    timeout=5s
    Click Element    ${Xpath}

ClickButton_rgp
    [Arguments]    ${Xpath}
    Wait Until Element Is Visible    ${Xpath}    timeout=5s
    Wait Until Element Is Enabled    ${Xpath}    timeout=5s
    Click Button    ${Xpath}

ClickLink_rgp
    [Arguments]    ${Xpath}
    Wait Until Element Is Visible    ${Xpath}    timeout=5s
    Wait Until Element Is Enabled    ${Xpath}    timeout=5s
    Click Link    ${Xpath}

InputText_rgp
    [Arguments]    ${Xpath}    ${Value}
    Wait Until Element Is Visible    ${Xpath}    timeout=5s
    Log    ${Xpath}
    Clear Element Text    ${Xpath}
    Input Text    ${Xpath}    ${Value}
