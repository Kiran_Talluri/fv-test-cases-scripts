*** Settings ***
Resource          Pages/ClientPage.robot
Resource          Pages/LoginAndRegistrationPage.robot
Resource          Pages/MyAccountPage.robot
Resource          Pages/VendorPage.robot

*** Keywords ***
Config
    InitializeAllWebElements
    ${Url_Test}    Set Variable    https://pqtest.firstverify.com/
    ${Environment}    Set Variable    Test
    ${UserName_admin}    Set Variable    FVTestingTeam@ahaapps.com
    ${Password_admin}    Set Variable    Testing#123
    Set Suite Variable    ${Url_Test}
    Set Suite Variable    ${Environment}
    Set Suite Variable    ${UserName_admin}
    Set Suite Variable    ${Password_admin}

InitializeAllWebElements
    Login_InitializePageElements
    MyAcc_InitializeWebElement
    Client_InitializeWebElement
    Vendor_InitializeWebElements
